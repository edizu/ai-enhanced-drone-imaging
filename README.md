---------------------------------------------
AI Enhanced Drone Imaging on Zynq UltraScale+
---------------------------------------------

This is the repository for the project titled “AI Enhanced Drone Imaging on Zynq UltraScale+”, a EEE Fifth Year Masters project.

Drones have demonstrated great potential in areas such as autonomous delivery and search and rescue, with over 800% market growth predicted in the next five years. Such applications require Artificial Intelligence (AI) enhanced image processing which demands high on-device computational capabilities, with low power consumption. 

This project developed a novel AI acceleration platform on the Zynq UltraScale+ programmable system-on-chip (SoC), used to enable advanced drone applications in academia and industry. Video data captured from a camera is processed by the convolutional neural network (CNN) in real-time, accelerated in custom hardware, to detect objects of interest in the video frames. These detections are processed by an advanced target mapping system which fuses this data with GPS sensor readings to enable object tracking, resulting in a powerful and adaptable platform technology for drone applications.

The main code for the project is contained within the Group_A_Notebook.ipynb Jupyter Notebook file. This links to the additional code files found within their respective folders. Additionally, the hardware designs for this project have also been included.