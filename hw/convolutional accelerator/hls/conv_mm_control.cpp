// Ryan Greer
// 24/04/21
// conv_mm_control.cpp
// HLS control block top-level source code for implementing AXI4-Stream interfaces and data sorting for controlling systolic arrays

#include "conv_mm_control.h"

// get_cache_input_FM_matrix_tile stores a tile of the cache input FM matrix in BRAM from the stream (only
// one row needs to be stored at a time
void get_cache_input_FM_matrix_tile(input_fm_stream &cache_input_FM_stream,
									input_fm_values cache_input_FM[SYSTOLIC_ARRAY_SIZE][MAX_TELEM],
									sys_array_dim *r_tile_size,
									cache_dim *Telem,
									cache_dim *n_elem_blocks){

	ap_uint<AXIS_DATA_LANE_WIDTH> input_stream_value;

	input_read_rows: for(ap_uint<4> row = 0; row < 8; row++){
		if(row < *r_tile_size){
			input_read_elems: for(cache_dim elem_block = 0; elem_block < MAX_TELEM/8; elem_block++){
#pragma HLS PIPELINE II=1
				if(elem_block < *n_elem_blocks){
					input_stream_value = cache_input_FM_stream.read().data;
					cache_dim elem_start_idx = elem_block<<3;
					input_read_words: for(ap_uint<4> i = 0; i < 8; i++){
						cache_input_FM[row][i+elem_start_idx] = input_stream_value.range(i*16+15,i*16);
					}
				}
				else{
					break;
				}
			}
		}
		else{
			break;
		}
	}

}

// get_cache_weights_matrix stores the entire cache weights matrix in BRAM
void get_cache_weights_matrix(weights_stream &cache_weights_stream,
							  weights_values cache_weights[MAX_TELEM][MAX_TC],
							  cache_dim *Telem,
							  cache_dim *Tc,
							  cache_dim *n_col_blocks){

	ap_uint<AXIS_DATA_LANE_WIDTH> weights_stream_value;

	weights_read_elems: for(cache_dim elem = 0; elem < MAX_TELEM; elem++){
		if(elem < *Telem){
			weights_read_cols: for(cache_dim col_block = 0; col_block < MAX_TC/8; col_block++){
#pragma HLS PIPELINE II=1
				if(col_block < *n_col_blocks){
					weights_stream_value = cache_weights_stream.read().data;
					cache_dim col_start_idx = col_block<<3;
					weights_read_words: for(ap_uint<4> i = 0; i < 8; i++){
						cache_weights[elem][i+col_start_idx] = weights_stream_value.range(i*16+15,i*16);
					}
				}
				else{
					break;
				}
			}
		}
		else{
			break;
		}
	}

}

// prepare AXI4-Stream for output FM matrix
void stream_cache_output_FM_tile(output_fm_AXIS_small cache_output_FM[SYSTOLIC_ARRAY_SIZE][MAX_TC],
								 output_fm_stream &cache_output_FM_tile_stream,
								 sys_array_dim *r_tile_size,
								 cache_dim *Tc,
								 cache_dim *n_col_blocks){

	output_fm_AXIS output_stream_value;		// we also need to inclde TLAST signal here
	output_stream_value.last = 0;

	// loop over output FM tile and add to stream output
	output_write_rows: for(ap_uint<4> row = 0; row < SYSTOLIC_ARRAY_SIZE; row++){
		if(row < *r_tile_size){
			output_write_cols: for(cache_dim col_block = 0; col_block < MAX_TC/8; col_block++){
#pragma HLS PIPELINE II=1
				if(col_block < *n_col_blocks){
					cache_dim col_start_idx = col_block<<3;
					output_write_words: for(ap_uint<4> i = 0; i < 8; i++){
						output_stream_value.data.range(i*16+15,i*16) = cache_output_FM[row][i+col_start_idx].data;
						if(cache_output_FM[row][i+col_start_idx].last == 1){
							output_stream_value.last = 1;
						}
					}
					cache_output_FM_tile_stream.write(output_stream_value);
				}
				else{
					break;
				}
			}
		}
		else{
			break;
		}
	}

}

// conv_mm_control tiles the input FM and weights cache matrices into 8x8 matrices
// which are compatible with the systolic array
// Tr and Tc are the number of rows and columns in the input FM and weights cache
// matrices, respectively
void conv_mm_control(input_fm_stream &cache_input_FM_stream,
					 weights_stream &cache_weights_stream,
					 output_fm_stream &cache_output_FM_stream,
					 row_dim *Tr,
					 cache_dim *Tc,
					 cache_dim *Telem,
					 ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_1[AXI_WRITE_BURST_SIZE],
					 ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_1[AXI_READ_BURST_SIZE],
					 ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_2[AXI_WRITE_BURST_SIZE],
					 ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_2[AXI_READ_BURST_SIZE]){

	// ceiling of Tr / SYSTOLIC_ARRAY_SIZE and Tc / SYSTOLIC_ARRAY_SIZE
	row_tile_idx r_itr = (*Tr + SYSTOLIC_ARRAY_SIZE - 1) / SYSTOLIC_ARRAY_SIZE;
	col_tile_idx c_itr = (*Tc + SYSTOLIC_ARRAY_SIZE - 1) / SYSTOLIC_ARRAY_SIZE;

	// number of 128 bit blocks for reading multiple columns of input FM matrix in parallel - ceiling of Telem/8
	cache_dim n_elem_blocks = (*Telem + 8 - 1) / 8;
	cache_dim n_col_blocks = c_itr;

	// rows and columns remaining if tiles of size 8 are used - can avoid additional circuitry if
	// we ensure the input matrix dimensions are multiples of the systolic array dimension
	sys_array_dim remain_row = *Tr % SYSTOLIC_ARRAY_SIZE;
	sys_array_dim remain_col = *Tc % SYSTOLIC_ARRAY_SIZE;

	cache_dim c_start_index = 0;

	// define memory for a single tile of the input FM matrix - we only need to access the tile on every col tile iteration
	// then discard it - not the case with the weights matrix so we need to store that entirely in BRAM
	input_fm_values cache_input_FM_tile[SYSTOLIC_ARRAY_SIZE][MAX_TELEM];
#pragma HLS ARRAY_PARTITION variable=cache_input_FM_tile cyclic factor=4 dim=2
#pragma HLS ARRAY_PARTITION variable=cache_input_FM_tile block factor=4 dim=1

	// stream weights matrix into BRAM
	// weights_values cache_weights[MAX_TELEM][MAX_TC];
	static weights_values cache_weights[MAX_TELEM][MAX_TC];
	// change factor to 16, 24, 32 when adding multiple systolic arrays****
#pragma HLS ARRAY_PARTITION variable=cache_weights cyclic factor=16 dim=2
	get_cache_weights_matrix(cache_weights_stream,
							 cache_weights,
							 Telem,
							 Tc,
							 &n_col_blocks);

	// define memory for tile of cache output - this tile is for all columns and one set of rows as this is important for the order
	// of streaming out the result
	output_fm_AXIS_small cache_output_FM_tile[SYSTOLIC_ARRAY_SIZE][MAX_TC];
	// change factor to 16, 24, 32 when adding multiple systolic arrays****
#pragma HLS ARRAY_PARTITION variable=cache_output_FM_tile cyclic factor=16 dim=2
#pragma HLS ARRAY_PARTITION variable=cache_output_FM_tile cyclic factor=2 dim=1

	// tile sizes as rows and columns of output FM tiled matrix - usually 8x8 but at the ends of the matrix may be a little smaller
	sys_array_dim r_tile_size = SYSTOLIC_ARRAY_SIZE;
	sys_array_dim c_tile_size = SYSTOLIC_ARRAY_SIZE*N_SYSTOLIC_ARRAYS;

	// implementation of loop tiling for the systolic array
	// LOOP OVER ROW TILES:
	row_tiles_loop: for(row_tile_idx r_tile = 0; r_tile < MAX_R_TILES; r_tile++){
		if(r_tile < r_itr){
			if(r_tile == (r_itr-1) && remain_row > 0){
				r_tile_size = remain_row;
			}
			else{
				r_tile_size = SYSTOLIC_ARRAY_SIZE;
			}

			// get the tiled input FM matrix
			get_cache_input_FM_matrix_tile(cache_input_FM_stream,
										   cache_input_FM_tile,
										   &r_tile_size,
										   Telem,
										   &n_elem_blocks);

			// LOOP OVER COLUMN TILES:
			col_tiles_loop: for(col_tile_idx c_tile_loop = 0; c_tile_loop < MAX_C_TILES/N_SYSTOLIC_ARRAYS; c_tile_loop++){
				col_tile_idx c_tile = c_tile_loop * N_SYSTOLIC_ARRAYS;
				if(c_tile < c_itr){
					c_start_index = cache_dim(c_tile) << 3;				// note, this only works if the systolic array has size 8x8
					if(c_tile >= (c_itr-N_SYSTOLIC_ARRAYS)){
						// we are on the last columns tiles
						if(remain_col > 0){
							c_tile_size = remain_col + (c_itr - c_tile - 1) * 8;
						}
						else{
							c_tile_size = (c_itr - c_tile) * 8;
						}
					}
					else{
						c_tile_size = SYSTOLIC_ARRAY_SIZE * N_SYSTOLIC_ARRAYS;
					}

					// call function to prepare buffers for systolic array - cache output FM tile will be updated
					systolic_arr_buffer_setup(cache_input_FM_tile,
											  cache_weights,
											  cache_output_FM_tile,
											  r_tile_size,
											  c_tile_size,
											  c_start_index,
											  Telem,
											  sa_m_axi_output_1,
											  sa_m_axi_input_1,
											  sa_m_axi_output_2,
											  sa_m_axi_input_2);

					// check if we are at the end of the output FM matrix and set TLAST signal to '1' if so
					if(r_tile == (r_itr-1) && c_tile >= (c_itr-N_SYSTOLIC_ARRAYS)){
						cache_dim output_FM_last_row_idx = r_tile_size-1;
						cache_dim output_FM_last_col_idx = c_start_index + c_tile_size-1;
						cache_output_FM_tile[output_FM_last_row_idx][output_FM_last_col_idx].last = 1;
					}


				}
				else{
					break;
				}
			}

			// add cache output FM matrix tile to cache output FM matrix stream (tile of size r_tile_size x Tc)
			// can stream directly - no need to add to previous calculation as we are not tiling along the elements
			// within the hardware design (this happens in the software driver)
			stream_cache_output_FM_tile(cache_output_FM_tile,
					 	 	 	 	 	cache_output_FM_stream,
										&r_tile_size,
										Tc,
										&n_col_blocks);

		}
		else{
			break;			// remove for latency analysis
		}
	}


}

void conv_mm_top(input_fm_stream &cache_input_FM_stream,
				weights_stream &cache_weights_stream,
				output_fm_stream &cache_output_FM_stream,
				ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_1[AXI_WRITE_BURST_SIZE],
				ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_1[AXI_READ_BURST_SIZE],
				ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_2[AXI_WRITE_BURST_SIZE],
				ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_2[AXI_READ_BURST_SIZE]){
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=cache_input_FM_stream
#pragma HLS INTERFACE axis port=cache_weights_stream
#pragma HLS INTERFACE axis port=cache_output_FM_stream
#pragma HLS INTERFACE m_axi depth=32 port=sa_m_axi_input_1 offset=direct bundle=SYSTOLIC_ARRAY_1 num_write_outstanding=32 max_write_burst_length=32
#pragma HLS INTERFACE m_axi depth=32 port=sa_m_axi_output_1 offset=direct bundle=SYSTOLIC_ARRAY_1 num_write_outstanding=32 max_write_burst_length=32
#pragma HLS INTERFACE m_axi depth=32 port=sa_m_axi_input_2 offset=direct bundle=SYSTOLIC_ARRAY_2 num_write_outstanding=32 max_write_burst_length=32
#pragma HLS INTERFACE m_axi depth=32 port=sa_m_axi_output_2 offset=direct bundle=SYSTOLIC_ARRAY_2 num_write_outstanding=32 max_write_burst_length=32

	ap_uint<AXIS_DATA_LANE_WIDTH> tile_sizes = cache_input_FM_stream.read().data;
	row_dim Tr = tile_sizes.range(31,0);
	cache_dim Tc = tile_sizes.range(47,32);
	cache_dim Telem = tile_sizes.range(63,48);

	conv_mm_control(cache_input_FM_stream,
					cache_weights_stream,
					cache_output_FM_stream,
					&Tr,
					&Tc,
					&Telem,
					sa_m_axi_output_1,
					sa_m_axi_input_1,
					sa_m_axi_output_2,
					sa_m_axi_input_2);

}
