// Ryan Greer
// 24/04/21
// conv_mm_control.h
// Header for HLS control block top level

#ifndef CONV_MM_CONTROL__
#define CONV_MM_CONTROL__

#include <cmath>
#include <ap_fixed.h>
#include <ap_int.h>

#include "hls_stream.h"

#define SYSTOLIC_ARRAY_SIZE 8
#define MAX_TR 262144
#define MAX_TC 32
#define MAX_TELEM 4608
#define MAX_R_TILES MAX_TR / SYSTOLIC_ARRAY_SIZE
#define MAX_C_TILES MAX_TC / SYSTOLIC_ARRAY_SIZE

#define MATRIX_DATA_WIDTH 16

#define AXI_DATA_LANE_WIDTH_WRITE 256
#define AXI_DATA_LANE_WIDTH_READ 256
#define AXI_WRITE_BURST_SIZE 42
#define AXI_READ_BURST_SIZE 4

#define N_SYSTOLIC_ARRAYS 2

#define AXIS_DATA_LANE_WIDTH 128

// 16-bit types (bfloat16)
typedef ap_uint<MATRIX_DATA_WIDTH> input_fm_values;
typedef ap_uint<MATRIX_DATA_WIDTH> weights_values;
typedef ap_uint<MATRIX_DATA_WIDTH> output_fm_values;

// for indexing the cache matrices
typedef ap_uint<16> cache_dim;
typedef ap_uint<32> row_dim;

// for indexing the systolic array rows and columns - no bits = ceil(log2(SYSTOLIC_ARRAY_SIZE*N_SYSTOLIC_ARRAYS))+1
typedef ap_uint<6> sys_array_dim;

// for indexing row tiles - no bits = ceil(log2(MAX_R_TILES))+1
typedef ap_uint<16> row_tile_idx;

// for indexing column tiles - no bits = ceil(log2(MAX_TC/8))+1
typedef ap_uint<6> col_tile_idx;

struct input_fm_AXIS{
	ap_uint<AXIS_DATA_LANE_WIDTH> data;
	bool last;
};

struct weights_AXIS{
	ap_uint<AXIS_DATA_LANE_WIDTH> data;
	bool last;
};

// for an individual 16 bit slice of output AXIS
struct output_fm_AXIS_small{
	ap_uint<MATRIX_DATA_WIDTH> data;
	bool last;
};

struct output_fm_AXIS{
	ap_uint<AXIS_DATA_LANE_WIDTH> data;
	bool last;
};

typedef hls::stream<input_fm_AXIS> input_fm_stream;
typedef hls::stream<weights_AXIS> weights_stream;
typedef hls::stream<output_fm_AXIS> output_fm_stream;

#include "systolic_arr_buffer_setup.h"

void get_cache_input_FM_matrix_tile(input_fm_stream &cache_input_FM_stream,
									input_fm_values cache_input_FM[SYSTOLIC_ARRAY_SIZE][MAX_TELEM],
									sys_array_dim *r_tile_size,
									cache_dim *Telem,
									cache_dim *n_elem_blocks);

void get_cache_weights_matrix(weights_stream &cache_weights_stream,
							  weights_values cache_weights[MAX_TELEM][MAX_TC],
							  cache_dim *Telem,
							  cache_dim *Tc,
							  cache_dim *n_col_blocks);

void stream_cache_output_FM_tile(output_fm_AXIS_small cache_output_FM[SYSTOLIC_ARRAY_SIZE][MAX_TC],
								 output_fm_stream &cache_output_FM_tile_stream,
								 sys_array_dim *r_tile_size,
								 cache_dim *Tc,
								 cache_dim *n_col_blocks);

void conv_mm_control(input_fm_stream &cache_input_FM,
					 weights_stream &cache_weights,
					 output_fm_stream &cache_output_FM,
					 row_dim *Tr,
					 cache_dim *Tc,
					 cache_dim *Telem,
					 ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_1[AXI_WRITE_BURST_SIZE],
					 ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_1[AXI_READ_BURST_SIZE],
					 ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_2[AXI_WRITE_BURST_SIZE],
					 ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_2[AXI_READ_BURST_SIZE]);

void conv_mm_top(input_fm_stream &cache_input_FM,
				weights_stream &cache_weights,
				output_fm_stream &cache_output_FM,
				ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_1[AXI_WRITE_BURST_SIZE],
				ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_1[AXI_READ_BURST_SIZE],
				ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_2[AXI_WRITE_BURST_SIZE],
				ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_2[AXI_READ_BURST_SIZE]);
#endif
