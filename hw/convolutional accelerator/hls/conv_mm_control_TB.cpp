// Ryan Greer
// 24/04/21
// conv_mm_control_TB.cpp
// Testbench for HLS control block

#include "conv_mm_control.h"
#include <string>

#include <fstream>
#include <iostream>

using namespace std;

void get_cache_dimensions(cache_dim cache_data_dim[3]){
	// elements of file are Tr, Tc, Telem
	ifstream data_cache_dim;
	data_cache_dim.open("C:/Users/ryan_/Documents/University/Project/MATLAB/MatrixData/cache_matrix_dim.dat");
	for(int i = 0; i < 3; i++){
		data_cache_dim >> cache_data_dim[i];
	}
	data_cache_dim.close();
}

void get_cache_matrices(input_fm_stream &cache_input_FM_stream, weights_stream &cache_weights_stream, output_fm_AXIS_small cache_output_FM_golden[128][MAX_TC], cache_dim cache_data_dim[3]){
	cache_dim Tr = cache_data_dim[0];
	cache_dim Tc = cache_data_dim[1];
	cache_dim Telem = cache_data_dim[2];

	// get cache_input_FM_stream
	ifstream data_cache_input_fm;
	data_cache_input_fm.open("C:/Users/ryan_/Documents/University/Project/MATLAB/MatrixData/cache_input_matrix.dat");

	input_fm_AXIS input_fm_element;

	ap_uint<3> stream_element_counter = 0;

	for(int r = 0; r < Tr; r++){
		stream_element_counter = 0;
		for(int c = 0; c < Telem; c++){
			ap_uint<16> read_temp;
			data_cache_input_fm >> read_temp;
			input_fm_element.data.range(c%8*16+15, c%8*16) = read_temp;
			stream_element_counter = stream_element_counter + 1;
			if(stream_element_counter == 0 || c == (Telem-1)){
				cache_input_FM_stream.write(input_fm_element);
			}
		}
	}
	data_cache_input_fm.close();

	// get cache_weights_stream
	ifstream data_cache_weights;
	data_cache_weights.open("C:/Users/ryan_/Documents/University/Project/MATLAB/MatrixData/cache_weights_matrix.dat");

	weights_AXIS weights_element;

	stream_element_counter = 0;

	for(int r = 0; r < Telem; r++){
		stream_element_counter = 0;
		for(int c = 0; c < Tc; c++){
			ap_uint<16> read_temp;
			data_cache_weights >> read_temp;
			weights_element.data.range(c%8*16+15, c%8*16) = read_temp;
			stream_element_counter = stream_element_counter + 1;
			if(stream_element_counter == 0 || c == (Tc-1)){
				cache_weights_stream.write(weights_element);
			}
		}
	}
	data_cache_weights.close();

	// get cache_output_FM_stream
	ifstream data_cache_output_fm;
	data_cache_output_fm.open("C:/Users/ryan_/Documents/University/Project/MATLAB/MatrixData/cache_output_matrix.dat");

	for(int r = 0; r < Tr; r++){
		for(int c = 0; c < Tc; c++){
			data_cache_output_fm >> cache_output_FM_golden[r][c].data;
		}
	}
	data_cache_output_fm.close();
}

int main(void){

	// get cache matrix dimensions from file
	cache_dim cache_data_dim[3];
	get_cache_dimensions(cache_data_dim);
	cache_dim Tr = cache_data_dim[0];
	cache_dim Tc = cache_data_dim[1];
	cache_dim Telem = cache_data_dim[2];

	// get cache input fm, cache weights and golden reference cache output fm matrices (as streams)
	input_fm_stream cache_input_FM_stream;
	weights_stream cache_weights_stream;
	output_fm_AXIS_small cache_output_FM_golden[128][MAX_TC];

	input_fm_AXIS tile_sizes_AXIS;
	tile_sizes_AXIS.data.range(31,0) = Tr;
	tile_sizes_AXIS.data.range(47,32) = Tc;
	tile_sizes_AXIS.data.range(63,48) = Telem;
	tile_sizes_AXIS.last = 0;
	cache_input_FM_stream.write(tile_sizes_AXIS);

	get_cache_matrices(cache_input_FM_stream, cache_weights_stream, cache_output_FM_golden, cache_data_dim);

	// cache output fm from design to compare with golden reference
	output_fm_stream cache_output_FM_stream;

	ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_1[AXI_WRITE_BURST_SIZE];
	ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_1[AXI_READ_BURST_SIZE];
	ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_2[AXI_WRITE_BURST_SIZE];
	ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_2[AXI_READ_BURST_SIZE];

	// set up some data to be read from master AXI port
	for(int i = 0; i < AXI_READ_BURST_SIZE; i++){
		for(int j = 0; j < 8; j++){
			sa_m_axi_input_1[i].range(j*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, j*MATRIX_DATA_WIDTH) = 8 * i*2 + j;
			sa_m_axi_input_2[i].range(j*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, j*MATRIX_DATA_WIDTH) = 8 * i*2 + j;
		}
		for(int j = 8; j < 16; j++){
			sa_m_axi_input_1[i].range(j*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, j*MATRIX_DATA_WIDTH) = 8 * i*2 + j;
			sa_m_axi_input_2[i].range(j*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, j*MATRIX_DATA_WIDTH) = 8 * i*2 + j;
		}
	}

	conv_mm_top(cache_input_FM_stream,
				cache_weights_stream,
				cache_output_FM_stream,
				sa_m_axi_output_1,
				sa_m_axi_input_1,
				sa_m_axi_output_2,
				sa_m_axi_input_2);

	// compare result to golden reference
	int error_count = 0;
	float tol = 0.001;
	cout << setprecision(4) << fixed;

	output_fm_AXIS cache_output_FM_value;

	cout << "=========== CACHE OUTPUT FM MATRIX ===========\n";
	for(int r = 0; r < Tr; r++){
		for(int c = 0; c < Tc; c++){
			if(c%8 == 0){
				cache_output_FM_value = cache_output_FM_stream.read();
			}
			ap_uint<16> output_FM_value_16bit = cache_output_FM_value.data.range(c%8*16+15,c%8*16);
			cout << output_FM_value_16bit << "  \t";
			if(cache_output_FM_value.last == 1 && r == (Tr-1) && c == (Tc-1)){
				cout << "[LAST]" << "\t";
			}
			if(((output_FM_value_16bit < cache_output_FM_golden[r][c].data)
			   && ((output_FM_value_16bit + tol) < cache_output_FM_golden[r][c].data))
			   || ((output_FM_value_16bit > cache_output_FM_golden[r][c].data)
			   && ((output_FM_value_16bit - tol) > cache_output_FM_golden[r][c].data)))
			{
				error_count++;
			}
		}
		cout << "\n";
	}
	cout << "\n======= CACHE OUTPUT FM MATRIX GOLDEN =======\n";
	for(int r = 0; r < Tr; r++){
		for(int c = 0; c < Tc; c++){
			cout << cache_output_FM_golden[r][c].data << "  \t";
		}
		cout << "\n";
	}

	// we always return a zero if the test has been successful
	if(error_count == 0){
		cout << "\nResult matches golden reference :)\n";
		return 0;
	}
	else{
		cout << "\nResult does not match golden reference :(\n";
		cout << error_count << " errors.";
		return 0;			// TEMP - CHANGE BACK TO 1 LATER - FOR VERIFYING COSIM
	}
}
