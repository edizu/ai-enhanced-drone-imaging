// Ryan Greer
// 24/04/21
// systolic_arr_buffer_setup.cpp
// HLS source code for implementing buffer setup and timing for multiple systolic arrays

#include "systolic_arr_buffer_setup.h"

void get_cache_weights_matrix_tile(weights_values cache_weights[MAX_TELEM][MAX_TC],
		   	   	   	   	   	   	   sys_array_dim c_tile_size,
								   cache_dim c_start_index,
								   cache_dim elem_idx,
								   weights_values cache_weights_tile[SYSTOLIC_ARRAY_SIZE][SYSTOLIC_ARRAY_SIZE*N_SYSTOLIC_ARRAYS]){
#pragma HLS INLINE

	// inline and hence unroll loops in this function automatically

	// shift all values down to make room for new row tile of weights matrix
	for(ap_uint<4> elem = 0; elem < (SYSTOLIC_ARRAY_SIZE-1); elem++){
		for(ap_uint<5> col = 0; col < 16; col++){
			if(col < c_tile_size){
				cache_weights_tile[elem][col] = cache_weights_tile[elem+1][col];
			}
		}
	}

	// insert new row of weights matrix tile - manually unrolled loop to meet array access timing requirements
	if(c_start_index == 0){
		for(ap_uint<5> i = 0; i < 16; i++){
			if(i < c_tile_size){
				cache_weights_tile[7][i] = cache_weights[elem_idx][i];
			}
		}
	}
	else{
		for(ap_uint<5> i = 0; i < 16; i++){
			if(i < c_tile_size){
				cache_weights_tile[7][i] = cache_weights[elem_idx][i+16];
			}
		}
	}

	// insert zeros in cache_weights_tile beyond end of cache_weights if necessary
	for(ap_int<6> i = 15; i >= 0; i--){
		if(i > c_tile_size){
			cache_weights_tile[7][i] = 0;
		}
	}

}

void systolic_arr_buffer_setup(input_fm_values cache_input_FM_tile[SYSTOLIC_ARRAY_SIZE][MAX_TELEM],
							   weights_values cache_weights[MAX_TELEM][MAX_TC],
							   output_fm_AXIS_small cache_output_FM_tile[SYSTOLIC_ARRAY_SIZE][MAX_TC],
							   sys_array_dim r_tile_size,
							   sys_array_dim c_tile_size,
							   cache_dim c_start_index,
							   cache_dim *Telem,
							   ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_1[AXI_WRITE_BURST_SIZE],
							   ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_1[AXI_READ_BURST_SIZE],
							   ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_2[AXI_WRITE_BURST_SIZE],
							   ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_2[AXI_READ_BURST_SIZE]){

	// this matrix is an 8x8 matrix which updates for each systolic array cycle (tau) - the oldest row is removed and the
	// newest appended in a first-in first-out manner to help the RTL generation pipeline the loop
	weights_values cache_weights_tile[SYSTOLIC_ARRAY_SIZE][SYSTOLIC_ARRAY_SIZE*N_SYSTOLIC_ARRAYS];
#pragma HLS ARRAY_PARTITION variable=cache_weights_tile complete dim=0

	// value in loop bound is the maximum number of iterations to compute the
	// full matrix multiplication (zero to no. itr minus one)
	// send next set of elements to buffers (data is initaially in memory (BRAM))
	time_step: for(cache_dim tau_loop = 0; tau_loop < MAX_TELEM + 10; tau_loop+=AXI_WRITE_BURST_SIZE){
		if(tau_loop < (*Telem + 10)){
			axi_burst_loop: for(ap_uint<6> beat = 0; beat < AXI_WRITE_BURST_SIZE; beat++){
#pragma HLS PIPELINE II=1
				ap_uint<128> sa_m_axi_output_temp_input;

				// array to store weights buffer values for each systolic array
				ap_uint<128> sa_m_axi_output_temp_weights[N_SYSTOLIC_ARRAYS];

				cache_dim tau = tau_loop + beat;
				if(tau < (*Telem + 10)){

			        // iterates over rows of first matrix - i.e. iterates through the buffer entries
					input_fm_buffer_elem: for(sys_array_dim i = 0; i < SYSTOLIC_ARRAY_SIZE; i++){

						if(i < r_tile_size){
							int elem_idx = tau - i;							// which element of matrix do we want
							if(elem_idx >= 0 && elem_idx < *Telem){
								// next element for input buffer
								sa_m_axi_output_temp_input.range(i*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, i*MATRIX_DATA_WIDTH) = cache_input_FM_tile[i][elem_idx];
							}
							else{
					            // place a zero in buffer if not at the iteration yet for this
					            // element (and row/column of output matrix)
								sa_m_axi_output_temp_input.range(i*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, i*MATRIX_DATA_WIDTH) = 0;
							}
						}
						else{
							sa_m_axi_output_temp_input.range(i*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, i*MATRIX_DATA_WIDTH) = 0;
						}
					}

					// get tile of weights matrix
					if(tau < *Telem){
						get_cache_weights_matrix_tile(cache_weights,
													  c_tile_size,
													  c_start_index,
													  tau,
													  cache_weights_tile);
					}

					// LOOP FOR DIFFERENT SYSTOLIC ARRAYS WEIGHTS BUFFERS

					for(ap_uint<3> h = 0; h < N_SYSTOLIC_ARRAYS; h++){

						// iterates over columns of second matrix
						int elem_idx_w = 7;							// starts at 7 as we are using the tiles weights matrix (which element do we want)

						weights_buffer_elem: for(sys_array_dim i = 0; i < SYSTOLIC_ARRAY_SIZE; i+=2){
							sys_array_dim j = i+1;

							cache_dim elem_idx_check = tau-0.5*i;

							if((i+h*8) < c_tile_size){
								//if(elem_idx >= 0 && (tau-i) < *Telem){
								if(elem_idx_check >= 0 && elem_idx_check < *Telem){
									// next element for input buffer
									sa_m_axi_output_temp_weights[h].range(i*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, i*MATRIX_DATA_WIDTH) = cache_weights_tile[elem_idx_w][i+h*8];
								}
								else{
						            // place a zero in buffer if not at the iteration yet for this
						            // element (and row/column of output matrix)
									sa_m_axi_output_temp_weights[h].range(i*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, i*MATRIX_DATA_WIDTH) = 0;
								}
							}
							else{
								sa_m_axi_output_temp_weights[h].range(i*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, i*MATRIX_DATA_WIDTH) = 0;
							}

							if((j+h*8) < c_tile_size){
								if(elem_idx_check >= 0 && elem_idx_check < *Telem){
									// next element for input buffer
									sa_m_axi_output_temp_weights[h].range(j*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, j*MATRIX_DATA_WIDTH) = cache_weights_tile[elem_idx_w][j+h*8];
									elem_idx_w--;
								}
								else{
						            // place a zero in buffer if not at the iteration yet for this
						            // element (and row/column of output matrix)
									sa_m_axi_output_temp_weights[h].range(j*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, j*MATRIX_DATA_WIDTH) = 0;
								}
							}
							else{
								sa_m_axi_output_temp_weights[h].range(j*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1, j*MATRIX_DATA_WIDTH) = 0;
							}

						}

					}
				}
				else{
					// last few beats in burst need to be sent as zeros
					sa_m_axi_output_temp_input = 0;
					sa_m_axi_output_temp_weights[0] = 0;
					sa_m_axi_output_temp_weights[1] = 0;
				}

				// AXI write transfers for all systolic arrays - 256-bit bus
				ap_uint<128> sa_m_axi_output_temp_weights_1 = sa_m_axi_output_temp_weights[0];
				sa_m_axi_output_1[beat].range(127,0) = sa_m_axi_output_temp_input;
				sa_m_axi_output_1[beat].range(255,128) = sa_m_axi_output_temp_weights_1;

				ap_uint<128> sa_m_axi_output_temp_weights_2 = sa_m_axi_output_temp_weights[1];
				sa_m_axi_output_2[beat].range(127,0) = sa_m_axi_output_temp_input;
				sa_m_axi_output_2[beat].range(255,128) = sa_m_axi_output_temp_weights_2;

			}

		}
		else{
			break;
		}

	}

	// read result - loop over different beats of the 8-beat burst - NEEDS MODIFIED IF WE CHOOSE DIFFERENT BURST SIZES ETC... CHANGING MACROS ISNT ENOUGH
	ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_temp_1;
	ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_temp_2;

	read_sa_output_rows_1: for(sys_array_dim i = 0; i < SYSTOLIC_ARRAY_SIZE/2; i++){
#pragma HLS PIPELINE II=1
		sa_m_axi_input_temp_1 = sa_m_axi_input_1[i];
		sa_m_axi_input_temp_2 = sa_m_axi_input_2[i];
		sys_array_dim row = i*2;
		sys_array_dim row_next = i*2+1;
		read_sa_output_cols_1: for(sys_array_dim j = 0; j < SYSTOLIC_ARRAY_SIZE*N_SYSTOLIC_ARRAYS; j++){
#pragma HLS DEPENDENCE variable=cache_output_FM_tile inter false
			sys_array_dim col_idx_1 = j;
			sys_array_dim col_idx_1_next = j + 8;
			sys_array_dim col_idx_2 = j - 8;
			sys_array_dim col_idx_2_next = j;
			if(i < r_tile_size){
				if(j < c_tile_size){
					if(j < 8){
						cache_output_FM_tile[row][j+c_start_index].data = sa_m_axi_input_temp_1.range(col_idx_1*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1,col_idx_1*MATRIX_DATA_WIDTH);
						cache_output_FM_tile[row_next][j+c_start_index].data = sa_m_axi_input_temp_1.range(col_idx_1_next*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1,col_idx_1_next*MATRIX_DATA_WIDTH);
						cache_output_FM_tile[row][j+c_start_index].last = 0;
						cache_output_FM_tile[row_next][j+c_start_index].last = 0;
					}
					else{
						cache_output_FM_tile[row][j+c_start_index].data = sa_m_axi_input_temp_2.range(col_idx_2*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1,col_idx_2*MATRIX_DATA_WIDTH);
						cache_output_FM_tile[row_next][j+c_start_index].data = sa_m_axi_input_temp_2.range(col_idx_2_next*MATRIX_DATA_WIDTH+MATRIX_DATA_WIDTH-1,col_idx_2_next*MATRIX_DATA_WIDTH);
						cache_output_FM_tile[row][j+c_start_index].last = 0;
						cache_output_FM_tile[row_next][j+c_start_index].last = 0;
					}
				}
				else{
					break;
				}
			}
			else{
				break;
			}
		}
	}

}
