// Ryan Greer
// 24/04/21
// systolic_arr_buffer_setup.h
// HLS header for systlic_arr_buffer_setup

#ifndef SYSTOLIC_ARR_BUFFER_SETUP_H__
#define SYSTOLIC_ARR_BUFFER_SETUP_H__

#include "conv_mm_control.h"

void get_cache_weights_matrix_tile(weights_values cache_weights[MAX_TELEM][MAX_TC],
	   	   	   	   	   	   	   	   sys_array_dim c_tile_size,
								   cache_dim c_start_index,
								   cache_dim elem_idx,
								   weights_values cache_weights_tile[SYSTOLIC_ARRAY_SIZE][SYSTOLIC_ARRAY_SIZE*N_SYSTOLIC_ARRAYS]);

void systolic_arr_buffer_setup(input_fm_values cache_input_FM_tile[SYSTOLIC_ARRAY_SIZE][MAX_TELEM],
							   weights_values cache_weights[MAX_TELEM][MAX_TC],
							   output_fm_AXIS_small cache_output_FM_tile[SYSTOLIC_ARRAY_SIZE][MAX_TC],
							   sys_array_dim r_tile_size,
							   sys_array_dim c_tile_size,
							   cache_dim c_start_index,
							   cache_dim *Telem,
							   ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_1[AXI_WRITE_BURST_SIZE],
							   ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_1[AXI_READ_BURST_SIZE],
							   ap_uint<AXI_DATA_LANE_WIDTH_WRITE> sa_m_axi_output_2[AXI_WRITE_BURST_SIZE],
							   ap_uint<AXI_DATA_LANE_WIDTH_READ> sa_m_axi_input_2[AXI_READ_BURST_SIZE]);

#endif
