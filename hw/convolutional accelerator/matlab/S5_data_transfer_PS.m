% Ryan Greer
% 24/04/21

%% Convolution (PS side) Data Preparation and Transfer to PL with Loop Tiling
rng(1);                                 % same data every time run

% === DATA TO PROCESS IN CONVOLUTION LAYER ===
input_FM = rand(15,10,8);               % 8 2D images with size 15 x 10 pixels
kernels = rand(3,3,8,16);               % 16 3x3 3D kernels to operate on 8 layer input FM
output_FM = zeros(15,10,16);            % output is 16 feature maps with spatial dimensinality of input

%% Method 1: 3D convolution (input FM is 3D tensor, kernels are 4D tensor
% ----------------------------------------------------------------------
% the most natural method with respect to the behaviour of convolution - no
% duplication of data occurs (so it has the smallest data transfer overhead)
% but the method involves more loops and is generally more complicated than 
% the matrix multiply methods
% tile sizes - make sure they split the tensor exactly for now...
r_tile_size = 15;
c_tile_size = 10;
o_tile_size = 2;
i_tile_size = 1;

r_itr = size(input_FM,1) / r_tile_size;
c_itr = size(input_FM,2) / c_tile_size;
o_itr = size(kernels,4) / o_tile_size;
i_itr = size(input_FM,3) / i_tile_size;

% 5 tiles of 3
for r_tile = 0:1:(r_itr-1)
    r_indices = (r_tile*r_tile_size+1:r_tile*r_tile_size+r_tile_size);
    % 2 tiles of 5
    for c_tile = 0:1:(c_itr-1)
        c_indices = (c_tile*c_tile_size+1:c_tile*c_tile_size+c_tile_size);
        % 4 tiles of 4
        for o_tile = 0:1:(o_itr-1)
            o_indices = (o_tile*o_tile_size+1):(o_tile*o_tile_size+o_tile_size);
            % 4 tiles of 2
            for i_tile = 0:1:(i_itr-1)
                i_indices = (i_tile*i_tile_size+1:i_tile*i_tile_size+i_tile_size);
                cache_out = zeros(r_tile_size,c_tile_size,o_tile_size);
                cache_in = input_FM(r_indices,c_indices,i_indices);
                cache_weights = kernels(:,:,i_indices,o_indices);
                % CALL THE FUNCTION TO SIMULATE PL
                cache_out = conv_3D(cache_in, cache_weights, cache_out, r_tile_size, c_tile_size, o_tile_size, i_tile_size);
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                output_FM(r_indices,c_indices,o_indices) = output_FM(r_indices,c_indices,o_indices) + cache_out;
            end
        end
    end
end

output_FM_conv = output_FM;

%% Method 2: Matrix multiply convolution with im2row expansion
% -----------------------------------------------------------
% input feature map is converted to a 2D matrix via the im2row expansion
% and the kernels are converted to a 2D matrix (by reducing the first 3
% dimensions into the first dimension and the 4th dimesnion into the 2nd).
% Therefore the kernels don't get duplicated and the order of memory access
% does not change (the conversion to a 2D matrix is just a different way to
% interpret the data). The im2row expansion does increase the amount of
% elements in the input feature map by duplication
% [the increase is by a factor of the amount of elements in the kernel spatially i.e. 9, 25, 49...]

input_FM_mtx = im2row_expansion(input_FM, 3, 3);
kernels_mtx = reshape(kernels, [3*3*8,16]);

% output_FM_mtx = conv_mm(input_FM_mtx, kernels_mtx);
output_FM_mtx = zeros(150,16);
r_tile_size = 50;
c_tile_size = 4;
elem_tile_size = 8;

r_itr = size(input_FM_mtx,1) / r_tile_size;
c_itr = size(kernels_mtx,2) / c_tile_size;
elem_itr = size(input_FM_mtx,2) / elem_tile_size;

% loop tiling for matrix multiply:
% 3 tiles of 50
for r_tile = 0:1:(r_itr-1)
    r_indices = (r_tile*r_tile_size+1:r_tile*r_tile_size+r_tile_size);
    % 4 tiles of 4
    for c_tile = 0:1:(c_itr-1)
        c_indices = (c_tile*c_tile_size+1:c_tile*c_tile_size+c_tile_size);
        % 9 tiles of 8
        for elem_tile = 0:1:(elem_itr-1)
            elem_indices = (elem_tile*elem_tile_size+1:elem_tile*elem_tile_size+elem_tile_size);
            % CALL THE FUNCTION TO SIMULATE PL
            cache_out = conv_mm(input_FM_mtx(r_indices,elem_indices), kernels_mtx(elem_indices,c_indices), 50, 4);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            output_FM_mtx(r_indices,c_indices) = output_FM_mtx(r_indices,c_indices) + cache_out;
        end
    end
end

output_FM = reshape(output_FM_mtx, [15,10,16]);
output_FM_mm = output_FM;

%% Method 2 with Systolic Array Matrix Multiplication

% convert input FM and weights to matrix format for multiplication
input_FM_mtx = im2row_expansion(input_FM, 3, 3);
kernels_mtx = reshape(kernels, [3*3*8,16]);

% output FM matrix format
output_FM_mtx = zeros(150,16);

% define tile sizes - these will be dictated by PL BRAM resources so if the
% matrix multiplication is suitably small, then these may equal the size of
% the matrices
r_tile_size = 50;
c_tile_size = 10;
elem_tile_size = 10;

% compute required number of tile iterations
r_itr = ceil(size(input_FM_mtx,1) / r_tile_size);
c_itr = ceil(size(kernels_mtx,2) / c_tile_size);
elem_itr = ceil(size(input_FM_mtx,2) / elem_tile_size);

% rows and columns remaining if tiles of size 8 are used
remain_row = mod(size(input_FM_mtx,1),r_tile_size);
remain_col = mod(size(kernels_mtx,2),c_tile_size);
remain_elem = mod(size(input_FM_mtx,2),elem_tile_size);

% loop tiling for matrix multiply:

% r_itr tiles of size r_tile_size or (r_itr - 1) tiles of size r_tile_size
% and one tile of size remain_row
for r_tile = 0:1:(r_itr-1)
    if(r_tile == r_itr-1 && remain_row > 0)
        % last loop - fewer rows than r_tile_size
        r_indices = (r_tile*r_tile_size+1:r_tile*r_tile_size+remain_row);
    else
        r_indices = (r_tile*r_tile_size+1:r_tile*r_tile_size+r_tile_size);
    end
    % c_itr tiles of size c_tile_size or (c_itr - 1) tiles of size c_tile_size
    % and one tile of size remain_col
    for c_tile = 0:1:(c_itr-1)
        if(c_tile == c_itr-1 && remain_col > 0)
            % last loop - fewer columns than c_tile_size
            c_indices = (c_tile*c_tile_size+1:c_tile*c_tile_size+remain_col);
        else
            c_indices = (c_tile*c_tile_size+1:c_tile*c_tile_size+c_tile_size);
        end
        % elem_itr tiles of size elem_tile_size or (elem_itr - 1) tiles of size elem_tile_size
        % and one tile of size remain_elem
        for elem_tile = 0:1:(elem_itr-1)
            if(elem_tile == elem_itr-1 && remain_elem > 0)
                % last loop - fewer elements than elem_tile_size
                elem_indices = (elem_tile*elem_tile_size+1:elem_tile*elem_tile_size+remain_elem);
            else
                elem_indices = (elem_tile*elem_tile_size+1:elem_tile*elem_tile_size+elem_tile_size);
            end                 
            
            % Call the function to simulate PL operations
            cache_out = systolic_array_control_pl(input_FM_mtx(r_indices,elem_indices), kernels_mtx(elem_indices,c_indices));
            
            % add cache_out tile to full output matrix
            output_FM_mtx(r_indices,c_indices) = output_FM_mtx(r_indices,c_indices) + cache_out;
        end
    end
end

% reshape output matrix to 3D tensor
output_FM = reshape(output_FM_mtx, [15,10,16]);
output_FM_mm_systolic = output_FM;