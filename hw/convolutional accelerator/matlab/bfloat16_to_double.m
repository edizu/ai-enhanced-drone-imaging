% Ryan Greer
% 24/04/21

function out = bfloat16_to_double(in)
% BFLOAT16_TO_DOUBLE returns an array which is the input converted fro bits
% (bfloat16) to double

binary_value = dec2bin(in,16);

bfloat16_sign = bin2dec(binary_value(:,1));
bfloat16_exponent = bin2dec(binary_value(:,2:9));
bfloat16_mantissa = bin2dec(binary_value(:,10:16));

out = (-1).^bfloat16_sign .* (bfloat16_mantissa/128 + 1) .* 2.^(bfloat16_exponent - 127);
out = reshape(out,size(in));

end

