% Ryan Greer
% 24/04/21

function cache_out = conv_3D(cache_in,cache_weights,cache_out, Tr, Tc, To, Ti)
% CONV_3D computes a tile of the 3D convolution of input feature map with a set of
% kernels

% size of kernel spatial
K1 = 3;
K2 = 3;

% I assume caches are full size of arrays initially
for k1 = -1:1:1
    for k2 = -1:1:1
        for r = 1:1:Tr
            % column loop will be pipelined
            for c = 1:1:Tc
                % loops below will be fully parallelised - operating on
                % systolic array PEs
                for o = 1:1:To
                    for i = 1:1:Ti
                        % ignore the border of images for now - implement
                        % zero padding later on (or just assume zero
                        % padding done already...)
                        % IMPORTANT - WE ARE WORKING WITH IMAGE TILES -
                        % MAY DISTORT OUTPUT FEATURE MAPS SPATIALLY (MINOR
                        % DISTORTION MAY BE ACCEPTABLE...?) - or overlap
                        % the tiles spatially
                        if(r == 1 || c == 1 || r == Tr || c == Tc)
                            continue;
                        else
                            cache_out(r,c,o) = cache_out(r,c,o) + cache_weights(k1+2,k2+2,i,o) * cache_in(r+k1,c+k2,i);
                        end
                    end
                end
            end
        end
    end
end

end
