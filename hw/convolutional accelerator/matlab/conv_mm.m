% Ryan Greer
% 24/04/21

function cache_out = conv_mm(cache_in,cache_weights,Tr,Tc)
% CONV_MM computes the matrix multiplication of two inputs (the input
% feature map and and weights. This is a tiled matrix multiplication

cache_out = zeros(size(cache_in,1), size(cache_weights,2));

for o_row = 1:1:Tr
    for o_col = 1:1:Tc
        for elem = 1:1:size(cache_in,2)
            cache_out(o_row, o_col) = cache_out(o_row, o_col) + cache_in(o_row, elem) * cache_weights(elem, o_col);
        end
    end
end

end