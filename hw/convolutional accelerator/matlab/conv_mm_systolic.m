% Ryan Greer
% 24/04/21

function cache_out = conv_mm_systolic(cache_in,cache_weights,Tr,Tc,sys_dim)
% CONV_MM_SYSTOLIC computes the matrix multiplication of two inputs (the input
% feature map and and weights. This is a tiled matrix multiplication
% fixed systolic array of 8x8 - input arrays can be smaller
% Tr and Tc are rows and columns of first and second matrices respecitvely
% ***inner dimensions can be any size***
% ref: http://ecelabs.njit.edu/ece459/lab3.php

T_elem = size(cache_in,2);      % number of columns of first matrix or number of rows of second (same)

cache_in_buffer = zeros(sys_dim,1);                              % horizontal buffer
cache_weights_buffer = zeros(1,sys_dim);                    % vertical buffer

% define interconnects at input of respective PE
pe_interconnects_hor = zeros(sys_dim,sys_dim);
pe_interconnects_ver = zeros(sys_dim,sys_dim);
cache_out = zeros(Tr,Tc);

for tau = 0:1:(max(T_elem+2*Tr-2,T_elem+2*Tc)-1)
    % send next element to buffer (data is initaially in memory (BRAM) or
    % being streamed from AXIS (i.e. for the first matrix)
    for i = 0:1:(sys_dim-1)
        % iterates over rows of first matrix and columns of second matrix -
        % i.e. iterates through the buffer entries - can be UNROLLED in hardware
        elem_idx = tau - i;       % which element of matrix do we want
        
        if(i < Tr)
            if(elem_idx >= 0 && elem_idx < T_elem)
                cache_in_buffer(i+1,1) = cache_in(i+1,elem_idx+1);
            else
                cache_in_buffer(i+1,1) = 0;
            end            
        end
        if(i < Tc)
            if(elem_idx >= 0 && elem_idx < T_elem)
                cache_weights_buffer(1,i+1) = cache_weights(elem_idx+1,i+1);
            else
                cache_weights_buffer(1,i+1) = 0;
            end          
        end

    end
    
    % update interconnects for this set of calculations
    pe_interconnects_hor(:,2:sys_dim) = pe_interconnects_hor(:,1:sys_dim-1);
    pe_interconnects_hor(:,1) = cache_in_buffer(:,1);
    
    pe_interconnects_ver(2:sys_dim,:) = pe_interconnects_ver(1:sys_dim-1,:);
    pe_interconnects_ver(1,:) = cache_weights_buffer(1,:);
    
    % do the computation - sys_dim*sys_dim parallel multiplications and
    % accumulation
    cache_out = cache_out + pe_interconnects_hor(1:Tr,1:Tc) .* pe_interconnects_ver(1:Tr,1:Tc);
end

end
