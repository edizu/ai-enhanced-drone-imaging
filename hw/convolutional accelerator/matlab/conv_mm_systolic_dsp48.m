% Ryan Greer
% 24/04/21

function cache_out = conv_mm_systolic_dsp48(cache_in,cache_weights,sys_dim)
% CONV_MM_SYSTOLIC_DSP48 computes the matrix multiplication of two inputs (the input
% feature map and and weights - result is sys_dim x sys_dim size matrix.
% this function uses the DSP48 'trick' so implements a 2-layer systolic
% array
% ***inner dimensions can be any size***
% the buffers are prepared for the systolic array
% ref for systolic array theory (our implementation is custom): http://ecelabs.njit.edu/ece459/lab3.php

T_elem = size(cache_in,2);                                  % number of columns of first matrix or number of rows of second (same)

% these 4 lines pad the input matrices with zeros if they are smaller than
% 8x8 - this will not be required in hardware
cache_in = [cache_in,zeros(size(cache_in,1),sys_dim - size(cache_in,2))];
cache_in = [cache_in;zeros(sys_dim - size(cache_in,1),size(cache_in,2))];
cache_weights = [cache_weights,zeros(size(cache_weights,1),sys_dim - size(cache_weights,2))];
cache_weights = [cache_weights;zeros(sys_dim - size(cache_weights,1),size(cache_weights,2))];

cache_in_buffer = zeros(sys_dim,1);                         % horizontal buffer
cache_weights_buffer = zeros(2,sys_dim/2);                  % vertical buffer

% define interconnects at input of respective PE
pe_interconnects_hor = zeros(sys_dim,sys_dim/2,2);          % matrix sizes simulate a '3D systolic array' for simplicity
pe_interconnects_ver = zeros(sys_dim,sys_dim/2,2);
cache_out = zeros(sys_dim,sys_dim);

% value in loop bound is the maximum number of iterations to compute the
% full matrix multiplication (zero to no. itr minus one)
for tau = 0:1:(T_elem+2*sys_dim-5)
    % send next element to buffer (data is initaially in memory (BRAM) or
    % being streamed from AXIS (i.e. for the first matrix)
    weight_col_idx = 0;
    for i = 0:1:(sys_dim-1)
        % iterates over rows of first matrix and columns of second matrix -
        % i.e. iterates through the buffer entries - can be UNROLLED in hardware
        elem_idx = tau - i;       % which element of matrix do we want
        if(elem_idx >= 0 && elem_idx < T_elem)
            % next element for input buffer
            cache_in_buffer(i+1,1) = cache_in(i+1,elem_idx+1);
            % next element for 2 weights buffers
            if(i < sys_dim/2)
                cache_weights_buffer(1,i+1) = cache_weights(elem_idx+1,weight_col_idx+1);
                cache_weights_buffer(2,i+1) = cache_weights(elem_idx+1,weight_col_idx+2);
            end
        else
            % place a zero in buffer if not at the iteration yet for this
            % element (and row/column of output matrix)
            cache_in_buffer(i+1,1) = 0;
            if(i < sys_dim/2)
                cache_weights_buffer(1,i+1) = 0;
                cache_weights_buffer(2,i+1) = 0;
            end
        end
        weight_col_idx = weight_col_idx + 2;
    end
    
    % update interconnects for this set of calculations
    pe_interconnects_hor(:,2:sys_dim/2,:) = pe_interconnects_hor(:,1:sys_dim/2-1,:);
    pe_interconnects_hor(:,1,1) = cache_in_buffer(:,1);
    pe_interconnects_hor(:,1,2) = cache_in_buffer(:,1);
    
    pe_interconnects_ver(2:sys_dim,:,:) = pe_interconnects_ver(1:sys_dim-1,:,:);
    pe_interconnects_ver(1,:,1) = cache_weights_buffer(1,:);
    pe_interconnects_ver(1,:,2) = cache_weights_buffer(2,:);
    
    % do the computation - sys_dim*sys_dim parallel multiplications and
    % accumulation
    % this line of code is arranging the result into 8x8 2D (as we have
    % used 3D arrays in this function for visualiations purposes) - in the
    % hardware design, complicated code like this is not needed
    cache_out = cache_out + reshape([pe_interconnects_hor(:,:,1);pe_interconnects_hor(:,:,2)] .* [pe_interconnects_ver(:,:,1);pe_interconnects_ver(:,:,2)],[8 8]);
end

end
