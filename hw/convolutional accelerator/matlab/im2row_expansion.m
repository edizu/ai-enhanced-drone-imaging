% Ryan Greer
% 24/04/21

function [input_FM_mtx] = im2row_expansion(input_FM, kernel_height, kernel_width)
% IM2ROW_EXPANSION Computes the im2row expansion of an input feature map
% which is a 3D tensor (rows x cols x input_frames). The result is a 2D matrix
% (rows*cols x kernel_height*kernel_width*input_frames)

rows = size(input_FM,1);
cols = size(input_FM,2);
input_frames = size(input_FM,3);

% result will 
input_FM_mtx = zeros(rows*cols,kernel_height*kernel_width*input_frames);
input_FM_mtx_row_indx = 0;

% compute the im2row expansion
for col = 1:1:cols
    for row = 1:1:rows
        input_FM_mtx_row_indx = input_FM_mtx_row_indx + 1;
        % we will put zeros in the matrix for locations outwith the borders
        % of the image (i.e. leave the matrix alone)
        if((row-1 < 1) || (col-1 < 1) ||  (row+1 > rows) || (col+1 > cols))
            continue;
        end
        % im2row expansion for one row of input feature map
        input_FM_mtx(input_FM_mtx_row_indx,:) = reshape(input_FM(row-floor(kernel_height/2):row+floor(kernel_height/2),col-floor(kernel_width/2):col+floor(kernel_width/2),:),[kernel_height*kernel_width*input_frames,1]);
    end
end

end
