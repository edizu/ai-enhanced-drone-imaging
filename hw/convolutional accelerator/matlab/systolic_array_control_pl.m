% Ryan Greer
% 24/04/21

function cache_out = systolic_array_control_pl(cache_in,cache_weights)
% SYSTOLIC_ARRAY_CONTROL_PL Implements tiling in the h/w to get correct
% matrix sizes to systolic array buffer preparation function
% (conv_mm_systolic_dsp48)
% - multiplies "cache_in" and "cache_weights" matrices
% the number of elements (i.e the columns in first matrix or rows in second) 
% does not need cached as there is no limit to this in the systolic array

% ROW OR COLUMN LOOP CAN BE UNROLLED BY A CERTAIN FACTOR DEPENDING HOW MANY
% SYSTOLIC ARRAYS ARE AVAILABLE

% rows and columns of input and weights matrix caches
Tr = size(cache_in,1);
Tc = size(cache_weights,2);

sytolic_array_size = 8;                 % i.e. an 8x8 systolic array

cache_out = zeros(Tr,Tc);

% tile sizes must match (or be lower than) dimension of systolic array
r_tile_size = sytolic_array_size;
c_tile_size = sytolic_array_size;

% number of iterations assuming systolic array of size given above
% note: iterations could be done in parallel as more than one systolic
% array will be available
r_itr = ceil(size(cache_in,1) / r_tile_size);
c_itr = ceil(size(cache_weights,2) / c_tile_size);

% rows and columns remaining if tiles of size 8 are used
remain_row = mod(size(cache_in,1),r_tile_size);
remain_col = mod(size(cache_weights,2),c_tile_size);

% loop tiling for systolic array:
for r_tile = 0:1:(r_itr-1)
    if(r_tile == r_itr-1 && remain_row > 0)
        r_indices = (r_tile*r_tile_size+1:r_tile*r_tile_size+remain_row);
    else
        r_indices = (r_tile*r_tile_size+1:r_tile*r_tile_size+r_tile_size);
    end
    for c_tile = 0:1:(c_itr-1)
        if(c_tile == c_itr-1 && remain_col > 0)
            c_indices = (c_tile*c_tile_size+1:c_tile*c_tile_size+remain_col);
        else
            c_indices = (c_tile*c_tile_size+1:c_tile*c_tile_size+c_tile_size);
        end        
        % CALL THE FUNCTION TO GET DATA TO SYSTOLIC ARRAY
        temp_out = conv_mm_systolic_dsp48(cache_in(r_indices,:), cache_weights(:,c_indices), sytolic_array_size);
        
        if(r_tile == r_itr-1 && remain_row > 0)
            temp_out = temp_out(1:1:remain_row,:);
        end
        if(c_tile == c_itr-1 && remain_col > 0)
            temp_out = temp_out(:,1:1:remain_col);
        end
            
        % add output to current cache_out
        cache_out(r_indices,c_indices) = temp_out;
    end
end

end

