% Ryan Greer
% 24/04/21

%% Writes cache input and cache weights matrices into '.dat' files which can be used by the HLS C sim and cosim testbenches
% data is floating point (single precision) initially

% define path to .dat files
path_name = 'C:\Users\ryan_\Documents\University\Project\MATLAB\MatrixData\';
% path_name = '\\192.168.3.1\xilinx\jupyter_notebooks\conv_mm_systolic\';

% define sizes of matrices
Tr = 8;
Tc = 64;
Telem = 8;

% define matrices
% rng(1);
cache_input_matrix = randi(8,Tr,Telem);
cache_weights_matrix = randi(8,Telem,Tc);
cache_output_matrix = cache_input_matrix * cache_weights_matrix;

% WRITE FILE - CACHE_INPUT_MATRIX
cache_input_matrix_path = strcat(path_name, 'cache_input_matrix.dat');
cache_input_matrix_file = fopen(cache_input_matrix_path, 'w');

for r = 1:1:Tr
    for c = 1:1:Telem
        fprintf(cache_input_matrix_file, '%d ', cache_input_matrix(r,c));
    end
    fprintf(cache_input_matrix_file, '\n');
end

fclose(cache_input_matrix_file);

% WRITE FILE - CACHE_WEIGHTS_MATRIX
cache_weights_matrix_path = strcat(path_name, 'cache_weights_matrix.dat');
cache_weights_matrix_file = fopen(cache_weights_matrix_path, 'w');

for r = 1:1:Telem
    for c = 1:1:Tc
        fprintf(cache_weights_matrix_file, '%d ', cache_weights_matrix(r,c));        
    end
    fprintf(cache_weights_matrix_file, '\n');
end

fclose(cache_weights_matrix_file);

% WRITE FILE - CACHE_OUTPUT_MATRIX
cache_output_matrix_path = strcat(path_name, 'cache_output_matrix.dat');
cache_output_matrix_file = fopen(cache_output_matrix_path, 'w');

for r = 1:1:Tr
    for c = 1:1:Tc
        fprintf(cache_output_matrix_file, '%d ', cache_output_matrix(r,c));        
    end
    fprintf(cache_output_matrix_file, '\n');
end

fclose(cache_output_matrix_file);

% WRITE FILE - CACHE_MATRIX_DIM
cache_matrix_dim_path = strcat(path_name, 'cache_matrix_dim.dat');
cache_matrix_dim_file = fopen(cache_matrix_dim_path, 'w');

fprintf(cache_matrix_dim_file, '%d \n', Tr);
fprintf(cache_matrix_dim_file, '%d \n', Tc);
fprintf(cache_matrix_dim_file, '%d \n', Telem);

fclose(cache_matrix_dim_file);