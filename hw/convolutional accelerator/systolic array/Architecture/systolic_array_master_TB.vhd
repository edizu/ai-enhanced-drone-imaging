-- Ryan Greer
-- 24/04/21
-- systolic_array_master_TB.vhd
-- instantiates the AXI4 master interface and systolic array (slave), implements testbench
-- based from Vivado AXI4 interface template code

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.systolic_array_pkg.all;

entity systolic_array_master_TB is
--  Port ( );
end systolic_array_master_TB;

architecture systolic_array_master_TB_impl of systolic_array_master_TB is

	-- master AXI testbench parameter constants
	constant C_M00_AXI_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
	constant C_M00_AXI_BURST_LEN_WRITE	: integer	:= 1;
	constant C_M00_AXI_BURST_LEN_READ	: integer	:= 4;
	constant C_M00_AXI_ID_WIDTH	: integer	:= 0;
	constant C_M00_AXI_ADDR_WIDTH	: integer	:= 32;
	constant C_M00_AXI_DATA_WIDTH	: integer	:= 256;
	constant C_M00_AXI_AWUSER_WIDTH	: integer	:= 0;
	constant C_M00_AXI_ARUSER_WIDTH	: integer	:= 0;
	constant C_M00_AXI_WUSER_WIDTH	: integer	:= 0;
	constant C_M00_AXI_RUSER_WIDTH	: integer	:= 0;
	constant C_M00_AXI_BUSER_WIDTH	: integer	:= 0;
	
	---- master AXI testbench signals ----
	-- GLOBAL SIGNALS --
	signal m00_axi_aclk	: std_logic;
	signal m00_axi_aresetn	: std_logic;
	
	-- MISCELLANEOUS SIGNALS --
    signal m00_axi_init_axi_txn	: std_logic;
	signal m00_axi_txn_done	: std_logic;
	signal m00_axi_error	: std_logic;
	
	-- WRITE ADDRESS CHANNEL SIGNALS --
	signal m00_axi_awid	    : std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
	signal m00_axi_awaddr	: std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
	signal m00_axi_awlen	: std_logic_vector(7 downto 0);
	signal m00_axi_awsize	: std_logic_vector(2 downto 0);
	signal m00_axi_awburst	: std_logic_vector(1 downto 0);
	signal m00_axi_awlock	: std_logic;
	signal m00_axi_awcache	: std_logic_vector(3 downto 0);
	signal m00_axi_awprot	: std_logic_vector(2 downto 0);
	signal m00_axi_awqos	: std_logic_vector(3 downto 0);
	signal m00_axi_awuser	: std_logic_vector(C_M00_AXI_AWUSER_WIDTH-1 downto 0);
	signal m00_axi_awvalid	: std_logic;
	signal m00_axi_awready	: std_logic;	 
	signal m00_axi_awregion	: std_logic_vector(3 downto 0);		                      -- not included by IP packager
	
	-- WRITE DATA CHANNEL SIGNALS
    signal m00_axi_wid	    : std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);	
	signal m00_axi_wdata	: std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
	signal m00_axi_wstrb	: std_logic_vector(C_M00_AXI_DATA_WIDTH/8-1 downto 0);
	signal m00_axi_wlast	: std_logic;
	signal m00_axi_wuser	: std_logic_vector(C_M00_AXI_WUSER_WIDTH-1 downto 0);
	signal m00_axi_wvalid	: std_logic;
	signal m00_axi_wready	: std_logic;
	
	-- WRITE RESPONSE CHANNEL SIGNALS --
	signal m00_axi_bid	: std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
	signal m00_axi_bresp	: std_logic_vector(1 downto 0);
	signal m00_axi_buser	: std_logic_vector(C_M00_AXI_BUSER_WIDTH-1 downto 0);
	signal m00_axi_bvalid	: std_logic;
	signal m00_axi_bready	: std_logic;	
		
	-- READ ADDRESS CHANNEL SIGNALS --
	signal m00_axi_arid	: std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
	signal m00_axi_araddr	: std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
	signal m00_axi_arlen	: std_logic_vector(7 downto 0);
	signal m00_axi_arsize	: std_logic_vector(2 downto 0);
	signal m00_axi_arburst	: std_logic_vector(1 downto 0);
	signal m00_axi_arlock	: std_logic;
	signal m00_axi_arcache	: std_logic_vector(3 downto 0);
	signal m00_axi_arprot	: std_logic_vector(2 downto 0);
	signal m00_axi_arqos	: std_logic_vector(3 downto 0);
	signal m00_axi_arregion	: std_logic_vector(3 downto 0);                        -- not included by IP packager
	signal m00_axi_aruser	: std_logic_vector(C_M00_AXI_ARUSER_WIDTH-1 downto 0);
	signal m00_axi_arvalid	: std_logic;
	signal m00_axi_arready	: std_logic;

    -- READ ADDRESS CHANNEL SIGNALS --
	signal m00_axi_rid	: std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
	signal m00_axi_rdata	: std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
	signal m00_axi_rresp	: std_logic_vector(1 downto 0);
	signal m00_axi_rlast	: std_logic;
	signal m00_axi_ruser	: std_logic_vector(C_M00_AXI_RUSER_WIDTH-1 downto 0);
	signal m00_axi_rvalid	: std_logic;
	signal m00_axi_rready	: std_logic;
	
	-- master interface component declaration
	-- component declaration
	component systolic_array_master_TB_M00_AXI is
		generic (
		C_M_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_M_AXI_BURST_LEN_WRITE	: integer	:= 2;
		C_M_AXI_BURST_LEN_READ	: integer	:= 8;
		C_M_AXI_ID_WIDTH	: integer	:= 1;
		C_M_AXI_ADDR_WIDTH	: integer	:= 32;
		C_M_AXI_DATA_WIDTH	: integer	:= 32;
		C_M_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_M_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_M_AXI_WUSER_WIDTH	: integer	:= 0;
		C_M_AXI_RUSER_WIDTH	: integer	:= 0;
		C_M_AXI_BUSER_WIDTH	: integer	:= 0
		);
		port (
		INIT_AXI_TXN	: in std_logic;
		TXN_DONE	: out std_logic;
		ERROR	: out std_logic;
		M_AXI_ACLK	: in std_logic;
		M_AXI_ARESETN	: in std_logic;
		M_AXI_AWID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_AWLEN	: out std_logic_vector(7 downto 0);
		M_AXI_AWSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_AWBURST	: out std_logic_vector(1 downto 0);
		M_AXI_AWLOCK	: out std_logic;
		M_AXI_AWCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		M_AXI_AWQOS	: out std_logic_vector(3 downto 0);
		M_AXI_AWUSER	: out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
		M_AXI_AWVALID	: out std_logic;
		M_AXI_AWREADY	: in std_logic;
		M_AXI_WDATA	: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_WSTRB	: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		M_AXI_WLAST	: out std_logic;
		M_AXI_WUSER	: out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
		M_AXI_WVALID	: out std_logic;
		M_AXI_WREADY	: in std_logic;
		M_AXI_BID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_BRESP	: in std_logic_vector(1 downto 0);
		M_AXI_BUSER	: in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
		M_AXI_BVALID	: in std_logic;
		M_AXI_BREADY	: out std_logic;
		M_AXI_ARID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_ARADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_ARLEN	: out std_logic_vector(7 downto 0);
		M_AXI_ARSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_ARBURST	: out std_logic_vector(1 downto 0);
		M_AXI_ARLOCK	: out std_logic;
		M_AXI_ARCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_ARPROT	: out std_logic_vector(2 downto 0);
		M_AXI_ARQOS	: out std_logic_vector(3 downto 0);
		M_AXI_ARUSER	: out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
		M_AXI_ARVALID	: out std_logic;
		M_AXI_ARREADY	: in std_logic;
		M_AXI_RID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_RDATA	: in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_RRESP	: in std_logic_vector(1 downto 0);
		M_AXI_RLAST	: in std_logic;
		M_AXI_RUSER	: in std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
		M_AXI_RVALID	: in std_logic;
		M_AXI_RREADY	: out std_logic
		);
	end component systolic_array_master_TB_M00_AXI;	

	-- DUT component declaration
	component systolic_array_v1 is
		generic (
		C_S00_AXI_ID_WIDTH	: integer	:= 1;
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6;
		C_S00_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_WUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_RUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_BUSER_WIDTH	: integer	:= 0		
		);
		port (
        s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		--s00_axi_awid	: in std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awlen	: in std_logic_vector(7 downto 0);
		s00_axi_awsize	: in std_logic_vector(2 downto 0);
		s00_axi_awburst	: in std_logic_vector(1 downto 0);
		s00_axi_awlock	: in std_logic;
		s00_axi_awcache	: in std_logic_vector(3 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awqos	: in std_logic_vector(3 downto 0);
		s00_axi_awregion	: in std_logic_vector(3 downto 0);
		--s00_axi_awuser	: in std_logic_vector(C_S00_AXI_AWUSER_WIDTH-1 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wlast	: in std_logic;
		--s00_axi_wuser	: in std_logic_vector(C_S00_AXI_WUSER_WIDTH-1 downto 0);
		s00_axi_wvalid	: in std_logic; 
		s00_axi_wready	: out std_logic;
		--s00_axi_bid	: out std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		--s00_axi_buser	: out std_logic_vector(C_S00_AXI_BUSER_WIDTH-1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		--s00_axi_arid	: in std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arlen	: in std_logic_vector(7 downto 0);
		s00_axi_arsize	: in std_logic_vector(2 downto 0);
		s00_axi_arburst	: in std_logic_vector(1 downto 0);
		s00_axi_arlock	: in std_logic;
		s00_axi_arcache	: in std_logic_vector(3 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arqos	: in std_logic_vector(3 downto 0);
		s00_axi_arregion	: in std_logic_vector(3 downto 0);
		--s00_axi_aruser	: in std_logic_vector(C_S00_AXI_ARUSER_WIDTH-1 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		--s00_axi_rid	: out std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rlast	: out std_logic;
		--s00_axi_ruser	: out std_logic_vector(C_S00_AXI_RUSER_WIDTH-1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic		
		);
	end component systolic_array_v1;
	
begin

-- Instantiation of Axi Bus Interface M00_AXI
systolic_array_master_TB_M00_AXI_inst : systolic_array_master_TB_M00_AXI
	generic map (
		C_M_TARGET_SLAVE_BASE_ADDR	=> C_M00_AXI_TARGET_SLAVE_BASE_ADDR,
		C_M_AXI_BURST_LEN_WRITE	=> C_M00_AXI_BURST_LEN_WRITE,
		C_M_AXI_BURST_LEN_READ	=> C_M00_AXI_BURST_LEN_READ,
		C_M_AXI_ID_WIDTH	=> C_M00_AXI_ID_WIDTH,
		C_M_AXI_ADDR_WIDTH	=> C_M00_AXI_ADDR_WIDTH,
		C_M_AXI_DATA_WIDTH	=> C_M00_AXI_DATA_WIDTH,
		C_M_AXI_AWUSER_WIDTH	=> C_M00_AXI_AWUSER_WIDTH,
		C_M_AXI_ARUSER_WIDTH	=> C_M00_AXI_ARUSER_WIDTH,
		C_M_AXI_WUSER_WIDTH	=> C_M00_AXI_WUSER_WIDTH,
		C_M_AXI_RUSER_WIDTH	=> C_M00_AXI_RUSER_WIDTH,
		C_M_AXI_BUSER_WIDTH	=> C_M00_AXI_BUSER_WIDTH
	)
	port map (
		INIT_AXI_TXN	=> m00_axi_init_axi_txn,
		TXN_DONE	=> m00_axi_txn_done,
		ERROR	=> m00_axi_error,
		M_AXI_ACLK	=> m00_axi_aclk,
		M_AXI_ARESETN	=> m00_axi_aresetn,
		M_AXI_AWID	=> m00_axi_awid,
		M_AXI_AWADDR	=> m00_axi_awaddr,
		M_AXI_AWLEN	=> m00_axi_awlen,
		M_AXI_AWSIZE	=> m00_axi_awsize,
		M_AXI_AWBURST	=> m00_axi_awburst,
		M_AXI_AWLOCK	=> m00_axi_awlock,
		M_AXI_AWCACHE	=> m00_axi_awcache,
		M_AXI_AWPROT	=> m00_axi_awprot,
		M_AXI_AWQOS	=> m00_axi_awqos,
		M_AXI_AWUSER	=> m00_axi_awuser,
		M_AXI_AWVALID	=> m00_axi_awvalid,
		M_AXI_AWREADY	=> m00_axi_awready,
		M_AXI_WDATA	=> m00_axi_wdata,
		M_AXI_WSTRB	=> m00_axi_wstrb,
		M_AXI_WLAST	=> m00_axi_wlast,
		M_AXI_WUSER	=> m00_axi_wuser,
		M_AXI_WVALID	=> m00_axi_wvalid,
		M_AXI_WREADY	=> m00_axi_wready,
		M_AXI_BID	=> m00_axi_bid,
		M_AXI_BRESP	=> m00_axi_bresp,
		M_AXI_BUSER	=> m00_axi_buser,
		M_AXI_BVALID	=> m00_axi_bvalid,
		M_AXI_BREADY	=> m00_axi_bready,
		M_AXI_ARID	=> m00_axi_arid,
		M_AXI_ARADDR	=> m00_axi_araddr,
		M_AXI_ARLEN	=> m00_axi_arlen,
		M_AXI_ARSIZE	=> m00_axi_arsize,
		M_AXI_ARBURST	=> m00_axi_arburst,
		M_AXI_ARLOCK	=> m00_axi_arlock,
		M_AXI_ARCACHE	=> m00_axi_arcache,
		M_AXI_ARPROT	=> m00_axi_arprot,
		M_AXI_ARQOS	=> m00_axi_arqos,
		M_AXI_ARUSER	=> m00_axi_aruser,
		M_AXI_ARVALID	=> m00_axi_arvalid,
		M_AXI_ARREADY	=> m00_axi_arready,
		M_AXI_RID	=> m00_axi_rid,
		M_AXI_RDATA	=> m00_axi_rdata,
		M_AXI_RRESP	=> m00_axi_rresp,
		M_AXI_RLAST	=> m00_axi_rlast,
		M_AXI_RUSER	=> m00_axi_ruser,
		M_AXI_RVALID	=> m00_axi_rvalid,
		M_AXI_RREADY	=> m00_axi_rready
	);

    -- instantiate master AXI slave DUT
    -- master (testbench) will WRITE to this DUT - this is a read/write DUT but only using write channels for now...
    systolic_array_v1_inst : systolic_array_v1
    generic map(
		C_S00_AXI_ID_WIDTH => C_M00_AXI_ID_WIDTH,
		C_S00_AXI_DATA_WIDTH	=> C_M00_AXI_DATA_WIDTH,
		C_S00_AXI_ADDR_WIDTH	=> C_M00_AXI_ADDR_WIDTH
    )
    port map(
		s00_axi_aclk	=> m00_axi_aclk,
		s00_axi_aresetn	=> m00_axi_aresetn,
		--s00_axi_awid	=> m00_axi_awid,
		s00_axi_awaddr	=> m00_axi_awaddr,
		s00_axi_awlen	=> m00_axi_awlen,
		s00_axi_awsize	=> m00_axi_awsize,
		s00_axi_awburst	=> m00_axi_awburst,
		s00_axi_awlock	=> m00_axi_awlock,
		s00_axi_awcache	=> m00_axi_awcache,
		s00_axi_awprot	=> m00_axi_awprot,
		s00_axi_awqos	=> m00_axi_awqos,
	    s00_axi_awregion	=> m00_axi_awregion,
		--s00_axi_awuser	=> m00_axi_awuser,
		s00_axi_awvalid	=> m00_axi_awvalid,
		s00_axi_awready	=> m00_axi_awready,
		s00_axi_wdata	=> m00_axi_wdata,
		s00_axi_wstrb	=> m00_axi_wstrb,
		s00_axi_wlast	=> m00_axi_wlast,
		--s00_axi_wuser	=> m00_axi_wuser,
		s00_axi_wvalid	=> m00_axi_wvalid,
		s00_axi_wready	=> m00_axi_wready,
		--s00_axi_bid	=> m00_axi_bid,
		s00_axi_bresp	=> m00_axi_bresp,
		--s00_axi_buser	=> m00_axi_buser,
		s00_axi_bvalid	=> m00_axi_bvalid,
		s00_axi_bready	=> m00_axi_bready,
		--s00_axi_arid	=> m00_axi_arid,
		s00_axi_araddr	=> m00_axi_araddr,
		s00_axi_arlen	=> m00_axi_arlen,
		s00_axi_arsize	=> m00_axi_arsize,
		s00_axi_arburst	=> m00_axi_arburst,
		s00_axi_arlock	=> m00_axi_arlock,
		s00_axi_arcache	=> m00_axi_arcache,
		s00_axi_arprot	=> m00_axi_arprot,
		s00_axi_arqos	=> m00_axi_arqos,
		s00_axi_arregion	=> m00_axi_arregion,
		--s00_axi_aruser	=> m00_axi_aruser,
		s00_axi_arvalid	=> m00_axi_arvalid,
		s00_axi_arready	=> m00_axi_arready,
		--s00_axi_rid	=> m00_axi_rid,
		s00_axi_rdata	=> m00_axi_rdata,
		s00_axi_rresp	=> m00_axi_rresp,
		s00_axi_rlast	=> m00_axi_rlast,
		--s00_axi_ruser	=> m00_axi_ruser,
		s00_axi_rvalid	=> m00_axi_rvalid,
		s00_axi_rready	=> m00_axi_rready
    );
    
    -- testbench stimuli generation
    reset_gen: process
    begin
        m00_axi_aresetn <= '0';      
        wait for 10ns;
        m00_axi_aresetn <= '1';  
        wait;
    end process reset_gen;
    
 	clock_gen: process
	begin
	   while now < 3000ns loop
	       m00_axi_aclk <= '0';
	       wait for 5ns;
	       m00_axi_aclk <= '1';
	       wait for 5ns;
       end loop;
       wait;
    end process clock_gen;
    
    test_stim_gen: process
    begin
        
        -- initiate a transaction
        m00_axi_init_axi_txn <= '1';
        wait for 20ns;
        m00_axi_init_axi_txn <= '0';
        wait for 780ns;
        m00_axi_init_axi_txn <= '1';
        wait for 20ns;
        m00_axi_init_axi_txn <= '0';

        wait for 10ns;
        wait;
    end process test_stim_gen;
    
end systolic_array_master_TB_impl;
