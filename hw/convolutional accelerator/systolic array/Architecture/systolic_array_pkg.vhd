-- Ryan Greer
-- 24/04/21
-- systolic_array_pkg.vhd
-- provides type declarations for signals used in the systolic array

library work;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package systolic_array_pkg is
	-- length of the systolic array buffers:
	constant SYSTOLIC_ARR_BUFF_LENGTH : integer := 8;
	-- number of bits in the systolic array buffer elements:
	constant SYSTOLIC_ARR_BUFF_ELEM_WIDTH : integer := 16;
	
	-- type declaration for systolic array buffers
    type systolic_arr_buff is array (0 to SYSTOLIC_ARR_BUFF_LENGTH-1) of std_logic_vector(SYSTOLIC_ARR_BUFF_ELEM_WIDTH-1 downto 0);
    -- type declarations for interconnects
	type systolic_arr_row_interconnects is array (0 to SYSTOLIC_ARR_BUFF_LENGTH-1, 0 to SYSTOLIC_ARR_BUFF_LENGTH/2-1) of std_logic_vector(SYSTOLIC_ARR_BUFF_ELEM_WIDTH-1 downto 0);
	type systolic_arr_col_interconnects is array (0 to SYSTOLIC_ARR_BUFF_LENGTH-1, 0 to SYSTOLIC_ARR_BUFF_LENGTH/2-1, 0 to 1) of std_logic_vector(SYSTOLIC_ARR_BUFF_ELEM_WIDTH-1 downto 0);

	-- type declaration for processing element accumulators
    type systolic_arr_pe_accum is array (0 to SYSTOLIC_ARR_BUFF_LENGTH-1, 0 to SYSTOLIC_ARR_BUFF_LENGTH-1) of std_logic_vector(SYSTOLIC_ARR_BUFF_ELEM_WIDTH-1 downto 0);
    -- type declaration for rows of processing element accumulators
    type systolic_arr_pe_accum_row is array (0 to SYSTOLIC_ARR_BUFF_LENGTH-1) of std_logic_vector(SYSTOLIC_ARR_BUFF_LENGTH*SYSTOLIC_ARR_BUFF_ELEM_WIDTH-1 downto 0);

end package systolic_array_pkg;

package body systolic_array_pkg is

end package body systolic_array_pkg;
