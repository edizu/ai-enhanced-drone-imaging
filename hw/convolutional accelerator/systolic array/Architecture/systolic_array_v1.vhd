-- Ryan Greer
-- 24/04/21
-- systolic_array_v1.vhd
-- implements the main functionality of the systolic array, instantiates and connects processing elements
-- based from Vivado AXI4 interface template code

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.systolic_array_pkg.all;

entity systolic_array_v1 is
	generic (
		-- Users to add parameters here
		

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_ID_WIDTH	: integer	:= 0;
		C_S00_AXI_DATA_WIDTH	: integer	:= 256;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 32;
		C_S00_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_WUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_RUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_BUSER_WIDTH	: integer	:= 0
	);
	port (
		-- Users to add ports here

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awlen	: in std_logic_vector(7 downto 0);
		s00_axi_awsize	: in std_logic_vector(2 downto 0);
		s00_axi_awburst	: in std_logic_vector(1 downto 0);
		s00_axi_awlock	: in std_logic;
		s00_axi_awcache	: in std_logic_vector(3 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awqos	: in std_logic_vector(3 downto 0);
		s00_axi_awregion	: in std_logic_vector(3 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wlast	: in std_logic;
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arlen	: in std_logic_vector(7 downto 0);
		s00_axi_arsize	: in std_logic_vector(2 downto 0);
		s00_axi_arburst	: in std_logic_vector(1 downto 0);
		s00_axi_arlock	: in std_logic;
		s00_axi_arcache	: in std_logic_vector(3 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arqos	: in std_logic_vector(3 downto 0);
		s00_axi_arregion	: in std_logic_vector(3 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rlast	: out std_logic;
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end systolic_array_v1;

architecture systolic_array_v1_impl of systolic_array_v1 is

	-- component declaration
	component systolic_array_v1_S00_AXI is
		generic (
		C_S_AXI_ID_WIDTH	: integer	:= 1;
		C_S_AXI_DATA_WIDTH	: integer	:= 128;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6;
		C_S_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_S_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_S_AXI_WUSER_WIDTH	: integer	:= 0;
		C_S_AXI_RUSER_WIDTH	: integer	:= 0;
		C_S_AXI_BUSER_WIDTH	: integer	:= 0
		);
		port (
		sa_buffers : out std_logic_vector(255 downto 0);
		systolic_arr_buffers_valid : out std_logic;
		systolic_arr_pe : in systolic_arr_pe_accum;
		systolic_arr_read : out std_logic;
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWID	: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWLEN	: in std_logic_vector(7 downto 0);
		S_AXI_AWSIZE	: in std_logic_vector(2 downto 0);
		S_AXI_AWBURST	: in std_logic_vector(1 downto 0);
		S_AXI_AWLOCK	: in std_logic;
		S_AXI_AWCACHE	: in std_logic_vector(3 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWQOS	: in std_logic_vector(3 downto 0);
		S_AXI_AWREGION	: in std_logic_vector(3 downto 0);
		S_AXI_AWUSER	: in std_logic_vector(C_S_AXI_AWUSER_WIDTH-1 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WLAST	: in std_logic;
		S_AXI_WUSER	: in std_logic_vector(C_S_AXI_WUSER_WIDTH-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BID	: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BUSER	: out std_logic_vector(C_S_AXI_BUSER_WIDTH-1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARID	: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARLEN	: in std_logic_vector(7 downto 0);
		S_AXI_ARSIZE	: in std_logic_vector(2 downto 0);
		S_AXI_ARBURST	: in std_logic_vector(1 downto 0);
		S_AXI_ARLOCK	: in std_logic;
		S_AXI_ARCACHE	: in std_logic_vector(3 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARQOS	: in std_logic_vector(3 downto 0);
		S_AXI_ARREGION	: in std_logic_vector(3 downto 0);
		S_AXI_ARUSER	: in std_logic_vector(C_S_AXI_ARUSER_WIDTH-1 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RID	: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RLAST	: out std_logic;
		S_AXI_RUSER	: out std_logic_vector(C_S_AXI_RUSER_WIDTH-1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component systolic_array_v1_S00_AXI;
	
	-- component declaration for "microFloat_dual_mac_single_cycle" SystemVerilog module
	component microFloat_dual_mac_single_cycle
	   port(
	       a_i : in std_logic_vector(15 downto 0);
	       b1_i : in std_logic_vector(15 downto 0);
	       c1_i : in std_logic_vector(15 downto 0);
	       b2_i : in std_logic_vector(15 downto 0);
	       c2_i : in std_logic_vector(15 downto 0);
	       out1_o : out std_logic_vector(15 downto 0);
	       out2_o : out std_logic_vector(15 downto 0)
	   );
	end component microFloat_dual_mac_single_cycle;
		
	---- signals for systolic array implementation
	
	-- raw signal which we are slicing up to get the buffer elements - for 128 lane bus architecture
	signal sa_buffers : std_logic_vector(255 downto 0) := std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_LENGTH*SYSTOLIC_ARR_BUFF_ELEM_WIDTH*2));	
	
	-- signal to indicate "systolic_array_buffers" data is valid and we may propagate data through the systolic array
	signal systolic_arr_buffers_valid : std_logic;
	
    -- signals for the input feature map and weights matrix buffers for systolic array:
	signal input_fm_buff_array : systolic_arr_buff := (others => std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH)));
	signal weights_buff_array : systolic_arr_buff := (others => std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH)));
	
    -- signal for row interconnects
    signal row_interconnects : systolic_arr_row_interconnects := (others => (others => std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH))));
    -- signal for column interconnects
    signal col_interconnects : systolic_arr_col_interconnects := (others => (others => (others => std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH)))));
    
    -- signal for PE accumulator
    signal systolic_arr_pe_prev : systolic_arr_pe_accum := (others => (others => std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH))));
    signal systolic_arr_pe : systolic_arr_pe_accum := (others => (others => std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH))));
    
    --signal to indicate when we have read the systolic array - used to reset PE accumulators to zero
	signal systolic_arr_read : std_logic := '0';
	
begin

-- Instantiation of Axi Bus Interface S00_AXI
systolic_array_v1_S00_AXI_inst : systolic_array_v1_S00_AXI
	generic map (
		C_S_AXI_ID_WIDTH	=> C_S00_AXI_ID_WIDTH,
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH,
		C_S_AXI_AWUSER_WIDTH	=> C_S00_AXI_AWUSER_WIDTH,
		C_S_AXI_ARUSER_WIDTH	=> C_S00_AXI_ARUSER_WIDTH,
		C_S_AXI_WUSER_WIDTH	=> C_S00_AXI_WUSER_WIDTH,
		C_S_AXI_RUSER_WIDTH	=> C_S00_AXI_RUSER_WIDTH,
		C_S_AXI_BUSER_WIDTH	=> C_S00_AXI_BUSER_WIDTH
	)
	port map (
	    sa_buffers => sa_buffers,
	    systolic_arr_buffers_valid => systolic_arr_buffers_valid,
	    systolic_arr_pe => systolic_arr_pe,
	    systolic_arr_read => systolic_arr_read,
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWID => std_logic_vector(to_unsigned(0,0)),
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWLEN	=> s00_axi_awlen,
		S_AXI_AWSIZE	=> s00_axi_awsize,
		S_AXI_AWBURST	=> s00_axi_awburst,
		S_AXI_AWLOCK	=> s00_axi_awlock,
		S_AXI_AWCACHE	=> s00_axi_awcache,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWQOS	=> s00_axi_awqos,
		S_AXI_AWREGION	=> s00_axi_awregion,
		S_AXI_AWUSER => std_logic_vector(to_unsigned(0,0)),
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WLAST	=> s00_axi_wlast,
		S_AXI_WUSER => std_logic_vector(to_unsigned(0,0)),
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARID => std_logic_vector(to_unsigned(0,0)),
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARLEN	=> s00_axi_arlen,
		S_AXI_ARSIZE	=> s00_axi_arsize,
		S_AXI_ARBURST	=> s00_axi_arburst,
		S_AXI_ARLOCK	=> s00_axi_arlock,
		S_AXI_ARCACHE	=> s00_axi_arcache,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARQOS	=> s00_axi_arqos,
		S_AXI_ARREGION	=> s00_axi_arregion,
		S_AXI_ARUSER => std_logic_vector(to_unsigned(0,0)),
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RLAST	=> s00_axi_rlast,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here
	
    ---- get buffers for systolic array - based on one 256 lane bus
    -- the first 128 bits of the 256 lane data bus is for the input feature map matrix buffer
    input_fm_buff_gen : for buffer_elem in 0 to 7 generate
    begin
        --input_fm_buff_array(buffer_elem) <= input_fm_buffer((buffer_elem*16+15) downto buffer_elem*16);
        input_fm_buff_array(buffer_elem) <= sa_buffers((buffer_elem*16+15) downto buffer_elem*16);
        row_interconnects(buffer_elem,0) <= input_fm_buff_array(buffer_elem);
    end generate input_fm_buff_gen;
    
    -- the second 128 bits of the 256 lane data bus is for the weights matrix buffer
    weights_buff_gen : for buffer_elem in 0 to 7 generate
    begin
        -- weights_buff_array(buffer_elem) <= weights_buffer((buffer_elem*16+15) downto buffer_elem*16);
        weights_buff_array(buffer_elem) <= sa_buffers(((buffer_elem+8)*16+15) downto (buffer_elem+8)*16);
    end generate weights_buff_gen;
    
    col_interconnects_populate : for buffer_elem in 0 to 3 generate
    begin
        col_interconnects(0,buffer_elem,0) <= weights_buff_array(buffer_elem*2);
        col_interconnects(0,buffer_elem,1) <= weights_buff_array(buffer_elem*2 + 1);
    end generate col_interconnects_populate;
    
    ---- implementation of systolic array
    -- defines the multiply accumulate processing element which uses above signals as inputs

    -- instantiates 32 of the dual mac single cycle floating point units - equivalent to 64 multiply-accumulates
    pe_mac_instantiate_rows : for i in 0 to 7 generate
        pe_mac_instantiate_cols : for j in 0 to 3 generate
            mac_pe : microFloat_dual_mac_single_cycle
            port map(
	           a_i => row_interconnects(i,j),
	           b1_i => col_interconnects(i,j,0),
	           c1_i => systolic_arr_pe_prev(i,j*2),
	           b2_i => col_interconnects(i,j,1),
	           c2_i => systolic_arr_pe_prev(i,j*2+1),
	           out1_o => systolic_arr_pe(i,j*2),
	           out2_o => systolic_arr_pe(i,j*2+1)                   
            );
        end generate pe_mac_instantiate_cols;
    end generate pe_mac_instantiate_rows;
    
    -- processing elements (PEs) - new, optimised using DSP48E2 trick
    pe_row_gen: for i in 0 to 7 generate
        pe_col_gen : for j in 0 to 3 generate
            pe_data_propagate_rows_gen : if j < 3 generate
                pe_data_propagate_rows_proc : process(s00_axi_aclk) is
                begin
                    if(rising_edge(s00_axi_aclk)) then
                        if(systolic_arr_buffers_valid = '1') then
                            row_interconnects(i,j+1) <= row_interconnects(i,j);
                        elsif(systolic_arr_read = '1') then
                            -- resets the row interconnects
                            row_interconnects(i,j+1) <= std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH));
                        end if;
                    end if;
                end process pe_data_propagate_rows_proc;
            end generate pe_data_propagate_rows_gen;
            
            pe_data_propagate_cols_gen : if i < 7 generate
                pe_data_propagate_cols_proc : process(s00_axi_aclk) is
                begin
                    if(rising_edge(s00_axi_aclk)) then
                        if(systolic_arr_buffers_valid = '1') then
                            col_interconnects(i+1,j,0) <= col_interconnects(i,j,0);
                            col_interconnects(i+1,j,1) <= col_interconnects(i,j,1);
                        elsif(systolic_arr_read = '1') then
                            -- resets the column interconnects
                            col_interconnects(i+1,j,0) <= std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH));
                            col_interconnects(i+1,j,1) <= std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH));
                        end if;
                    end if;
                end process pe_data_propagate_cols_proc;
            end generate pe_data_propagate_cols_gen;
            
            pe_mac_compute_proc : process(s00_axi_aclk) is
            begin
                if(rising_edge(s00_axi_aclk)) then
                    if(systolic_arr_buffers_valid = '1') then
                        systolic_arr_pe_prev(i,j*2) <= systolic_arr_pe(i,j*2);
                        systolic_arr_pe_prev(i,j*2+1) <= systolic_arr_pe(i,j*2+1);
                    elsif(systolic_arr_read = '1') then
                        -- this resets the systolic array processing element accumulators when the last valu has been read by the master
                        systolic_arr_pe_prev(i,j*2) <= std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH));
                        systolic_arr_pe_prev(i,j*2+1) <= std_logic_vector(to_unsigned(0,SYSTOLIC_ARR_BUFF_ELEM_WIDTH));
                    end if;
                end if;
            end process pe_mac_compute_proc;
            
        end generate pe_col_gen;
    end generate pe_row_gen;
        
	-- User logic ends

end systolic_array_v1_impl;
