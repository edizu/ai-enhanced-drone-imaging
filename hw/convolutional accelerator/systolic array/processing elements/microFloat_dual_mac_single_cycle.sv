// -------------------------------------------------------- //
// ----------- microFloat Dual, Single Cycle MAC ---------- //
// -------------------------------------------------------- //
// The design performs a single floating-point              //
// multiply-accumulate using the bfloat16 standard. The     //
// design has no pipelining.                                //
// The module performs the following calculations           //
// simultaniously                                           //
// out1_o = a_i * b1_i + c1_i                               //
// out2_o = a_i * b2_i + c2_i                               //
// -------------------------------------------------------- //
// Author: Martin Riis                                      //
// Last revision: 05/01/2021                                //
// Pre-requisites: microFloat_pkg.sv, microFloat_utils.sv.  //

//import microFloat_pkg::*;

`ifndef MICROFLOAT_PKG_INLINE__
`define MICROFLOAT_PKG_INLINE__

parameter C_E_WIDTH = 8;
parameter C_M_WIDTH = 7;
parameter C_BIAS = (2 ** (C_E_WIDTH - 1)) - 1;

typedef struct {
    logic s; // sign
    logic[C_E_WIDTH-1:0] e; // exponent
    logic[C_M_WIDTH:0] m; // mantissa, includes hidden bit
    logic gd; // guard
    logic rd; // round
    logic st; // sticky 
} fp_type;

`endif

module microFloat_dual_mac_single_cycle (
    input logic[15:0] a_i,
                      b1_i,
                      c1_i,
                      b2_i,
                      c2_i,
    output logic[15:0] out1_o,
                      out2_o
);

fp_type a_int,
        b1_int,
        c1_int,
        b2_int,
        c2_int,
        out_ac_int,
        out_bc_int,
        out1_int,
        out2_int;

always_comb begin
    a_int.s <= a_i[15]; a_int.e <= a_i[14:7]; a_int.m <= {1'b1, a_i[6:0]};
    b1_int.s <= b1_i[15]; b1_int.e <= b1_i[14:7]; b1_int.m <= {1'b1, b1_i[6:0]};
    c1_int.s <= c1_i[15]; c1_int.e <= c1_i[14:7]; c1_int.m <= {1'b1, c1_i[6:0]};
    b2_int.s <= b2_i[15]; b2_int.e <= b2_i[14:7]; b2_int.m <= {1'b1, b2_i[6:0]};
    c2_int.s <= c2_i[15]; c2_int.e <= c2_i[14:7]; c2_int.m <= {1'b1, c2_i[6:0]};
    out1_o <= {out1_int.s, out1_int.e, out1_int.m[6:0]};
    out2_o <= {out2_int.s, out2_int.e, out2_int.m[6:0]};
end

microFloat_dual_mul_single_cycle dual_mul_single_cycle_inst (
    .a_i (b1_int),
    .b_i (b2_int),
    .c_i (a_int),
    .out_ac_o (out_ac_int),
    .out_bc_o (out_bc_int)
);

floating_point_0 add_single_cycle_inst0 (
    .s_axis_a_tvalid (1'b1),
    .s_axis_a_tdata ({out_ac_int.s, out_ac_int.e, out_ac_int.m[6:0]}),
    .s_axis_b_tvalid (1'b1),
    .s_axis_b_tdata ({c1_int.s, c1_int.e, c1_int.m[6:0]}),
    .m_axis_result_tvalid (),
    .m_axis_result_tdata ({out1_int.s, out1_int.e, out1_int.m[6:0]})
);

floating_point_0 add_single_cycle_inst1 (
    .s_axis_a_tvalid (1'b1),
    .s_axis_a_tdata ({out_bc_int.s, out_bc_int.e, out_bc_int.m[6:0]}),
    .s_axis_b_tvalid (1'b1),
    .s_axis_b_tdata ({c2_int.s, c2_int.e, c2_int.m[6:0]}),
    .m_axis_result_tvalid (),
    .m_axis_result_tdata ({out2_int.s, out2_int.e, out2_int.m[6:0]})
);

endmodule
