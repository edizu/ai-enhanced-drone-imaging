// -------------------------------------------------------- //
// ------- microFloat Dual, Single Cycle Multiplier ------- //
// -------------------------------------------------------- //
// This design uses a single Xilinx DSP48E2 multiplier to   //
// perform two bfloat16 multiplications simultaneously.     //
// This is conditional on the two calculations sharing a    //
// common multiplicand.                                     //
// The module microFloat_dual_mul can be generalised using  //
// the equations:                                           //
//     out_ac_o = a_i * c_i                                 //
//     out_bc_o = b_i * c_i                                 //
// Both multiplications are preformed in parallel and have  //
// no pipelining.                                           //
// -------------------------------------------------------- //
// Author: Martin Riis                                      //
// Last revision: 04/01/2021                                //
// Pre-requisites: microFloat_pkg.sv, microFloat_utils.sv.  //

//import microFloat_pkg::*;

`ifndef MICROFLOAT_PKG_INLINE__
`define MICROFLOAT_PKG_INLINE__

parameter C_E_WIDTH = 8;
parameter C_M_WIDTH = 7;
parameter C_BIAS = (2 ** (C_E_WIDTH - 1)) - 1;

typedef struct {
    logic s; // sign
    logic[C_E_WIDTH-1:0] e; // exponent
    logic[C_M_WIDTH:0] m; // mantissa, includes hidden bit
    logic gd; // guard
    logic rd; // round
    logic st; // sticky 
} fp_type; 

`endif

module microFloat_dual_mul_single_cycle (
    input fp_type a_i,
                  b_i,
                  c_i,
    output fp_type out_ac_o,
                   out_bc_o
);

fp_type out_ac_int[2],
        out_bc_int[2];
logic[15:0] mul_result_ac[2],
            mul_result_bc[2];
logic shift_ac,
      shift_bc;
         
always_comb begin
    out_ac_o.s <= a_i.s ^ c_i.s;
    out_bc_o.s <= b_i.s ^ c_i.s;
    
    out_ac_int[0].e <= a_i.e + c_i.e - 127;
    out_bc_int[0].e <= b_i.e + c_i.e - 127;
    
    mul_result_ac[1] <= mul_result_ac[0] << shift_ac;
    mul_result_bc[1] <= mul_result_bc[0] << shift_bc;
    
    if ((a_i.e == '0) || (c_i.e == '0)) begin
        out_ac_o.e <= '0;
        out_ac_o.m <= '0;
    end
    else begin
        out_ac_o.e <= out_ac_int[1].e;
        out_ac_o.m <= out_ac_int[1].m;
    end
    
    if ((b_i.e == '0) || (c_i.e == '0)) begin
        out_bc_o.e <= '0;
        out_bc_o.m <= '0;
    end
    else begin
        out_bc_o.e <= out_bc_int[1].e;
        out_bc_o.m <= out_bc_int[1].m;
    end
end

dual_mul_single_cycle dual_mul_ac (
    .a_i (a_i.m),
    .b_i (b_i.m),
    .c_i (c_i.m),
    .ac_o (mul_result_ac[0]),
    .bc_o (mul_result_bc[0])
);

clz8_norm clz_ac_inst (
    .x_i (mul_result_ac[0][15:8]),
    .out_o (shift_ac)
); 

clz8_norm clz_bc_inst (
    .x_i (mul_result_bc[0][15:8]),
    .out_o (shift_bc)
);

microFloat_dual_mul_post post_ac_inst (
    .out_i (out_ac_int[0]),
    .out_o (out_ac_int[1]),
    .mul_result_i (mul_result_ac[1]),
    .shift_i (shift_ac)
);

microFloat_dual_mul_post post_bc_inst (
    .out_i (out_bc_int[0]),
    .out_o (out_bc_int[1]),
    .mul_result_i (mul_result_bc[1]),
    .shift_i (shift_bc)
);
endmodule


module microFloat_dual_mul_post (
    input fp_type out_i,
    output fp_type out_o,
    input logic[15:0] mul_result_i,
    input logic shift_i
);

logic G,
      R,
      S,
      L,
      ULP;
      
always_comb begin
    L = mul_result_i[8];
    G = mul_result_i[7];
    R = mul_result_i[6];
    S = |mul_result_i[5:0];
    ULP = (G & (R | S)) | (L & G & ~(R | S));
    out_o.m = mul_result_i[15:8] + !mul_result_i[15] + ULP;
    // Last add element detects exponent wraparound
    out_o.e = out_i.e - shift_i + mul_result_i[15] + (&mul_result_i[15:8] & (!mul_result_i[15] | ULP));
end
endmodule


// Dual unsigned 8-bit multiplier
// Uses a single DSP48E2 
module dual_mul_single_cycle (
    input logic[7:0] a_i,
                     b_i,
                     c_i,
    output logic[15:0] ac_o,
                       bc_o
);

logic[17:0] a_int;
logic[26:0] b_int;
(*use_dsp48 = "yes"*) logic[44:0] p_int;
    
assign ac_o = p_int[33:18];
assign bc_o = p_int[15:0];

assign a_int = {19'b0, c_i};
assign b_int = {1'b0, a_i, 10'b0, b_i};
assign p_int = a_int * b_int;
endmodule