// -------------------------------------------------------- //
// ------------------ microFloat Package ------------------ //
// -------------------------------------------------------- //
// This package supports all microFloat designs, providing  //
// the fp_type type definition.                             //
// -------------------------------------------------------- //
// Author: Martin Riis                                      //
// Last revision: 27/08/2020                                //
// Pre-requisites: None                                     //

package microFloat_pkg;

parameter C_E_WIDTH = 8;
parameter C_M_WIDTH = 7;
parameter C_BIAS = (2 ** (C_E_WIDTH - 1)) - 1;

typedef struct {
    logic s; // sign
    logic [C_E_WIDTH-1:0] e; // exponent
    logic [C_M_WIDTH:0] m; // mantissa, includes hidden bit
} fp_type; 

endpackage