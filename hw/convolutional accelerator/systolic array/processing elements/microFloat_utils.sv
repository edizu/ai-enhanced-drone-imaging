// -------------------------------------------------------- //
// ----------------- microFloat Utilities ----------------- //
// -------------------------------------------------------- //
// This file contains utility modules to support all        //
// microFloat designs. These include parameterised pipeling //
// registers and a leading zero counter.                    //
// -------------------------------------------------------- //
// Author: Martin Riis                                      //
// Last revision: 21/12/2020                                //
// Pre-requisites: microFloat_pkg.sv                        //

//import microFloat_pkg::*;

`ifndef MICROFLOAT_PKG_INLINE__
`define MICROFLOAT_PKG_INLINE__

parameter C_E_WIDTH = 8;
parameter C_M_WIDTH = 7;
parameter C_BIAS = (2 ** (C_E_WIDTH - 1)) - 1;

typedef struct {
    logic s; // sign
    logic[C_E_WIDTH-1:0] e; // exponent
    logic[C_M_WIDTH:0] m; // mantissa, includes hidden bit
    logic gd; // guard
    logic rd; // round
    logic st; // sticky 
} fp_type; 

`endif

// fp_type register with bypass option
module fp_type_reg # (
    parameter C_BYPASS = 0,
    parameter C_DELAY = 1
)
(
    input wire clk_i,
    input fp_type in_i,
    output fp_type out_o
);

// Generates C_DELAY fp_type registers to provide a delay of C_DELAY
fp_type int_sig[C_DELAY+1];
assign int_sig[0] = in_i;
assign out_o = int_sig[C_DELAY];
genvar i;
for (i=0; i<C_DELAY; i++) begin
    fp_type_reg_single # (
        .C_BYPASS (C_BYPASS)
    )
    fp_type_reg_single_inst (
        .clk_i (clk_i),
        .in_i (int_sig[i]),
        .out_o (int_sig[i+1])
    );
end
endmodule

// Single fp_type register with bypass option
module fp_type_reg_single # (
    parameter C_BYPASS = 0
)
(
    input wire clk_i,
    input fp_type in_i,
    output fp_type out_o
);

always_comb begin
    if (C_BYPASS == 1)
        out_o <= in_i;
end

always_ff @ (posedge clk_i) begin
    if (C_BYPASS == 0)
        out_o <= in_i;
end
endmodule


// Variable width flip-flop with bypass option
module flip_flop # (
    parameter C_BYPASS = 0,
    parameter C_WIDTH = 1,
    parameter C_DELAY = 1
)
(
    input wire clk_i,
    input wire [C_WIDTH-1:0] in_i,
    output reg [C_WIDTH-1:0] out_o
);

// Generates C_DELAY flip-flops with a width of 
// C_WIDTH to provide a delay of C_DELAY
wire [C_WIDTH-1:0] int_sig[C_DELAY+1];
assign int_sig[0] = in_i;
assign out_o = int_sig[C_DELAY];
genvar i;
for (i=0; i<C_DELAY; i++) begin
    flip_flop_single # (
        .C_BYPASS (C_BYPASS),
        .C_WIDTH (C_WIDTH)
    )
    flip_flop_single_inst (
        .clk_i (clk_i),
        .in_i (int_sig[i]),
        .out_o (int_sig[i+1])
    );
end
endmodule

// Single variable width flip-flop with bypass option
module flip_flop_single # (
    parameter C_BYPASS = 0,
    parameter C_WIDTH = 1
)
(
    input wire clk_i,
    input wire [C_WIDTH-1:0] in_i,
    output reg [C_WIDTH-1:0] out_o
);

always_comb begin
    if (C_BYPASS == 1)
        out_o <= in_i;
end

always_ff @ (posedge clk_i) begin
    if (C_BYPASS == 0)
        out_o <= in_i;
end
endmodule

// 4-bit leading zero counter
module lzc4 (
    input wire [3:0] x_i,
    output reg a_o,
    output reg [1:0] z_o
);

assign a_o = ~|x_i; // Reuction NOR, indicates if the result is zero
assign z_o[1] = ~|x_i[3:2];
assign z_o[0] = (~x_i[1] | x_i[2]) & ~x_i[3];

endmodule

// Leading zero encoder
module lze (
    input wire [1:0] a_i,
    output reg q_o
);

assign q_o = a_i[0] & (~a_i[1]);
endmodule

// Updated 8-bit leading zero counter
module clz8_new # (
    parameter C_HAS_OUTPUT_REG = 0
)
(
    input wire clk_i,
    input wire [7:0] x_i,
    output reg [3:0] q_o
);

wire [1:0] a_int;
wire [1:0] z_int [2];
reg [2:0] q_int;

if (C_HAS_OUTPUT_REG == 0) begin
    assign q_o = q_int;
end
else begin
    always_ff @ (posedge clk_i) begin
        q_o <= q_int;
    end
end

lzc4 lzc4_0 (
    .x_i (x_i[7:4]),
    .a_o (a_int[0]),
    .z_o (z_int[0])
);

lzc4 lzc4_1 (
    .x_i (x_i[3:0]),
    .a_o (a_int[1]),
    .z_o (z_int[1])
);

lze lze_inst (
    .a_i (a_int),
    .q_o (q_int[2])
);

always_comb begin
    q_int[3] = 1'b0;
    if (q_int[2] == 1'b0)
        q_int[1:0] <= z_int[0];
    else
        q_int[1:0] <= z_int[1];
end
endmodule


// Special 8-bit leading zero counter for designs with 
// no support for denormalised numbers
module clz8_norm (
    input logic[7:0] x_i,
    output logic out_o
);

always_comb begin
    if (x_i[7])
        out_o <= 1'b0;
    else
        out_o <= 1'b1;    
end
endmodule
