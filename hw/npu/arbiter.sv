module arbiter # (
    parameter C_DATA_WIDTH = 128,
    parameter C_ADDR_WIDTH = 16,
    parameter C_NUM_EX_UNITS = 4
)
(
    input logic clk_i,
                rst_ni,
    
    // *** REGISTER WRITE PORT *** //
    output logic[C_DATA_WIDTH-1:0] reg_data_o,
    output logic[C_ADDR_WIDTH-1:0] reg_addr_o,

    // *** INPUT FROM EXECUTION UNITS *** //
    input logic[C_DATA_WIDTH-1:0] result_data_i[C_NUM_EX_UNITS],
    input logic[C_ADDR_WIDTH-1:0] result_addr_i[C_NUM_EX_UNITS],
    output logic[$clog2(C_NUM_EX_UNITS)-1:0] grant_o
);

logic[$clog2(C_NUM_EX_UNITS)-1:0] count;

always_ff @ (posedge clk_i) begin
    if (!rst_ni) begin
        count = '0;
    end
    else begin
        if (count == C_NUM_EX_UNITS - 1) begin
            count = '0;
        end
        else begin
            count += 1;
        end
    end
end

always_comb begin
    reg_data_o <= result_data_i[count];
    reg_addr_o <= result_addr_i[count];
    grant_o <= {C_NUM_EX_UNITS{1'b0}};
    grant_o[count] <= 1'b1;
end


/*
logic[$clog2(C_NUM_EX_UNITS)-1:0] s_ready,
                                  s_valid,
                                  mask,
                                  valid_isolated,
                                  valid_prioritized,
                                  valid_prioritized_isolated,
                                  valid;

genvar i;
for (i=0; i<C_NUM_EX_UNITS; i++)
    assign s_valid[i] = |result_addr_i[i];

always_comb begin
    case (mask) 
        4'b0001: begin
            reg_data_o <= result_data_i[0];
            reg_addr_o <= result_addr_i[0];
        end
        4'b0011: begin
            reg_data_o <= result_data_i[1];
            reg_addr_o <= result_addr_i[1];
        end
        4'b0111: begin
            reg_data_o <= result_data_i[2];
            reg_addr_o <= result_addr_i[2];
        end
        4'b1111: begin
            reg_data_o <= result_data_i[3];
            reg_addr_o <= result_addr_i[3];
        end
        default: begin
            reg_data_o <= '0;
            reg_addr_o <= '0;
        end
    endcase
end

always_ff @ (posedge clk_i) begin
    if (!rst_ni)
        mask = 0;
    else begin
        valid_isolated = s_valid & -s_valid;
        valid_prioritized = s_valid & ~mask;
        valid_prioritized_isolated = valid_prioritized & -valid_prioritized;
        valid = |valid_prioritized_isolated ? valid_prioritized_isolated : valid_isolated;
        s_ready = valid;
        mask <= (s_ready << 1) - 1;
    end    
end
*/
endmodule