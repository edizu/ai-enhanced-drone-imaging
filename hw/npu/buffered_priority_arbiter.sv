module buffered_priority_arbiter # (
    parameter C_WIDTH = 128,
    parameter C_ADDR_WIDTH = 7,
    parameter C_INPUT_FIFO_DEPTH = 4
)
(
    input logic clk_i,
    input logic rst_ni,
    
    input logic[C_WIDTH-1:0] din_i[3:0],
    input logic[C_ADDR_WIDTH-1:0] addr_i[3:0],
    input logic req_i[3:0],
    output logic stall_o[3:0],
    output logic[C_WIDTH-1:0] dout_o,
    output logic[C_ADDR_WIDTH-1:0] addr_o
);

logic[C_WIDTH+C_ADDR_WIDTH-1:0] data[3:0];
logic grant[3:0];
logic empty[3:0];

(* keep_hierarchy = "yes" *)
fifo # (
    .C_WIDTH (C_WIDTH+C_ADDR_WIDTH)
)
data_fifo_inst0 (
    .clk (clk_i),
    .rst (rst_ni),
    .wr_en (req_i[0]),
    .rd_en (grant[0]),
    .din ({din_i[0], addr_i[0]}),
    .dout (data[0]),
    .full (stall_o[0]),
    .empty (empty[0])
);

(* keep_hierarchy = "yes" *)
fifo # (
    .C_WIDTH (C_WIDTH+C_ADDR_WIDTH)
)
data_fifo_inst1 (
    .clk (clk_i),
    .rst (rst_ni),
    .wr_en (req_i[1]),
    .rd_en (grant[1]),
    .din ({din_i[1], addr_i[1]}),
    .dout (data[1]),
    .full (stall_o[1]),
    .empty (empty[1])
);

(* keep_hierarchy = "yes" *)
fifo # (
    .C_WIDTH (C_WIDTH+C_ADDR_WIDTH)
)
data_fifo_inst2 (
    .clk (clk_i),
    .rst (rst_ni),
    .wr_en (req_i[2]),
    .rd_en (grant[2]),
    .din ({din_i[2], addr_i[2]}),
    .dout (data[2]),
    .full (stall_o[2]),
    .empty (empty[2])
);

(* keep_hierarchy = "yes" *)
fifo # (
    .C_WIDTH (C_WIDTH+C_ADDR_WIDTH)
)
data_fifo_inst3 (
    .clk (clk_i),
    .rst (rst_ni),
    .wr_en (req_i[3]),
    .rd_en (grant[3]),
    .din ({din_i[3], addr_i[3]}),
    .dout (data[3]),
    .full (stall_o[3]),
    .empty (empty[3])
);
   
(* keep_hierarchy = "yes" *)             
priority_arbiter arbiter_inst (
    .clk_i (clk_i),
    .rst_ni (rst_ni),
    
    .request_i ('{~{empty[0], empty[1], empty[2], empty[3]}}),
    .grant_o ('{grant[0], grant[1], grant[2], grant[3]})
);

always_ff @ (posedge clk_i) begin
    unique case ({grant[0], grant[1], grant[2], grant[3]})
        4'b0001 : {dout_o, addr_o} <= data[3];
        4'b0010 : {dout_o, addr_o} <= data[2];
        4'b0100 : {dout_o, addr_o} <= data[1];
        4'b1000 : {dout_o, addr_o} <= data[0];
        default : {dout_o, addr_o} <= '0;
    endcase
end
endmodule
