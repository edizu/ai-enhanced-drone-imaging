import olympus_pkg::*;

module decode_v2 # (
    parameter C_INST_WIDTH = 32,
    parameter C_VREG_ADDR_WIDTH = 7,
    parameter C_SREG_ADDR_WIDTH = 7,
    parameter C_ICACHE_DEPTH = 128
)
(
    input logic clk_i,
                clk_en_i, //Sctive high clock enable
    input logic rst_ni,
    
    input inst_type inst_i, // Instruction at PC
        
    // *** PROGRAM COUNTER CONTROL *** //
    output logic[$clog2(C_ICACHE_DEPTH)-1:0] pc_o,
    output logic update_pc_o,
    output logic hazard_stall_o,
    
    input logic[15:0] sreg_data2_i, // Used for conditional jumps
    
    // *** REGISTER READ PORT INTERFACE *** //
    output logic[C_VREG_ADDR_WIDTH-1:0] vreg_addr1_o,
                                        vreg_addr2_o,
                                        sreg_addr1_o,
                                        sreg_addr2_o,
    
    // *** REGISTER WRITE SNOOP PORT *** //
    input logic[C_VREG_ADDR_WIDTH-1:0] vreg_snoop_addr_i,
    input logic[C_SREG_ADDR_WIDTH-1:0] sreg_snoop_addr_i,

    // *** INPUT MUX CONTROL *** //
    output t_ssrc_mux scalar_input_b_sel_o, // register or immediate
    output t_vsrc_mux vector_input_b_sel_o, // vector or scalar
    
    output logic[6:0] dst_addr_o, // destination address
    output t_v128 dst_vdata_o, // data in vector destination address
    output logic[15:0] dst_sdata_o, // data in scalar destination address
    
    output logic[15:0] immediate_o, // sign extended immediate value
    
    // Execution Unit Stall Signals
    input logic vfpu_stall_i,
                valu_stall_i,
                sfpu_stall_i,
                salu_stall_i,
                pcu_stall_i,
                lsu_mm_stall_i,
    
    // *** EXECUTION UNIT CONTROL *** //
    output t_vfpu_opcodes vfpu_opcode_o, // VFPU
    output t_valu_opcodes valu_opcode_o, // VALU
    output t_sfpu_opcodes sfpu_opcode_o, // SFPU
    output t_salu_opcodes salu_opcode_o, // SALU
    output t_pcu_opcodes pcu_opcode_o, // PCU
    output t_lsu_opcodes lsu_opcode_o, // LSU
    
    // LSU Interface
    input logic lsu_tlast_i,
    output logic lsu_rst_tlast_o
);

t_r_type r_type_inst;
t_ir_type ir_type_inst;
t_i_type i_type_inst;
t_major_opcodes major_opcode;
logic[2:0] minor_opcode;
logic[127:0] vreg_valid,
             sreg_valid;

assign r_type_inst = t_r_type'(inst_i);
assign ir_type_inst = t_ir_type'(inst_i);
assign i_type_inst = t_i_type'(inst_i);
assign major_opcode = t_major_opcodes'({r_type_inst.opcode.vs, r_type_inst.opcode.vvvs, r_type_inst.opcode.dtype, r_type_inst.opcode.opt});
assign minor_opcode = r_type_inst.opcode.minor;

always_ff @ (posedge clk_i) begin
    if (!rst_ni) begin
        hazard_stall_o <= 1'b0;
        // After reset, all registers contain valid data
        vreg_valid <= '1;
        sreg_valid <= '1;
    end
    else if (clk_en_i == 1'b0) begin // Decode is disabled
        // Scoreboard update has to always occur, regardless of
        // whether or not the decoder is active.
        vreg_valid[vreg_snoop_addr_i] <= 1'b1;
        sreg_valid[sreg_snoop_addr_i] <= 1'b1;
    end
    else begin
        vfpu_opcode_o <= VFPU_NOP;
        valu_opcode_o <= VALU_NOP;
        sfpu_opcode_o <= SFPU_NOP;
        salu_opcode_o <= SALU_NOP;
        pcu_opcode_o <= PCU_NOP;
        lsu_opcode_o <= LSU_NOP;
        
        scalar_input_b_sel_o <= SSRC_REG;
        vector_input_b_sel_o <= VSRC_VECTOR;
        
        dst_addr_o <= '0;
        vreg_addr1_o <= '0;
        vreg_addr2_o <= '0;
        sreg_addr1_o <= '0;
        sreg_addr2_o <= '0;
        /*
        dst_addr_o <= inst_i.r.dst;
        vreg_addr1_o <= inst_i.r.src1;
        vreg_addr2_o <= inst_i.r.src2;
        sreg_addr1_o <= inst_i.r.src1;
        sreg_addr2_o <= inst_i.r.src2;
        */
        update_pc_o <= 1'b0;
        pc_o <= '0;

        immediate_o <= 16'h0;
        
        lsu_rst_tlast_o <= 1'b0;
        
        vreg_valid[vreg_snoop_addr_i] <= 1'b1;
        sreg_valid[sreg_snoop_addr_i] <= 1'b1;
        
        unique case (major_opcode)
            VFV : begin
                if (minor_opcode != 0) begin
                    // Not NOP
                    if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                        (vreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                        (vfpu_stall_i == 1'b0)) begin
                        // Both sources are valid, instruction can be executed
                        dst_addr_o <= inst_i.r.dst;
                        vreg_addr1_o <= inst_i.r.src1;
                        vreg_addr2_o <= inst_i.r.src2;
                        sreg_addr1_o <= inst_i.r.src1;
                        sreg_addr2_o <= inst_i.r.src2;
                        hazard_stall_o <= 1'b0;
                        vreg_valid[inst_i.r.dst] <= 1'b0;
                        unique case (minor_opcode)
                            1 : begin
                                vfpu_opcode_o <= VFPU_MAC;
                            end
                            2 : begin
                                vfpu_opcode_o <= VFPU_MSC;
                            end
                            3 : begin
                                vfpu_opcode_o <= VFPU_MUL;
                            end
                            5 : begin
                                vfpu_opcode_o <= VFPU_ADD;
                            end
                            6 : begin
                                vfpu_opcode_o <= VFPU_SUB;
                            end
                            7 : begin
                                vfpu_opcode_o <= VFPU_ABS;
                            end
                        endcase
                    end
                    else begin
                        // One or more sources is invalid, i.e. waiting on previous instruction
                        // to write data back to a source register.
                        hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                    end
                end
            end
            
            VFR : begin
                if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (vfpu_stall_i == 1'b0)) begin
                    // Source is valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    sreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        3 : begin
                            vfpu_opcode_o <= VFPU_MIN;
                        end
                        4 : begin
                            vfpu_opcode_o <= VFPU_MAX;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            VIV : begin
                if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (vreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (valu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    // Note, some of these instructions do not have src2, in this case, src2 = 0
                    // register zero is always valid as it is read only
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    vreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        1 : begin
                            valu_opcode_o <= VALU_MAC;
                        end
                        2 : begin
                            valu_opcode_o <= VALU_MSC;
                        end
                        3 : begin
                            valu_opcode_o <= VALU_MUL;
                        end
                        5 : begin
                            valu_opcode_o <= VALU_ADD;
                        end
                        6 : begin
                            valu_opcode_o <= VALU_SUB;
                        end
                        7 : begin
                            valu_opcode_o <= VALU_ABS;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            VIR : begin
                if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (vreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (valu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    // Note, some of these instructions do not have src2, in this case, src2 = 0
                    // register zero is always valid as it is read only
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    sreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        0 : begin
                            valu_opcode_o <= VALU_EQ;
                        end
                        1 : begin
                            valu_opcode_o <= VALU_LT;
                        end
                        2 : begin
                            valu_opcode_o <= VALU_GE;
                        end
                        3 : begin
                            valu_opcode_o <= VALU_MIN;
                        end
                        4 : begin
                            valu_opcode_o <= VALU_MAX;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            VFS : begin
                if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (sreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (vfpu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    vreg_valid[inst_i.r.dst] <= 1'b0;
                    vector_input_b_sel_o <= VSRC_SCALAR;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        1 : begin
                            vfpu_opcode_o <= VFPU_MAC;
                        end
                        2 : begin
                            vfpu_opcode_o <= VFPU_MSC;
                        end
                        3 : begin
                            vfpu_opcode_o <= VFPU_MUL;
                        end
                        5 : begin
                            vfpu_opcode_o <= VFPU_ADD;
                        end
                        6 : begin
                            vfpu_opcode_o <= VFPU_SUB;
                        end
                        7 : begin
                            vfpu_opcode_o <= VFPU_RLU;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            VMEM : begin
                unique case (minor_opcode)
                    0 : begin
                        if ((sreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                            (lsu_mm_stall_i == 1'b0)) begin
                            // Source is valid and memory-mapped LSU is not stalled, instruction can be executed
                            dst_addr_o <= inst_i.r.dst;
                            vreg_addr1_o <= inst_i.r.src1;
                            vreg_addr2_o <= inst_i.r.src2;
                            sreg_addr1_o <= inst_i.r.src1;
                            sreg_addr2_o <= inst_i.r.src2;
                            hazard_stall_o <= 1'b0;
                            vreg_valid[inst_i.r.dst] <= 1'b0;
                            lsu_opcode_o <= LSU_VLD;
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing       
                        end
                    end
                    1 : begin
                        if (vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) begin
                            // Source is valid, instruction can be executed
                            dst_addr_o <= inst_i.r.dst;
                            vreg_addr1_o <= inst_i.r.src1;
                            vreg_addr2_o <= inst_i.r.src2;
                            sreg_addr1_o <= inst_i.r.src1;
                            sreg_addr2_o <= inst_i.r.src2;
                            hazard_stall_o <= 1'b0;
                            sreg_valid[inst_i.r.dst] <= 1'b0;
                            lsu_opcode_o <= LSU_VST;
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing    
                        end
                    end
                    2 : begin
                        if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                            (vreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                            (pcu_stall_i == 1'b0)) begin
                            // Source is valid, instruction can be executed
                            dst_addr_o <= inst_i.r.dst;
                            vreg_addr1_o <= inst_i.r.src1;
                            vreg_addr2_o <= inst_i.r.src2;
                            sreg_addr1_o <= inst_i.r.src1;
                            sreg_addr2_o <= inst_i.r.src2;
                            hazard_stall_o <= 1'b0;
                            vreg_valid[inst_i.r.dst] <= 1'b0;
                            pcu_opcode_o <= PCU_VPERM;
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing    
                        end
                    end
                    3 : begin
                        if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                            (pcu_stall_i == 1'b0)) begin
                            // Source is valid, instruction can be executed
                            hazard_stall_o <= 1'b0;
                            vreg_valid[inst_i.r.dst] <= 1'b0;
                            // vpermi
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing    
                        end
                    end
                    4 : begin
                        if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                            (pcu_stall_i == 1'b0)) begin
                            // Source is valid, instruction can be executed
                            dst_addr_o <= inst_i.r.dst;
                            vreg_addr1_o <= inst_i.r.src1;
                            vreg_addr2_o <= inst_i.r.src2;
                            sreg_addr1_o <= inst_i.r.src1;
                            sreg_addr2_o <= inst_i.r.src2;
                            hazard_stall_o <= 1'b0;
                            sreg_valid[inst_i.r.dst] <= 1'b0;
                            pcu_opcode_o <= PCU_CPY_VS;
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing    
                        end
                    end
                    5 : begin
                        if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                            (pcu_stall_i == 1'b0)) begin
                            // Source is valid, instruction can be executed
                            dst_addr_o <= inst_i.r.dst;
                            vreg_addr1_o <= inst_i.r.src1;
                            vreg_addr2_o <= inst_i.r.src2;
                            sreg_addr1_o <= inst_i.r.src1;
                            sreg_addr2_o <= inst_i.r.src2;
                            hazard_stall_o <= 1'b0;
                            vreg_valid[inst_i.r.dst] <= 1'b0;
                            pcu_opcode_o <= PCU_CPY_SV;
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing    
                        end
                    end
                    6 : begin
                        // Set TLAST signal on AXI Stream output
                        lsu_opcode_o <= LSU_SLAST;
                    end
                endcase
            end
            
            VIS : begin
                if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (sreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (valu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    vreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    vector_input_b_sel_o <= VSRC_SCALAR;
                    unique case (minor_opcode)
                        1 : begin
                            valu_opcode_o <= VALU_MAC;
                        end
                        2 : begin
                            valu_opcode_o <= VALU_MSC;
                        end
                        3 : begin
                            valu_opcode_o <= VALU_MUL;
                        end
                        5 : begin
                            valu_opcode_o <= VALU_ADD;
                        end
                        6 : begin
                            valu_opcode_o <= VALU_SUB;
                        end
                        7 : begin
                            valu_opcode_o <= VALU_RLU;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            VBIT : begin
                if ((vreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (vreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (valu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    vreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        1 : begin
                            valu_opcode_o <= VALU_NOT;
                        end
                        2 : begin
                            valu_opcode_o <= VALU_AND;
                        end
                        3 : begin
                            valu_opcode_o <= VALU_OR;
                        end
                        4 : begin
                            valu_opcode_o <= VALU_XOR;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            SFR : begin
                if ((sreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (sreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (sfpu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    sreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        1 : begin
                            sfpu_opcode_o <= SFPU_MAC;
                        end
                        2 : begin
                            sfpu_opcode_o <= SFPU_MSC;
                        end
                        3 : begin
                            sfpu_opcode_o <= SFPU_MUL;
                        end
                        5 : begin
                            sfpu_opcode_o <= SFPU_ADD;
                        end
                        6 : begin
                            sfpu_opcode_o <= SFPU_SUB;
                        end
                        7 : begin
                            sfpu_opcode_o <= SFPU_ABS;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            COND : begin
                // JUMPS
                unique case (minor_opcode)
                    0 : begin // JMP
                        update_pc_o <= 1'b1;
                        pc_o <= inst_i.i.imm;
                    end
                    1 : begin
                        // JAL
                        sreg_valid[inst_i.r.dst] <= 1'b0;
                    end
                    2 : begin // JZ
                        if (sreg_data2_i == 16'b0) begin
                            update_pc_o <= 1'b1;
                            pc_o <= inst_i.i.imm;
                        end
                    end
                    3 : begin // JNZ
                        if (sreg_data2_i != 16'b0) begin
                            update_pc_o <= 1'b1;
                            pc_o <= inst_i.i.imm;
                        end
                    end
                    4 : begin // JLAST
                        if (lsu_tlast_i == 1'b1) begin
                            update_pc_o <= 1'b1;
                            pc_o <= inst_i.i.imm;
                            lsu_rst_tlast_o <= 1'b1;
                        end
                    end
                endcase
            end
            
            SIR : begin
                if ((sreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (sreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (salu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    sreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        1 : begin
                            salu_opcode_o <= SALU_MAC;
                        end
                        2 : begin
                            salu_opcode_o <= SALU_MSC;
                        end
                        3 : begin
                            salu_opcode_o <= SALU_MUL;
                        end
                        5 : begin
                            salu_opcode_o <= SALU_ADD;
                        end
                        6 : begin
                            salu_opcode_o <= SALU_SUB;
                        end
                        7 : begin
                            salu_opcode_o <= SALU_ABS;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            SII : begin
                if ((sreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (salu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    sreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    scalar_input_b_sel_o <= SSRC_IMM;
                    immediate_o <= {{8{inst_i.ir.imm[7]}}, inst_i.ir.imm};
                    
                    unique case (minor_opcode)
                        1 : begin
                            salu_opcode_o <= SALU_MAC;
                        end
                        2 : begin
                            salu_opcode_o <= SALU_MSC;
                        end
                        3 : begin
                            salu_opcode_o <= SALU_MUL;
                        end
                        5 : begin
                            salu_opcode_o <= SALU_ADD;
                        end
                        6 : begin
                            salu_opcode_o <= SALU_SUB;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
            
            SMEM : begin
                unique case (minor_opcode)
                    0 : begin
                        if (sreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) begin
                            // Both sources are valid, instruction can be executed
                            dst_addr_o <= inst_i.r.dst;
                            vreg_addr1_o <= inst_i.r.src1;
                            vreg_addr2_o <= inst_i.r.src2;
                            sreg_addr1_o <= inst_i.r.src1;
                            sreg_addr2_o <= inst_i.r.src2;
                            sreg_valid[inst_i.r.dst] <= 1'b0;
                            hazard_stall_o <= 1'b0;
                            lsu_opcode_o <= LSU_LD;
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                        end
                    end
                    1 : begin
                        if (sreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) begin
                            // Both sources are valid, instruction can be executed
                            dst_addr_o <= inst_i.r.dst;
                            vreg_addr1_o <= inst_i.r.src1;
                            vreg_addr2_o <= inst_i.r.src2;
                            sreg_addr1_o <= inst_i.r.src1;
                            sreg_addr2_o <= inst_i.r.src2;
                            sreg_valid[inst_i.r.dst] <= 1'b0;
                            hazard_stall_o <= 1'b0;
                            lsu_opcode_o <= LSU_ST;
                        end
                        else begin
                            // Source is invalid, i.e. waiting on previous instruction
                            // to write data back to the source register.
                            hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                        end
                    end
                    2 : begin
                        // No dependancy
                        sreg_valid[inst_i.i.dst] <= 1'b0; // NOT SURE ABOUT THIS
                        immediate_o <= inst_i.i.imm;
                        pcu_opcode_o <= PCU_LD_IMM;
                    end
                    3 : begin
                        // No dependancy
                        lsu_opcode_o <= LSU_ST;
                    end
                endcase
            end
            
            SBIT : begin
                if ((sreg_valid[inst_i.r.src1] == 1'b1 | inst_i.r.src1 == inst_i.r.dst) & 
                    (sreg_valid[inst_i.r.src2] == 1'b1 | inst_i.r.src2 == inst_i.r.dst) & 
                    (salu_stall_i == 1'b0)) begin
                    // Both sources are valid, instruction can be executed
                    dst_addr_o <= inst_i.r.dst;
                    vreg_addr1_o <= inst_i.r.src1;
                    vreg_addr2_o <= inst_i.r.src2;
                    sreg_addr1_o <= inst_i.r.src1;
                    sreg_addr2_o <= inst_i.r.src2;
                    sreg_valid[inst_i.r.dst] <= 1'b0;
                    hazard_stall_o <= 1'b0;
                    unique case (minor_opcode)
                        0 : begin
                            salu_opcode_o <= SALU_NOT;
                        end
                        1 : begin
                            salu_opcode_o <= SALU_AND;
                        end
                        2 : begin
                            salu_opcode_o <= SALU_OR;
                        end
                        3 : begin
                            salu_opcode_o <= SALU_XOR;
                        end
                        4 : begin
                            salu_opcode_o <= SALU_SRA;
                        end
                        5 : begin
                            salu_opcode_o <= SALU_SRL;
                        end
                        6 : begin
                            salu_opcode_o <= SALU_SLL;
                        end
                    endcase
                end
                else begin
                    // Source is invalid, i.e. waiting on previous instruction
                    // to write data back to the source register.
                    hazard_stall_o <= 1'b1;// Prevent program counter from incrementing
                end
            end
        endcase
    end
end
endmodule
