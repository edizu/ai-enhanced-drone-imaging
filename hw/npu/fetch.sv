// *** OLYMPUS PROCESSING SYSTEM *** //
// Developer:       Martin Riis
// Date created:    04/11/2020
// File name:       fetch.sv
// Description:     Instruction fetch unit

typedef enum logic[1:0] {
    NORMAL,
    STALL,
    RESUME
} fetch_states_t;

module fetch # (
    parameter C_INST_WIDTH = 32,
    parameter C_ICACHE_DEPTH = 128,
    parameter C_PC_OFFSET = 1,
    parameter C_DUAL_DECODE_ENABLE = 0
)
(
    input logic clk_i,
                stall_i, // Active high decode stall signal
    input logic rst_ni,
    // Program counter
    input logic pc_set_i,
    input logic[$clog2(C_ICACHE_DEPTH)-1:0] pc_i,
    output logic[$clog2(C_ICACHE_DEPTH)-1:0] pc_o,
    // Read port B
    output logic[$clog2(C_ICACHE_DEPTH)-1:0] addrB_o,
    input logic[C_INST_WIDTH-1:0] doutB_i,
    // Read port C
    output logic[$clog2(C_ICACHE_DEPTH)-1:0] addrC_o,
    input logic [C_INST_WIDTH-1:0] doutC_i,
    // Instruction out
    output logic[C_INST_WIDTH-1:0] instA_o,
                                   instB_o
);

fetch_states_t state;

assign addrB_o = pc_o;
assign instA_o = doutB_i;
assign addrC_o = '0;
assign instB_o = '0;

always_ff @ (posedge clk_i) begin
    // Program counter reset
    if (!rst_ni) begin
        pc_o <= '0;
        state <= NORMAL; // Normal operation, no decode stall
    end
    else begin
        case (state)
            NORMAL : begin
                if (stall_i == 1'b1) begin
                    // Decoder stalled
                    // Don't update PC
                    state <= STALL;
                end
                else begin
                    // Normal operation
                    if (pc_set_i) begin
                        // External program counter update
                        // Can be used for jump instructions
                        pc_o <= pc_i;
                    end
                    else begin
                        pc_o <= pc_o + 1'b1;
                    end
                end
            end
            STALL : begin
                // Stall and resume state effectively 
                // halts the PC update for 2 cycle to 
                // let the decoder get to where it 
                // should be.
                //if (stall_i == 1'b0) begin
                  //  state <= RESUME;
                //end
                state <= NORMAL;
            end
            RESUME : begin // Not used
                state <= NORMAL;
            end
        endcase
    end
end

/*
assign addrB_o = pc_o;
assign instA_o = doutB_i;

if (C_DUAL_DECODE_ENABLE) begin
    assign addrC_o = pc_o + C_PC_OFFSET;
    assign instB_o = doutC_i;
end
else begin
    assign addrC_o = '0;
    assign instB_o = '0;
end

always_ff @ (posedge clk_i) begin
    // Program counter reset
    if (!rst_ni) begin
        pc_o <= '0;
    end
    // External program counter update
    // Can be used for jump instructions
    else if (pc_set_i) begin
        pc_o <= pc_i;
    end
    else begin
        pc_o <= pc_o + 1'b1;
    end
end
*/
endmodule