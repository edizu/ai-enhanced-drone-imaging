/*module fifo # (
    parameter C_WIDTH  = 135
)
(
    input  logic               clk,       // Main System Clock  (Sync FIFO)
    input  logic               rst,       // FIFO Counter Reset (Clk
    input  logic               wr_en,     // FIFO Write Enable  (Clk)
    input  logic               rd_en,     // FIFO Read Enable   (Clk)
    input  logic [C_WIDTH-1:0] din,       // FIFO Data Input    (Clk)
    output logic [C_WIDTH-1:0] dout,      // FIFO Data Output   (Clk)
    output logic               full,      // FIFO FULL Status   (Clk)
    output logic               empty      // FIFO EMPTY Status  (Clk)
);

logic[1:0] w_ptr,
           r_ptr,
           w_addr,
           r_addr,
           capacity;
logic[127:0] w_data;

assign full = capacity == '1;
assign empty = capacity == '0;

always_ff @ (posedge clk) begin
    if (!rst) begin
        w_ptr <= 2'd3;
        r_ptr <= 2'd3;
        w_addr <= 2'd0;
        r_addr <= 2'd0;
        capacity <= '0;
    end
    else begin
        if (rd_en == 1'b1) begin
            w_addr = r_ptr;
            w_data = '0;
            r_ptr = r_ptr - 1;
            capacity = capacity - 1; // Should never ready when it is empty
        end
        if (wr_en == 1'b1) begin
            w_addr = w_ptr;
            w_data = din;
            w_ptr = w_ptr - 1;
            capacity = capacity + 1; // Should never write when it is full
        end
    end
end

dist_mem_gen_0 mem_inst (
    .a ({2'b00, w_addr}), // Write address
    .d (w_data), // Write data
    .dpra ({2'b00, r_ptr}), // Read address
    .clk (clk),
    .we (wr_en),
    .qdpo (dout) // Read data
);
endmodule
*/



module fifo # (
    parameter C_WIDTH  = 135
)
(
    input  logic               clk,       // Main System Clock  (Sync FIFO)
    input  logic               rst,       // FIFO Counter Reset (Clk
    input  logic               wr_en,     // FIFO Write Enable  (Clk)
    input  logic               rd_en,     // FIFO Read Enable   (Clk)
    input  logic [C_WIDTH-1:0] din,       // FIFO Data Input    (Clk)
    output logic [C_WIDTH-1:0] dout,      // FIFO Data Output   (Clk)
    output logic               full,      // FIFO FULL Status   (Clk)
    output logic               empty      // FIFO EMPTY Status  (Clk)
);

logic[C_WIDTH-1:0] mem[4];
logic[1:0] w_ptr,
           r_ptr,
           capacity;

assign full = capacity == '1;
assign empty = capacity == '0;

always_ff @ (posedge clk) begin
    if (!rst) begin
        w_ptr <= 2'd3;
        r_ptr <= 2'd3;
        capacity <= '0;
        mem <= '{default:'0};
    end
    else begin
        if (rd_en == 1'b1 & capacity != 2'b00) begin
            dout = mem[r_ptr];
            mem[r_ptr] = '0;
            r_ptr = r_ptr - 1;
            capacity = capacity - 1; // Should never ready when it is empty
        end
        if (wr_en == 1'b1 & capacity != 2'b11) begin
            mem[w_ptr] = din;
            w_ptr = w_ptr - 1;
            capacity = capacity + 1; // Should never write when it is full
        end
    end
end
endmodule
