import olympus_pkg::*;

module lsu # (
    parameter C_AXI_ADDR_WIDTH = 16,
    parameter C_AXI_DATA_WIDTH = 128
)
(
    input logic sclk_i,
                mmclk_i,
                rst_ni,
                
    input t_lsu_opcodes lsu_opcode_i,
    
    // Memory Mapped Vector Load/Store //
    input logic[15:0] ldst_ext_base_i, // load/store external base address
                      ldst_ext_offset_i, // load/store external address offset
    input logic[6:0] ld_dst_addr_i, // load local destination register address input
    input logic[127:0] st_data_i, // store data
    output logic[6:0] ld_dst_addr_o, // load local destination register address output
    output logic[127:0] ld_dst_data_o, // load data output
    output logic load_waiting_o,
    
    // Stream Autowrite Input //
    input logic stream_stall_i,
    output logic[127:0] stream_vdata_o,
    output logic[6:0] stream_vaddr_o,
    output logic stream_tlast_o,
    input logic stream_rst_tlast_i,
    input logic tvalid_i,
    input logic tlast_i,
    output logic tready_o,
    input logic[127:0] tdata_i,
    
    // Stream Autowrite Output //
    input logic[6:0] vreg_waddr_snoop_i,
    output logic[6:0] vreg_raddr3_o,
    input logic[127:0] vreg_rdata3_i,
    output logic global_stall_o,
    output logic tvalid_o,
    output logic tlast_o,
    input logic tready_i,
    output logic[127:0] tdata_o,

    // Pseudo-AXI Interface //
    // Write port
    output logic[C_AXI_ADDR_WIDTH-1:0] m_axi_waddr_o,
    output logic[C_AXI_DATA_WIDTH-1:0] m_axi_wdata_o,
    output logic m_axi_wvalid_o,
    input logic m_axi_wready_i,
    // Read port
    output logic[C_AXI_ADDR_WIDTH-1:0] m_axi_raddr_o,
    input logic[C_AXI_DATA_WIDTH-1:0] m_axi_rdata_i,
    input logic m_axi_rvalid_i,
    output logic m_axi_rready_o
);

////////////////////////////
// Stream Autowrite Input //
////////////////////////////
always_ff @ (posedge sclk_i) begin
    if ((rst_ni == 1'b0) | (stream_rst_tlast_i == 1'b1)) begin
        stream_tlast_o <= 1'b0;
    end
    else if (tlast_i == 1'b1) begin
        stream_tlast_o <= 1'b1;
    end
end

always_comb begin
    if (stream_stall_i == 1'b1) begin
        tready_o <= 1'b0;
    end
    else begin
        tready_o <= 1'b1;
    end
end

/* 
* When data is received at the AXI Stream input,
* it is automatically written to one of the vector 
* registers v124-v127.
*/
logic[1:0] stream_in_count;
always_ff @ (posedge sclk_i) begin
    if (!rst_ni) begin
        stream_in_count <= 2'h0; // Count reset
        stream_vdata_o <= '0;
        stream_vaddr_o <= '0;
    end
    else begin
        if (tvalid_i == 1'b1 & tready_o == 1'b1) begin
            // Valid data is on the bus
            stream_vdata_o <= tdata_i;
            stream_vaddr_o = {5'b11111, stream_in_count};
            stream_in_count <= stream_in_count + 1; // Count only incremented when data is valid
        end
        else begin
            // Bus data invalid
            stream_vdata_o <= '0;
            stream_vaddr_o <= '0;
        end
    end
end


/////////////////////////////
// Stream Autowrite Output //
/////////////////////////////
//always_ff @ (posedge sclk_i) begin
    
//end

/* 
* When data is written to v123, it is
* automatically sent out the AXI 
* Stream output port.
*/
always_ff @ (posedge sclk_i) begin
    if (!rst_ni) begin
        global_stall_o <= 1'b0;
        tvalid_o <= 1'b0;
        vreg_raddr3_o <= '0;
    end
    else begin
        if ((vreg_waddr_snoop_i == 120) | (vreg_waddr_snoop_i == 121) | (vreg_waddr_snoop_i == 122) |
            (vreg_waddr_snoop_i == 123) | (global_stall_o == 1'b1)) begin
            // Data has just been written to the stream output register or
            // the core has been stalled due to the stream slave not being 
            // ready to receive data.
            vreg_raddr3_o <= vreg_waddr_snoop_i;
            if (tready_i != 1'b1) begin
                // AXI Stream slave is not ready to receive data
                // Core must be stalled until it is
                global_stall_o <= 1'b1;
                tvalid_o <= 1'b0;
                tdata_o <= '0;
            end
            else begin
                // Slave is ready to receive data
                global_stall_o <= 1'b0;
                tvalid_o <= 1'b1;
                tdata_o <= vreg_rdata3_i;
                if ((vreg_waddr_snoop_i == 123) & (lsu_opcode_i == LSU_SLAST)) begin
                    // TLAST signal needs asserting
                    // TLAST is only asserted if data is coming from register v123
                    tlast_o <= 1'b1;
                end
            end
        end
        else begin
            // New data has not been written to the stream output register
            tvalid_o <= 1'b0;
            tlast_o <= 1'b0;
            tdata_o <= '0;
        end
    end
end

/*
logic[6:0] dst_reg_addr_out;
logic[15:0] mm_ld_dst_addr_in,
            mm_ld_dst_addr_out;
assign mm_ld_dst_addr_in = $unsigned(ldst_ext_base_i) + $unsigned(ldst_ext_offset_i);
logic buffer_rd_en,
      buffer_empty;

fifo # (
    .C_WIDTH (16+7)
)
dst_addr_buffer_inst (
    .clk (mmclk_i),       // Main System Clock  (Sync FIFO)
    .rst (rst_ni),       // FIFO Counter Reset (Clk
    .wr_en (lsu_opcode_i == LSU_VLD),     // FIFO Write Enable  (Clk)
    .rd_en (buffer_rd_en),     // FIFO Read Enable   (Clk)
    .din ({mm_ld_dst_addr_in, ld_dst_addr_i}),       // FIFO Data Input    (Clk)
    .dout ({mm_ld_dst_addr_out, dst_reg_addr_out}),      // FIFO Data Output   (Clk)
    .full (),      // FIFO FULL Status   (Clk)
    .empty (buffer_empty)      // FIFO EMPTY Status  (Clk)
);

always_ff @ (posedge sclk_i) begin
    if (!rst_ni) begin
        ld_dst_addr_o <= '0;
    end
    else begin
        ld_dst_addr_o <= dst_reg_addr_out;
    end
end

always_comb begin
    if (!rst_ni) begin
        m_axi_raddr_o <= '0;
        ld_dst_data_o <= '0;
        m_axi_rready_o <= '0;
        buffer_rd_en <= 1'b0;
    end
    else begin
        ld_dst_data_o <= m_axi_rdata_i;
        if (buffer_empty != 1'b1) begin
            // If buffer is not empty
            buffer_rd_en <= 1'b1; // Read from buffer
            m_axi_raddr_o <= mm_ld_dst_addr_out;
            m_axi_rready_o <= 1'b1; // Ready to receive reply
        end
    end
end
*/


logic[6:0] temp_dst_addr_hold; // Register for holding destination address until data is returned
always_ff @ (posedge mmclk_i) begin
    if (!rst_ni) begin
        load_waiting_o <= 1'b0;
        m_axi_raddr_o <= '0;
        ld_dst_addr_o <= '0;
        ld_dst_data_o <= '0;
        m_axi_rready_o <= '0;
    end
    else begin
        if (load_waiting_o == 1'b1) begin
            // While waiting on data, no other load can happen
            if (m_axi_rvalid_i == 1'b1) begin
                // Data on bus is valid
                load_waiting_o <= 1'b0; // No longer waiting, have data
                ld_dst_addr_o <= temp_dst_addr_hold;
                ld_dst_data_o <= m_axi_rdata_i;
            end
        end
        else if (lsu_opcode_i == LSU_VLD) begin
            // Ready to receive data
            m_axi_rready_o <= 1'b1;
            m_axi_raddr_o <= $unsigned(ldst_ext_base_i) + $unsigned(ldst_ext_offset_i);
            load_waiting_o <= 1'b1; // Waiting for reply
            temp_dst_addr_hold <= ld_dst_addr_i;
        end
        else begin
            ld_dst_addr_o <= '0;
            ld_dst_data_o <= '0;
        end
    end
end

endmodule
