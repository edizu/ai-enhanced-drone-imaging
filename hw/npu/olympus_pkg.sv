package olympus_pkg;

typedef union packed {
    logic [127:0] f;
    logic [7:0][15:0] e;
} t_v128;

typedef struct packed {
    logic option;
    logic vector;
    logic [4:0] opcode;
} opcode_type;

typedef struct packed {
    logic vs;
    logic vvvs;
    logic dtype;
    logic opt;
    logic[2:0] minor;
} t_opcodes;

typedef struct packed {
    logic [3:0] reserved;
    logic [6:0] src1;
    logic [6:0] src2;
    t_opcodes opcode;
    logic [6:0] dst;
} t_r_type;

typedef struct packed {
    logic [2:0] reserved;
    logic [7:0] imm;
    logic [6:0] src2;
    t_opcodes opcode;
    logic [6:0] dst;
} t_ir_type;

typedef struct packed {
    logic [1:0] reserved;
    logic [15:0] imm;
    t_opcodes opcode;
    logic [6:0] dst;
} t_i_type;

typedef union packed {
    t_r_type r;
    t_ir_type ir;
    t_i_type i;
} inst_type;

typedef enum logic[3:0] {
    VFV     = 4'b0000,
    VFR     = 4'b0001,
    VIV     = 4'b0010,
    VIR     = 4'b0011,
    VFS     = 4'b0100,
    VMEM    = 4'b0101,
    VIS     = 4'b0110,
    VBIT    = 4'b0111,
    SFR     = 4'b1000,
    COND    = 4'b1001,
    SIR     = 4'b1010,
    SII     = 4'b1011,
    // = 4'b1100,
    SMEM    = 4'b1101,
    // = 4'b1110,
    SBIT    = 4'b1111
} t_major_opcodes;

typedef enum bit[2:0] {
    NOP = 3'b000,
    MAC = 3'b001,
    MSC = 3'b010,
    MUL = 3'b011,
    DIV = 3'b100,
    ADD = 3'b101,
    SUB = 3'b110,
    ABS = 3'b111
} math_op;

typedef enum bit[3:0] {
    LSU_NOP,
    LSU_VLD,
    LSU_VST,
    LSU_LD,
    LSU_ST,
    LSU_SLAST
} t_lsu_opcodes;

typedef enum logic[3:0] {
    VFPU_NOP,
    VFPU_MAC = 4'h1,
    VFPU_MSC = 4'h2,
    VFPU_MUL = 4'h3,
    VFPU_ADD = 4'h4,
    VFPU_SUB = 4'h5,
    VFPU_MIN,
    VFPU_MAX,
    VFPU_ABS,
    VFPU_RLU
} t_vfpu_opcodes;

typedef enum logic[2:0] {
    SFPU_NOP,
    SFPU_MAC,
    SFPU_MSC,
    SFPU_MUL,
    SFPU_ADD,
    SFPU_SUB,
    SFPU_ABS
} t_sfpu_opcodes;

typedef enum logic[4:0] {
    VALU_NOP,
    VALU_MAC,
    VALU_MSC,
    VALU_MUL,
    VALU_ADD,
    VALU_SUB,
    VALU_ABS,
    VALU_RLU,
    VALU_MIN,
    VALU_MAX,
    VALU_EQ,
    VALU_LT,
    VALU_GE,
    VALU_NOT,
    VALU_AND,
    VALU_OR,
    VALU_XOR
} t_valu_opcodes;

typedef enum logic[4:0] {
    SALU_NOP,
    SALU_MAC,
    SALU_MSC,
    SALU_MUL,
    SALU_ADD,
    SALU_SUB,
    SALU_ABS,
    SALU_EQ,
    SALU_LT,
    SALU_GE,
    SALU_NOT,
    SALU_AND,
    SALU_OR,
    SALU_XOR,
    SALU_SRA,
    SALU_SRL,
    SALU_SLL
} t_salu_opcodes;

typedef enum logic {
    VSRC_VECTOR = 1'b0,
    VSRC_SCALAR = 1'b1
} t_vsrc_mux;

typedef enum logic {
    SSRC_REG = 1'b0,
    SSRC_IMM = 1'b1
} t_ssrc_mux;

typedef enum logic {
    OP_MAX,
    OP_MIN
} t_maxmin_op;

typedef enum logic {
    VVALID_VECTOR,
    VVALID_SCALAR
} t_vector_result_valid;

typedef enum logic[2:0] {
    FMAC_MAC,
    FMAC_MSC,
    FMAC_ADD,
    FMAC_SUB,
    FMAC_MUL
} t_fpu_mac_op;

typedef enum logic[2:0] {
    PCU_NOP,
    PCU_LD_IMM,
    PCU_VPERM,
    PCU_CPY_VS,
    PCU_CPY_SV
} t_pcu_opcodes;

endpackage