// *** OLYMPUS PROCESSING SYSTEM *** //
// Developer:       Martin Riis
// Date created:    12/11/2020
// File name:       olympus_top.sv
// Description:     Olympus top level module

import olympus_pkg::*;

module olympus # (
    parameter C_INST_WIDTH = 32,
    parameter C_ICACHE_DEPTH = 128,
    parameter C_WRITEBACK_BUFF_DEPTH = 4,
    parameter C_AXI_ADDR_WIDTH = 16,
    parameter C_AXI_DATA_WIDTH = 128
)
(
    input logic clk_i,
    input logic rst_ni,
    
    // Instruction Input //
    input logic inst_we_i,
    input logic[6:0] inst_addr_i,
    input logic[31:0] inst_data_i,
    
    // Stream Autowrite Input //
    input logic tvalid_i,
                tlast_i,
    output logic tready_o,
    input logic[127:0] tdata_i,
    
    // Stream Autowrite Output //
    output logic tvalid_o,
                 tlast_o,
    input logic tready_i,
    output logic[127:0] tdata_o,
    
    // Pseudo-AXI Interface //
    // Write port
    output logic[C_AXI_ADDR_WIDTH-1:0] m_axi_waddr_o,
    output logic[C_AXI_DATA_WIDTH-1:0] m_axi_wdata_o,
    output logic m_axi_wvalid_o,
    input logic m_axi_wready_i,
    // Read port
    output logic[C_AXI_ADDR_WIDTH-1:0] m_axi_raddr_o,
    input logic[C_AXI_DATA_WIDTH-1:0] m_axi_rdata_i,
    input logic m_axi_rvalid_i,
    output logic m_axi_rready_o
);

logic global_stall,
      hazard_stall;

logic spcu_stall,
      vpcu_stall;

///////////////////////
// Instruction Cache //
///////////////////////
// GOOD
logic[$clog2(C_ICACHE_DEPTH)-1:0] i_cache_fetch_addrA;
logic[C_INST_WIDTH-1:0] i_cache_fetch_doutA;
/*
i_cache # (
    .C_INST_WIDTH (C_INST_WIDTH),
    .C_ICACHE_DEPTH (C_ICACHE_DEPTH)
)
i_cache_inst (
    .clk_i (clk_i & ~global_stall),
    // Write port A
    .we_i (inst_we_i), // Active high write enable
    .dinA_i (inst_data_i),
    .addrA_i (inst_addr_i),
    // Read port B
    .addrB_i (i_cache_fetch_addrA),
    .doutB_o (i_cache_fetch_doutA),
    // Read port C
    .addrC_i (),
    .doutC_o ()
);
*/
dist_mem_gen_0 i_mem_inst (
    .a (inst_addr_i),        // input wire [6 : 0] a
    .d (inst_data_i),        // input wire [31 : 0] d
    .dpra (i_cache_fetch_addrA),  // input wire [6 : 0] dpra
    .clk (clk_i & ~global_stall),    // input wire clk
    .we (inst_we_i),      // input wire we
    .dpo (i_cache_fetch_doutA)    // output wire [31 : 0] dpo
);


///////////////////////
// Instruction Fetch //
///////////////////////
// GOOD?
inst_type cur_inst;
logic pc_set;
logic[$clog2(C_ICACHE_DEPTH)-1:0] pc_out,
                                  pc_in;

fetch # (
    .C_INST_WIDTH (C_INST_WIDTH),
    .C_ICACHE_DEPTH (C_ICACHE_DEPTH),
    .C_PC_OFFSET (1),
    .C_DUAL_DECODE_ENABLE (0)
)
fetch_inst (
    .clk_i (clk_i),
    .stall_i (global_stall | hazard_stall),
    .rst_ni (rst_ni),
    // Program counter
    .pc_set_i (pc_set),
    .pc_i (pc_in),
    .pc_o (pc_out),
    // Read port B
    .addrB_o (i_cache_fetch_addrA),
    .doutB_i (i_cache_fetch_doutA),
    // Read port C
    .addrC_o (),
    .doutC_i (),
    // Instruction out
    .instA_o (cur_inst),
    .instB_o ()
);

//////////////////////////
// Scalar Register File //
//////////////////////////
// GOOD
logic sreg_we;
logic[15:0] sreg_dinA,
            sreg_doutB,
            sreg_doutC,
            sreg_doutD;
logic[6:0] sreg_addrA,
           sreg_addrB,
           sreg_addrC,
           sreg_addrD;

sreg # (
    .C_SCALAR_WIDTH (16),
    .C_SCALAR_REG_DEPTH (128)
)
scalar_reg_inst (
    .clk_i (clk_i & ~global_stall),
    .we_i (sreg_we), // Active high write enable
    // Write port A
    .dinA_i (sreg_dinA),
    .addrA_i (sreg_addrA),
    // Read port B
    .addrB_i (sreg_addrB),
    .doutB_o (sreg_doutB),
    // Read port C
    .addrC_i (sreg_addrC),
    .doutC_o (sreg_doutC),
    // Read port D
    .addrD_i (sreg_addrD),
    .doutD_o (sreg_doutD)
);


//////////////////////////
// Vector Register File //
//////////////////////////
// GOOD
logic vreg_we;
logic[127:0] vreg_dinA,
             vreg_doutB,
             vreg_doutC,
             vreg_doutD;
logic[6:0] vreg_addrA,
           vreg_addrB,
           vreg_addrC,
           vreg_addrD;

vreg # (
    .C_VECTOR_WIDTH (128),
    .C_VECTOR_REG_DEPTH (128)
)
vector_reg_inst (
    .clk_i (clk_i & ~global_stall),
    .we_i (vreg_we), // Active high write enable
    // Write port A
    .dinA_i (vreg_dinA),
    .addrA_i (vreg_addrA),
    // Read port B
    .addrB_i (vreg_addrB),
    .doutB_o (vreg_doutB),
    // Read port C
    .addrC_i (vreg_addrC),
    .doutC_o (vreg_doutC),
    // Read port D
    .addrD_i (vreg_addrD),
    .doutD_o (vreg_doutD)
);


/////////////////////////
// Instruction Decoder //
/////////////////////////
logic lsu_mm_stall; // LSU memory-mapped interface stall

logic[127:0] vector_input_b;
logic[15:0] scalar_input_b;

// Destination data used for MAC/MSC instructions
t_v128 dst_vdata;
logic[15:0] dst_sdata;

logic[15:0] immediate;

t_ssrc_mux scalar_input_b_sel;
t_vsrc_mux vector_input_b_sel;

t_vfpu_opcodes vfpu_opcode;
t_valu_opcodes valu_opcode;
t_sfpu_opcodes sfpu_opcode;
t_salu_opcodes salu_opcode;
t_pcu_opcodes pcu_opcode;
t_lsu_opcodes lsu_opcode;

logic[6:0] dst_addr;

logic lsu_tlast,
      lsu_rst_tlast,
      load_waiting;

/*
* Selects source of vector unit's input B. This can either be the vector register
* specified by the decoder address output (vreg_doutD), or the output from 
* the scalar register replicated eight times for form a 128b vetor. 
*/
always_comb begin
    if (vector_input_b_sel == VSRC_VECTOR) begin
        vector_input_b <= vreg_doutC;
    end
    else begin
        vector_input_b <= {sreg_doutC, sreg_doutC,
                           sreg_doutC, sreg_doutC,
                           sreg_doutC, sreg_doutC,
                           sreg_doutC, sreg_doutC};
    end
end

/*
* Selects source of scalar unit's input B. This can either be the scalar register
* specified by the decoder address output (sreg_doutD), or the immediate 
* value stored within the instruction. In the case of immediates, the 8b value
* is sign extended to 16b.
*/
assign scalar_input_b = logic'(scalar_input_b_sel) ? immediate : sreg_doutC;

decode_v2 # (
    .C_INST_WIDTH (32),
    .C_VREG_ADDR_WIDTH (7),
    .C_SREG_ADDR_WIDTH (7),
    .C_ICACHE_DEPTH (128)
)
decode_basic_inst (
    .clk_i (clk_i),
    .clk_en_i (~(global_stall | load_waiting | vfpu_vector_stall | vfpu_scalar_stall | 
                 sfpu_stall | salu_stall | vpcu_stall | spcu_stall | lsu_mm_stall)),
    .rst_ni (rst_ni),
    .inst_i (cur_inst), // Instruction at PC
    
    // Program Counter Update //
    .pc_o (pc_in),
    .update_pc_o (pc_set), // Active high
    .hazard_stall_o (hazard_stall),
    
    .sreg_data2_i (sreg_doutC),
    
    // Register Read Port Address Control //
    .vreg_addr1_o (vreg_addrB),
    .vreg_addr2_o (vreg_addrC),
    .sreg_addr1_o (sreg_addrB),
    .sreg_addr2_o (sreg_addrC),
             
    // Input MUX Control //
    .scalar_input_b_sel_o (scalar_input_b_sel), // register or immediate
    .vector_input_b_sel_o (vector_input_b_sel), // vector or scalar
    
    .vreg_snoop_addr_i (vreg_addrA),
    .sreg_snoop_addr_i (sreg_addrA),
    
    .dst_addr_o (dst_addr), // destination address
    .dst_vdata_o (dst_vdata),
    .dst_sdata_o (dst_sdata),
    
    .immediate_o (immediate), // 16-bit sign extended immediate
    
    .vfpu_stall_i (vfpu_vector_stall | vfpu_scalar_stall),
    .valu_stall_i (1'b1),
    .sfpu_stall_i (sfpu_stall),
    .salu_stall_i (salu_stall),
    .pcu_stall_i (vpcu_stall | spcu_stall),
    .lsu_mm_stall_i (lsu_mm_stall),
                      
    // Execution Unit Control //
    .vfpu_opcode_o (vfpu_opcode), // VFPU
    .valu_opcode_o (valu_opcode), // VALU
    .sfpu_opcode_o (sfpu_opcode), // SFPU
    .salu_opcode_o (salu_opcode), // SALU
    .pcu_opcode_o (pcu_opcode), // PCU
    .lsu_opcode_o (lsu_opcode), // LSU
    
    // LSU Interface //
    .lsu_tlast_i (lsu_tlast),
    .lsu_rst_tlast_o (lsu_rst_tlast)
);


//////////////////////////////////////
// Vector Floating-Point Unit (FPU) //
//////////////////////////////////////
// GOOD?
logic[15:0] result_scalar_vfpu;
logic[6:0] dst_vaddr_vfpu,
           dst_saddr_vfpu;
t_v128 result_vector_vfpu;

vfpu # (
    .C_FADD_LATENCY (4),
    .C_FMUL_LATENCY (6),
    .C_MAXMIN_LATENCY (0)
)
vfpu_inst (
    .clk_i (clk_i & ~(vfpu_vector_stall | vfpu_scalar_stall | global_stall)),
    .stall_i (vfpu_vector_stall | vfpu_scalar_stall | global_stall),
    
    .dst_addr_i (dst_addr),
    .dst_vaddr_o (dst_vaddr_vfpu),
    .dst_saddr_o (dst_saddr_vfpu),
    
    .vfpu_opcode_i (vfpu_opcode),
    .a_i (t_v128'(vreg_doutB)),
    .b_i (t_v128'(vector_input_b)),
    .vector_result_o (result_vector_vfpu),
    .scalar_result_o (result_scalar_vfpu)
);


//////////////////////////////////////
// Scalar Floating-Point Unit (FPU) //
//////////////////////////////////////
// GOOD?
logic[6:0] dst_addr_sfpu;
logic[15:0] result_sfpu;

sfpu #(
    .C_FADD_LATENCY (4),
    .C_FMUL_LATENCY (6)
)
sfpu_inst (
    .clk_i (clk_i & ~(sfpu_stall | global_stall)),
    
    .dst_addr_i (dst_addr),
    .dst_addr_o (dst_addr_sfpu),
    
    .sfpu_opcode_i (sfpu_opcode),
    .a_i (sreg_doutC),
    .b_i (scalar_input_b),
    .result_o (result_sfpu)
);


////////////////////////////////////////
// Scalar Arithmetic Logic Unit (ALU) //
////////////////////////////////////////
// GOOD?
logic[6:0] dst_addr_salu;
logic[15:0] result_salu;

salu #(
    .C_IMUL_LATENCY (3)
)
salu_inst (
    .clk_i (clk_i & ~(salu_stall | global_stall)),
    .stall_i (salu_stall), // For asynchronous results
    
    .dst_addr_i (dst_addr),
    .dst_addr_o (dst_addr_salu),
    
    .salu_opcode_i (salu_opcode),
    .a_i (sreg_doutC),
    .b_i (scalar_input_b),
    .result_o (result_salu)
);


/////////////////////////////////
// Permute and Copy Unit (PCU) //
/////////////////////////////////
t_v128 vresult_pcu;
logic[15:0] sresult_pcu;
logic[6:0] dst_vaddr_pcu,
           dst_saddr_pcu;
pcu pcu_inst (
    .clk_i (clk_i & ~(vpcu_stall | spcu_stall | global_stall)),
    .dst_addr_i (dst_addr),
    .dst_vaddr_o (dst_vaddr_pcu),
    .dst_saddr_o (dst_saddr_pcu),
    .op_i (pcu_opcode),
    .vinput_i (vreg_doutB),
    .index_i (vreg_doutC),
    .immediate_i (immediate),
    .sinput_i (sreg_doutB),
    .vresult_o (vresult_pcu),
    .sresult_o (sresult_pcu) 
);


///////////////////////////////
// Load and Store Unit (LSU) //
///////////////////////////////
/*
* Responsible for loading and storing data into
* and from registers.
* All LSU operations operate on 128b vectors, 
* however, scalars can also be loaded or stored
* in this way through the use of the copy 
* instructions.
* The LSU also includes stream autowrite 
* functionality, where data recceived on the 
* AXI Stream input is automatically written
* to registers v112-v127 in ascending order.
* Similar functionality is present on the 
* AXI Stream output, except only a single
* output register is available, v111.
* If a write request cannot be granted by
* the receiver, the LSU will assert the 
* gloabal_stall signal, which temporarily 
* stalls all CPU units (except the LSU)
* untill the receiver is ready.
*/
logic stream_stall;
logic[127:0] stream_vdin,
             mm_vdin;
logic[6:0] stream_vaddr,
           mm_vaddr;
lsu # (
    .C_AXI_ADDR_WIDTH (16),
    .C_AXI_DATA_WIDTH (128)
)
lsu_inst (
    .sclk_i (clk_i),
    .mmclk_i (clk_i & ~lsu_mm_stall),
    .rst_ni (rst_ni),
    
    .lsu_opcode_i (lsu_opcode),
    
    // Memory Mapped Vector Load/Store //
    .ldst_ext_base_i (vreg_doutB), // load/store external base address
    .ldst_ext_offset_i (vreg_doutC), // load/store external address offset
    .ld_dst_addr_i (dst_addr), // load local destination register address input
    .st_data_i (), // store data
    .ld_dst_addr_o (mm_vaddr), // load local destination register address output
    .ld_dst_data_o (mm_vdin), // load data output
    .load_waiting_o (load_waiting),
    
    // Stream Autowrite Input //
    .stream_stall_i (stream_stall),
    .stream_vdata_o (stream_vdin), // To arbiter
    .stream_vaddr_o (stream_vaddr), // To arbiter
    .stream_tlast_o (lsu_tlast), 
    .stream_rst_tlast_i (lsu_rst_tlast),
    // External AXI Stream input signals
    .tvalid_i (tvalid_i),
    .tlast_i (tlast_i),
    .tready_o (tready_o),
    .tdata_i (tdata_i),
    
    // Stream Autowrite Output //
    .vreg_waddr_snoop_i (vreg_addrA), // To vreg (snoop)
    .vreg_raddr3_o (vreg_addrD), // To vreg
    .vreg_rdata3_i (vreg_doutD), // To vreg
    .global_stall_o (global_stall), // Global (multiple sinks)
    // External AXI Stream output signals
    .tvalid_o (tvalid_o), 
    .tlast_o (tlast_o),
    .tready_i (tready_i),
    .tdata_o (tdata_o),

    // Pseudo-AXI Interface - All External //
    // Write port
    .m_axi_waddr_o (m_axi_waddr_o),
    .m_axi_wdata_o (m_axi_wdata_o),
    .m_axi_wvalid_o (m_axi_wvalid_o),
    .m_axi_wready_i (m_axi_wready_i),
    // Read port
    .m_axi_raddr_o (m_axi_raddr_o),
    .m_axi_rdata_i (m_axi_rdata_i),
    .m_axi_rvalid_i (m_axi_rvalid_i),
    .m_axi_rready_o (m_axi_rready_o)
);


/////////////////////
// Writeback Stage //
/////////////////////
/*
* Responsible for arbitrating write access to the 
* vector and scalar register files.
* Each input has a shallow FIFO to prevent write
* fails from immediatly stalling the execution 
* unit that submitted the write request.
* A priority system ensures that more important
* operations have higher write privileges.
* The priority system also includes last write
* memory which ensures that a higher priority 
* write cannot continually demand write access
* on every cycle.
* 0 = highest priority
* 3 = lowest priority
*/

// Vector Register Buffered Priority Arbiter //
logic vstall[3:0];
assign stream_stall = vstall[3];
assign vfpu_vector_stall = vstall[2];
assign vpcu_stall = vstall[1];
assign lsu_mm_stall = vstall[0];
assign vreg_we = |vreg_addrA; // Write whenever address != 0
buffered_priority_arbiter # (
    .C_WIDTH (128),
    .C_ADDR_WIDTH (7),
    .C_INPUT_FIFO_DEPTH (4)
)
vreg_arbiter_inst (
    .clk_i (clk_i & ~global_stall),
    .rst_ni (rst_ni),
    // Data input from execution units
    .din_i ({stream_vdin, // From LSU
            result_vector_vfpu, // From VFPU (vector result)
            vresult_pcu,
            mm_vdin}),
    // Destination address from execution units
    .addr_i ({stream_vaddr, // From LSU
              dst_vaddr_vfpu, // From VFPU (vector result)
              dst_vaddr_pcu,
              mm_vaddr}),
    // Write whenever address != 0
    .req_i ({|stream_vaddr,
             |dst_vaddr_vfpu,
             |dst_vaddr_pcu,
             |mm_vaddr}),
    // Stall ouptuts to execution units
    .stall_o (vstall),
    // Vector register file write interface
    .dout_o (vreg_dinA), // To vreg
    .addr_o (vreg_addrA) // To vreg
);

// Scalar register buffered priority arbiter
assign sreg_we = |sreg_addrA;
buffered_priority_arbiter # (
    .C_WIDTH (16),
    .C_ADDR_WIDTH (7),
    .C_INPUT_FIFO_DEPTH (4)
)
sreg_arbiter_inst (
    .clk_i (clk_i & ~global_stall),
    .rst_ni (rst_ni),
    // Data input from execution units
    .din_i ({result_scalar_vfpu, // From VFPU (scalar result)
             sresult_pcu, // From PCU
             result_salu, // From SALU
             result_sfpu}), // From SFPU
    // Destination address from execution units
    .addr_i ({dst_saddr_vfpu, // From VFPU (scalar result)
              dst_saddr_pcu, // From PCU
              dst_addr_salu, // From SALU
              dst_addr_sfpu}), // From SFPU
    // Write whenever address != 0
    .req_i ({|dst_saddr_vfpu,
             |dst_saddr_pcu,
             |dst_addr_salu,
             |dst_addr_sfpu}),
    // Stall ouptuts to execution units
    .stall_o ('{vfpu_scalar_stall, // To VFPU
               spcu_stall, // To PCU
               salu_stall, // To SALU
               sfpu_stall}), // To SFPU
    // Scalar register file write interface
    .dout_o (sreg_dinA), // To sreg
    .addr_o (sreg_addrA) // To sreg
);

endmodule
