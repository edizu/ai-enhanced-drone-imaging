module clz16 (
    input logic clk_i,
    input logic[15:0] x_i,
    output logic[3:0] out_o
);

logic[15:0] x_int;

always_comb begin
    for (int i=0; i<16; i++) begin
        x_int[i] <= x_i[15-i];
    end
end

always_comb begin
    casex (x_int)
        16'b0000_0000_0000_0000 : out_o <= 15;
        16'b0000_0000_0000_0001 : out_o <= 15;
        16'b0000_0000_0000_001X : out_o <= 14;
        16'b0000_0000_0000_01XX : out_o <= 13;
        16'b0000_0000_0000_1XXX : out_o <= 12;
        16'b0000_0000_0001_XXXX : out_o <= 11;
        16'b0000_0000_001X_XXXX : out_o <= 10;
        16'b0000_0000_01XX_XXXX : out_o <= 9;
        16'b0000_0000_1XXX_XXXX : out_o <= 8;
        16'b0000_0001_XXXX_XXXX : out_o <= 7;
        16'b0000_001X_XXXX_XXXX : out_o <= 6;
        16'b0000_01XX_XXXX_XXXX : out_o <= 5;
        16'b0000_1XXX_XXXX_XXXX : out_o <= 4;
        16'b0001_XXXX_XXXX_XXXX : out_o <= 3;
        16'b001X_XXXX_XXXX_XXXX : out_o <= 2;
        16'b01XX_XXXX_XXXX_XXXX : out_o <= 1;
        16'b1XXX_XXXX_XXXX_XXXX : out_o <= 0;
    endcase
end
endmodule


module clz16_ # (
    parameter C_HAS_OUTPUT_REG = 0
)
(
    input logic clk_i,
    input logic[15:0] x_i,
    output logic[4:0] out_o
);

logic[3:0] a_int;
logic[1:0] z_int[4];
logic q_int;
logic[1:0] y_int;
logic[4:0] out_int;

if (C_HAS_OUTPUT_REG == 0) begin
    assign out_o = out_int;
end
else begin
    always_ff @ (posedge clk_i) begin
        out_o <= out_int;
    end
end

genvar i;
for (i=0; i<4; i++) begin
    lzc4 lzc4_inst (
        .x_i (x_i[(3-i)*4+3:(3-i)*4]),
        .a_o (a_int[i]),
        .z_o (z_int[i])
    );
end

lze4 lze4_inst (
    .a_i (a_int),
    .q_o (q_int),
    .y_o (y_int)
);

assign out_int[3:2] = y_int;
assign out_int[1:0] = z_int[y_int];
endmodule

// 4-bit leading zero encoder
module lze4 (
    input logic[3:0] a_i,
    output logic q_o,
    output logic[1:0] y_o
);

assign q_o = &a_i;
assign y_o[0] = a_i[0] & (~a_i[1] | (a_i[2] & ~a_i[3]));
assign y_o[1] = a_i[0] & a_i[1] & (~a_i[2] | ~a_i[3]);
endmodule

// 4-bit leading zero counter
module lzc4 (
    input logic[3:0] x_i,
    output logic a_o,
    output logic[1:0] z_o
);

assign a_o = ~|x_i; // Reuction NOR, indicates if the result is zero
assign z_o[1] = ~|x_i[3:2];
assign z_o[0] = (~x_i[1] | x_i[2]) & ~x_i[3];

endmodule



// Variable width flip-flop with bypass option
module flip_flop # (
    parameter C_BYPASS = 0,
    parameter C_WIDTH = 1,
    parameter C_DELAY = 1
)
(
    input logic clk_i,
    input logic[C_WIDTH-1:0] in_i,
    output logic[C_WIDTH-1:0] out_o
);

// Generates C_DELAY flip-flops with a width of 
// C_WIDTH to provide a delay of C_DELAY
logic[C_WIDTH-1:0] int_sig[C_DELAY+1];
assign int_sig[0] = in_i;
assign out_o = int_sig[C_DELAY];
genvar i;
for (i=0; i<C_DELAY; i++) begin
    flip_flop_single # (
        .C_BYPASS (C_BYPASS),
        .C_WIDTH (C_WIDTH)
    )
    flip_flop_single_inst (
        .clk_i (clk_i),
        .in_i (int_sig[i]),
        .out_o (int_sig[i+1])
    );
end
endmodule

// Single variable width flip-flop with bypass option
module flip_flop_single # (
    parameter C_BYPASS = 0,
    parameter C_WIDTH = 1
)
(
    input logic clk_i,
    input logic[C_WIDTH-1:0] in_i,
    output logic[C_WIDTH-1:0] out_o
);

always_comb begin
    if (C_BYPASS == 1)
        out_o <= in_i;
end

always_ff @ (posedge clk_i) begin
    if (C_BYPASS == 0)
        out_o <= in_i;
end
endmodule