import olympus_pkg::*;

module pcu (
    input logic clk_i,
                rst_ni,
    input t_pcu_opcodes op_i,
    input logic[6:0] dst_addr_i,
    output logic[6:0] dst_vaddr_o,
                      dst_saddr_o,
    input t_v128 vinput_i,
                 index_i,
    input logic[15:0] immediate_i,
                      sinput_i,
    output t_v128 vresult_o,
    output logic[15:0] sresult_o
);

t_v128 permute_result,
       copy_vresult;
logic[15:0] copy_sresult;

always_ff @ (posedge clk_i) begin
    if (rst_ni == 1'b0) begin
        vresult_o.f <= '0;
        sresult_o <= '0;
        dst_vaddr_o <= '0;
        dst_saddr_o <= '0;
    end
    else begin
        unique case (op_i) 
            PCU_NOP : begin
                vresult_o.f <= '0;
                sresult_o <= '0;
                dst_vaddr_o <= '0;
                dst_saddr_o <= '0;
            end
            PCU_VPERM : begin
                vresult_o.f <= permute_result.f;
                sresult_o <= '0;
                dst_vaddr_o <= dst_addr_i;
                dst_saddr_o <= '0;
            end
            PCU_CPY_VS : begin
                vresult_o.f <= '0;
                sresult_o <= copy_sresult;
                dst_saddr_o <= dst_addr_i;
                dst_vaddr_o <= '0;
            end
            PCU_CPY_SV : begin
                vresult_o.f <= copy_vresult.f;
                sresult_o <= '0;
                dst_vaddr_o <= dst_addr_i;
                dst_saddr_o <= '0;
            end
            PCU_LD_IMM : begin
                sresult_o <= immediate_i;
                dst_saddr_o <= dst_addr_i;
            end
        endcase
    end
end

always_comb begin
    copy_sresult <= vinput_i.e[immediate_i[2:0]];
end

always_comb begin
    copy_vresult.f <= vinput_i.f;
    copy_vresult.e[immediate_i[2:0]] <= sinput_i;
end

////////////////////
// Vector Permute //
////////////////////
/*
* The permute instruction shuffles the order of
* elements in a vector based on indexes in 
* another vector of the same length.
* 
* |A|B|C|D|E|F|G|H| - input_i
* |6|5|1|2|1|4|1|9| - index_i
* |G|F|B|C|B|E|B|A| - permute result
*/
always_comb begin
    for (int i=0; i<8; i++) begin
        permute_result.e[i] <= vinput_i.e[index_i.e[i]];
    end
end
endmodule
