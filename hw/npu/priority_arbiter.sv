module priority_arbiter (
    input logic clk_i,
    input logic rst_ni,
    
    input logic[3:0] request_i,
    output logic[3:0] grant_o
);

logic[3:0] request_hist;

assign request_hist = request_i == grant_o ? request_i : request_i & ~grant_o;

always_ff @ (posedge clk_i) begin
    if (!rst_ni) begin
        grant_o <= 4'b0000;
    end
    else begin
        unique case (request_hist)
            4'b0000 : begin grant_o <= 4'b0000; end
            4'b0001 : begin grant_o <= 4'b0001; end
            4'b0010 : begin grant_o <= 4'b0010; end
            4'b0011 : begin grant_o <= 4'b0001; end
            4'b0100 : begin grant_o <= 4'b0100; end
            4'b0101 : begin grant_o <= 4'b0001; end
            4'b0110 : begin grant_o <= 4'b0010; end
            4'b0111 : begin grant_o <= 4'b0001; end
            4'b1000 : begin grant_o <= 4'b1000; end
            4'b1001 : begin grant_o <= 4'b0001; end
            4'b1010 : begin grant_o <= 4'b0010; end
            4'b1011 : begin grant_o <= 4'b0001; end
            4'b1100 : begin grant_o <= 4'b0100; end
            4'b1101 : begin grant_o <= 4'b0001; end
            4'b1110 : begin grant_o <= 4'b0010; end
            4'b1111 : begin grant_o <= 4'b0001; end
        endcase
    end
end
endmodule
