import olympus_pkg::*;

module sfpu #(
    parameter C_FADD_LATENCY = 4,
    parameter C_FMUL_LATENCY = 6
)
(
    input logic clk_i,
    
    input logic[6:0] dst_addr_i,
    output logic[6:0] dst_addr_o,
    
    input t_sfpu_opcodes sfpu_opcode_i,
    input logic[15:0] a_i,
                      b_i,
    output logic[15:0] result_o
);

logic[15:0] result_add,
            result_mul,
            result_single;

logic op_smul[2],
      op_sadd[2],
      op_single;

logic[6:0] dst_addr_smul,
           dst_addr_sadd,
           dst_addr_single;

logic addsub_op; // 0=add, 1=subtract

always_comb begin
    op_smul[0] <= 1'b0;
    op_sadd[0] <= 1'b0;
    op_single <= 1'b0;
    addsub_op <= 1'b0;
    
    unique case (sfpu_opcode_i)
        SFPU_ADD : begin
            op_sadd[0] <= 1'b1;
        end
        SFPU_SUB : begin
            op_sadd[0] <= 1'b1;
            addsub_op <= 1'b1;
        end
        SFPU_MUL : begin
            op_smul[0] <= 1'b1;
        end
        SFPU_ABS : begin
            op_single <= 1'b1;
            result_single <= {1'b0, a_i[14:0]};
        end
    endcase
    
    result_o <= '0;
    dst_addr_o <= '0;
    
    unique case ({op_smul[1], op_sadd[1], op_single})
        3'b100 : begin
            result_o <= result_mul;
            dst_addr_o <= dst_addr_smul;
        end
        3'b010 : begin
            result_o <= result_add;
            dst_addr_o <= dst_addr_sadd;
        end
        3'b001 : begin
            result_o <= result_single;
            dst_addr_o <= dst_addr_i;
        end
    endcase
end

////////////////////////////////////////////////
// Delay elements for operation indicators
////////////////////////////////////////////////
flip_flop # (
    .C_WIDTH (1),
    .C_DELAY (C_FMUL_LATENCY)
)
op_smul_buff_inst (
    .clk_i (clk_i),
    .in_i (op_smul[0]),
    .out_o (op_smul[1])
);

flip_flop # (
    .C_WIDTH (1),
    .C_DELAY (C_FADD_LATENCY)
)
op_sadd_buff_inst (
    .clk_i (clk_i),
    .in_i (op_sadd[0]),
    .out_o (op_sadd[1])
);

////////////////////////////////////////////////
// Delay elements for destination address
////////////////////////////////////////////////
flip_flop # (
    .C_WIDTH (7),
    .C_DELAY (C_FMUL_LATENCY)
)
id_tag_smul_buff_inst (
    .clk_i (clk_i),
    .in_i (dst_addr_i),
    .out_o (dst_addr_smul)
);

flip_flop # (
    .C_WIDTH (7),
    .C_DELAY (C_FADD_LATENCY)
)
id_tag_sadd_buff_inst (
    .clk_i (clk_i),
    .in_i (dst_addr_i),
    .out_o (dst_addr_sadd)
);

////////////////////////////////////////////////
// Execution units
////////////////////////////////////////////////
floating_point_0 fpu_add_inst (
    .aclk (clk_i),
    .s_axis_a_tvalid (1'b1),
    .s_axis_a_tdata (a_i),
    .s_axis_b_tvalid (1'b1),
    .s_axis_b_tdata (b_i),
    .s_axis_operation_tvalid (1'b1),
    .s_axis_operation_tdata ({7'b0, addsub_op}),
    .m_axis_result_tvalid (),
    .m_axis_result_tdata (result_add)
);

floating_point_1 fpu_mul_inst (
    .aclk (clk_i),
    .s_axis_a_tvalid (1'b1),
    .s_axis_a_tdata (a_i),
    .s_axis_b_tvalid (1'b1),
    .s_axis_b_tdata (b_i),
    .m_axis_result_tvalid (),
    .m_axis_result_tdata (result_mul)
);
endmodule