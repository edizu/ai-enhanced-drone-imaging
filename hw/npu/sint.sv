import olympus_pkg::*;

module salu #(
    parameter C_IMUL_LATENCY = 3
)
(
    input logic clk_i,
                stall_i,
    
    input logic[6:0] dst_addr_i,
    output logic[6:0] dst_addr_o,
    
    input t_salu_opcodes salu_opcode_i,
    input logic[15:0] a_i,
                      b_i,
    output logic[15:0] result_o
);

logic[15:0] result_single,
            result_mul;

logic op_smul[2],
      op_single;

logic[3:0] dst_addr_smul;

always_comb begin
    if (stall_i == 1'b0) begin
        op_smul[0] <= 1'b0;
        op_single <= 1'b0;
        
        unique case (salu_opcode_i)
            SALU_ADD : begin
                op_single <= 1'b1;
                result_single <= a_i + b_i;
            end
            SALU_SUB : begin
                op_single <= 1'b1;
                result_single <= a_i - b_i;
            end
            SALU_MUL : begin
                op_smul[0] <= 1'b1;
            end
            SALU_EQ : begin
                op_single <= 1'b1;
                result_single <= {15'b0, a_i == b_i};
            end
            SALU_LT : begin
                op_single <= 1'b1;
                result_single <= {15'b0, a_i < b_i};
            end
            SALU_GE : begin
                op_single <= 1'b1;
                result_single <= {15'b0, a_i >= b_i};
            end
            SALU_NOT : begin
                op_single <= 1'b1;
                result_single <= ~a_i;
            end
            SALU_AND : begin
                op_single <= 1'b1;
                result_single <= a_i & b_i;
            end
            SALU_OR : begin
                op_single <= 1'b1;
                result_single <= a_i | b_i;
            end
            SALU_XOR : begin
                op_single <= 1'b1;
                result_single <= a_i ^ b_i;
            end
            SALU_ABS : begin
                op_single <= 1'b1;
                if (a_i[15] == 1'b1)
                    result_single <= -a_i;
                else
                    result_single <= a_i;            
            end
            SALU_SRA : begin
                op_single <= 1'b1;
                result_single <= a_i >>> b_i;
            end
            SALU_SRL : begin
                op_single <= 1'b1;
                result_single <= a_i >> b_i;
            end
            SALU_SLL : begin
                op_single <= 1'b1;
                result_single <= a_i << b_i;
            end
        endcase
    end
end

always_comb begin
    if (stall_i == 1'b0) begin
        result_o <= '0;
        dst_addr_o <= '0;
    
        unique case ({op_smul[1], op_single})
            2'b10 : begin
                result_o <= result_mul;
                dst_addr_o <= dst_addr_smul;
            end
            2'b01 : begin
                result_o <= result_single;
                dst_addr_o <= dst_addr_i;
            end
        endcase
    end
end

////////////////////////////////////////////////
// Delay element for operation indicator
////////////////////////////////////////////////
flip_flop # (
    .C_WIDTH (1),
    .C_DELAY (C_IMUL_LATENCY)
)
op_smul_buff_inst (
    .clk_i (clk_i),
    .in_i (op_smul[0]),
    .out_o (op_smul[1])
);

////////////////////////////////////////////////
// Delay element for destination address
////////////////////////////////////////////////
flip_flop # (
    .C_WIDTH (7),
    .C_DELAY (C_IMUL_LATENCY)
)
id_tag_smul_buff_inst (
    .clk_i (clk_i),
    .in_i (dst_addr_i),
    .out_o (dst_addr_smul)
);

////////////////////////////////////////////////
// Execution units
////////////////////////////////////////////////
mult_gen_0 imul_inst (
    .CLK (clk_i),
    .A (a_i),
    .B (b_i),
    .P (result_mul)
);
endmodule