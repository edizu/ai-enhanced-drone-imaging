// *** OLYMPUS PROCESSING SYSTEM *** //
// Developer:       Martin Riis
// Date created:    12/11/2020
// File name:       sreg.sv
// Description:     Scalar register file, generates distributed memory

module sreg # (
    parameter C_SCALAR_WIDTH = 16,
    parameter C_SCALAR_REG_DEPTH = 16
)
(
    input logic clk_i,
    input logic we_i, // Active high write enable
    // Write port A
    input logic[C_SCALAR_WIDTH-1:0] dinA_i,
    input logic[$clog2(C_SCALAR_REG_DEPTH)-1:0] addrA_i,
    // Read port B
    input logic[$clog2(C_SCALAR_REG_DEPTH)-1:0] addrB_i,
    output logic[C_SCALAR_WIDTH-1:0] doutB_o,
    // Read port C
    input logic[$clog2(C_SCALAR_REG_DEPTH)-1:0] addrC_i,
    output logic[C_SCALAR_WIDTH-1:0] doutC_o,
    // Read port D
    input logic[$clog2(C_SCALAR_REG_DEPTH)-1:0] addrD_i,
    output logic[C_SCALAR_WIDTH-1:0] doutD_o
);

logic [C_SCALAR_WIDTH-1:0] mem_array [C_SCALAR_REG_DEPTH] = '{default:16'h0000};

// Asynchronous read
assign doutB_o = mem_array[addrB_i];
assign doutC_o = mem_array[addrC_i];
assign doutD_o = mem_array[addrD_i];

// Synchronous write
always_ff @ (posedge clk_i) begin
    if (we_i == 1'b1) begin
        mem_array[addrA_i] <= dinA_i;
    end
end

endmodule