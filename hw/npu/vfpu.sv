import olympus_pkg::*;

module vfpu #(
    parameter C_FADD_LATENCY = 4,
    parameter C_FMUL_LATENCY = 6,
    parameter C_MAXMIN_LATENCY = 0
)
(
    input logic clk_i,
                stall_i,
    
    input logic[6:0] dst_addr_i,
    output logic[6:0] dst_vaddr_o,
                      dst_saddr_o,
    
    input t_vfpu_opcodes vfpu_opcode_i,
    input t_v128 a_i,
                 b_i,
    output t_v128 vector_result_o,
    output logic[15:0] scalar_result_o
);

t_v128 result_add,
       result_mul,
       result_single;
       
logic[15:0] result_maxmin;

logic op_vmul[2],
      op_vadd[2],
      op_single,
      op_maxmin[2];

logic[6:0] dst_addr_vmul,
           dst_addr_vadd,
           dst_addr_single,
           dst_addr_maxmin;

logic addsub_op; // 0=add, 1=subtract
t_maxmin_op maxmin_op;

always_comb begin
    op_vmul[0] <= 1'b0;
    op_vadd[0] <= 1'b0;
    op_single <= 1'b0;
    addsub_op <= 1'b0;
    op_maxmin[0] <= 1'b0;
    
    unique case (vfpu_opcode_i)
        VFPU_ADD : begin
            op_vadd[0] <= 1'b1;
        end
        VFPU_SUB : begin
            op_vadd[0] <= 1'b1;
            addsub_op <= 1'b1;
        end
        VFPU_MUL : begin
            op_vmul[0] <= 1'b1;
        end
        VFPU_ABS : begin
            op_single <= 1'b1;
            for (int i=0; i<8; i++)
                result_single.e[i] <= {1'b0, a_i.e[i][14:0]};
        end
        VFPU_RLU : begin
            op_single <= 1'b1;
        end
        VFPU_MAX : begin
            op_maxmin[0] <= 1'b1;
            maxmin_op <= OP_MAX;
        end
        VFPU_MIN : begin
            op_maxmin[0] <= 1'b1;
            maxmin_op <= OP_MIN;
        end
    endcase
    
    if (stall_i == 1'b0) begin
        scalar_result_o <= '0;
        vector_result_o <= '0;
        dst_vaddr_o <= '0;
        dst_saddr_o <= '0;
        
        unique case ({op_vmul[1], op_vadd[1], op_single, op_maxmin[1]})
            4'b1000 : begin
                vector_result_o <= result_mul;
                dst_vaddr_o <= dst_addr_vmul;
            end
            4'b0100 : begin
                vector_result_o <= result_add;
                dst_vaddr_o <= dst_addr_vadd;
            end
            4'b0010 : begin
                vector_result_o <= result_single;
                dst_vaddr_o <= dst_addr_i;
            end
            4'b0001 : begin
                scalar_result_o <= result_maxmin;
                dst_saddr_o <= dst_addr_maxmin;
            end
        endcase
    end
    else begin
        scalar_result_o <= '0;
        vector_result_o <= '0;
        dst_vaddr_o <= '0;
        dst_saddr_o <= '0;
    end
end

////////////////////////////////////////////////
// Delay elements for operation indicators
////////////////////////////////////////////////
flip_flop # (
    .C_WIDTH (1),
    .C_DELAY (C_FMUL_LATENCY)
)
op_vmul_buff_inst (
    .clk_i (clk_i),
    .in_i (op_vmul[0]),
    .out_o (op_vmul[1])
);

flip_flop # (
    .C_WIDTH (1),
    .C_DELAY (C_FADD_LATENCY)
)
op_vadd_buff_inst (
    .clk_i (clk_i),
    .in_i (op_vadd[0]),
    .out_o (op_vadd[1])
);

flip_flop # (
    .C_WIDTH (1),
    .C_DELAY ((C_MAXMIN_LATENCY+1)*3)
)
op_vmaxmin_buff_inst (
    .clk_i (clk_i),
    .in_i (op_maxmin[0]),
    .out_o (op_maxmin[1])
);

////////////////////////////////////////////////
// Delay elements for destination address
////////////////////////////////////////////////
flip_flop # (
    .C_WIDTH (7),
    .C_DELAY (C_FMUL_LATENCY)
)
dst_addr_vmul_buff_inst (
    .clk_i (clk_i),
    .in_i (dst_addr_i),
    .out_o (dst_addr_vmul)
);

flip_flop # (
    .C_WIDTH (7),
    .C_DELAY (C_FADD_LATENCY)
)
dst_addr_vadd_buff_inst (
    .clk_i (clk_i),
    .in_i (dst_addr_i),
    .out_o (dst_addr_vadd)
);

flip_flop # (
    .C_WIDTH (7),
    .C_DELAY ((C_MAXMIN_LATENCY+1)*3)
)
dst_addr_vmaxmin_buff_inst (
    .clk_i (clk_i),
    .in_i (dst_addr_i),
    .out_o (dst_addr_maxmin)
);

////////////////////////////////////////////////
// Execution units
////////////////////////////////////////////////
vfpu_add vfpu_add_inst (
    .clk_i (clk_i),
    .addsub_op_i (addsub_op),
    .a_i (a_i),
    .b_i (b_i),
    .result_o (result_add)
);

vfpu_mul vfpu_mul_inst (
    .clk_i (clk_i),
    .a_i (a_i),
    .b_i (b_i),
    .result_o (result_mul)
);

vfpu_relu vfpu_relu_inst (
    .clk_i (clk_i),
    .a_i (a_i),
    .b_i (b_i),
    .result_o (result_single)
);

vfpu_maxmin # (
    .C_MAXMIN_LATENCY (C_MAXMIN_LATENCY)
)
vfpu_maxmin_inst (
    .clk_i (clk_i),
    .maxmin_op_i (maxmin_op),
    .in_i (a_i),
    .result_o (result_maxmin)
);
endmodule



module vfpu_add (
    input logic clk_i,
    input logic addsub_op_i,
    input t_v128 a_i,
                 b_i,
    output t_v128 result_o
);

genvar i;
for (i=0; i<8; i++) begin
    floating_point_0 fpu_addsub_inst (
        .aclk (clk_i),
        .s_axis_a_tvalid (1'b1),
        .s_axis_a_tdata (a_i.e[i]),
        .s_axis_b_tvalid (1'b1),
        .s_axis_b_tdata (b_i.e[i]),
        .s_axis_operation_tvalid (1'b1),
        .s_axis_operation_tdata ({7'b0, ~addsub_op_i}),
        .m_axis_result_tvalid (),
        .m_axis_result_tdata (result_o.e[i])
    );
end
endmodule



module vfpu_mul (
    input logic clk_i,
    input t_v128 a_i,
                 b_i,
    output t_v128 result_o
);

genvar i;
for (i=0; i<8; i++) begin
    floating_point_1 fpu_mul_inst (
        .aclk (clk_i),
        .s_axis_a_tvalid (1'b1),
        .s_axis_a_tdata (a_i.e[i]),
        .s_axis_b_tvalid (1'b1),
        .s_axis_b_tdata (b_i.e[i]),
        .m_axis_result_tvalid (),
        .m_axis_result_tdata (result_o.e[i])
    );
end
endmodule



module vfpu_relu (
    input logic clk_i,
    input t_v128 a_i,
                 b_i,
    output t_v128 result_o
);

// Leak ReLU block operates by observing the input value and 
// producing an output that depends on the sign of that input.
// If the sign is negative, the value will have to be scaled
// by a factor, 0 for standard ReLU, ~0 for leaky ReLU. This 
// scaling factor is then outputted to be used in a multiplication
// operation next.
// If the sign is positive, the value will not need to change
// or can be multiplied by 1, which is outputted in this case.
// This completes in a single cycle and therefore, a multiplication
// instruction should be called immediatly after the ReLU 
// instruction.

always_comb begin
    for (int i=0; i<8; i++) begin
        if (a_i.e[i][15]) begin // If number is negative
            result_o.e[i] <= b_i.e[i];
        end
        else begin
            result_o.e[i] <= 16'h3F80; // 1.0 in bfloat16
        end
    end
end
endmodule



module vfpu_maxmin # (
    parameter C_MAXMIN_LATENCY = 0
)
(
    input logic clk_i,
    input t_maxmin_op maxmin_op_i,
    input t_v128 in_i,
    output logic[15:0] result_o
);

t_maxmin_op maxmin_op_int[2];
logic[15:0] result_int0[4],
            result_int1[4],
            result_int2[2],
            result_int3[2],
            result_int4,
            result_int5;

always_ff @ (posedge clk_i) begin
    result_int1 = result_int0;
    result_int3 = result_int2;
end

genvar i;
for (i=0; i<8; i+=2) begin
    fpu_maxmin fpu_maxmin_inst0 (
        .maxmin_op_i (maxmin_op_i),
        .a_i (in_i.e[i]),
        .b_i (in_i.e[i+1]),
        .result_o (result_int0[i/2])
    );
end

delay_t_maxmin_op # (
    .C_DELAY (C_MAXMIN_LATENCY+1)
)
maxmin_op_ff_inst0 (
    .clk_i (clk_i),
    .in_i (maxmin_op_i),
    .out_o (maxmin_op_int[0])
);

genvar j;
for (j=0; j<4; j+=2) begin
    fpu_maxmin fpu_maxmin_inst1 (
        .maxmin_op_i (maxmin_op_int[0]),
        .a_i (result_int1[j]),
        .b_i (result_int1[j+1]),
        .result_o (result_int2[j/2])
    );
end

delay_t_maxmin_op # (
    .C_DELAY (C_MAXMIN_LATENCY+1)
)
maxmin_op_ff_inst1 (
    .clk_i (clk_i),
    .in_i (maxmin_op_int[0]),
    .out_o (maxmin_op_int[1])
);

fpu_maxmin fpu_maxmin_inst2 (
    .maxmin_op_i (maxmin_op_int[1]),
    .a_i (result_int3[0]),
    .b_i (result_int3[1]),
    .result_o (result_o)
);
endmodule



module fpu_maxmin (
    input t_maxmin_op maxmin_op_i,
    input logic[15:0] a_i,
                      b_i,
    output logic[15:0] result_o
);

// comp_int = 1 if a_i < b_i, otherwise 0
logic[7:0] comp_int;

always_comb begin
    if (comp_int[0] == 1'b1) begin // a_i < b_i
        if (maxmin_op_i == OP_MAX)
            result_o <= b_i;
        else // OP_MIN
            result_o <= a_i;
    end
    else begin // a_i >= b_i
        if (maxmin_op_i == OP_MAX)
            result_o <= a_i;
        else // OP_MIN
            result_o <= b_i;
    end
end

floating_point_2 sfpu_lt_inst (
    .s_axis_a_tvalid (1'b1),
    .s_axis_a_tdata (a_i),
    .s_axis_b_tvalid (1'b1),
    .s_axis_b_tdata (b_i),
    .m_axis_result_tvalid (),
    .m_axis_result_tdata (comp_int)
);
endmodule


module delay_t_maxmin_op # (
    parameter C_BYPASS = 0,
    parameter C_DELAY = 1
)
(
    input logic clk_i,
    input t_maxmin_op in_i,
    output t_maxmin_op out_o
);

logic in_int,
      out_int;

assign in_int = logic'(in_i);
assign out_o = t_maxmin_op'(out_int);

flip_flop # (
    .C_BYPASS (C_BYPASS),
    .C_WIDTH (1),
    .C_DELAY (C_DELAY)
)
maxmin_op_ff_inst1 (
    .clk_i (clk_i),
    .in_i (in_int),
    .out_o (out_int)
);
endmodule