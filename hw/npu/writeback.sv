import olympus_pkg::*;

module writeback (
    input logic clk_i,
    
    // *** DESTINATION ADDRESSES *** //
    input logic[6:0] dst_addr_vfpu_i,
    input logic[6:0] dst_addr_valu_i,
    input logic[6:0] dst_addr_sfpu_i,
    input logic[6:0] dst_addr_salu_i,
        
    // *** RESULTS *** //
    input t_v128 result_vector_vfpu_i,
    input logic[15:0] result_scalar_vfpu_i,
    input t_v128 result_vector_valu_i,
    input logic[15:0] result_scalar_valu_i,
    input logic[15:0] result_sfpu_i,
    input logic[15:0] result_salu_i,
    
    // *** RESULT VALID *** //
    input t_vector_result_valid valid_result_vfpu_i,
    
    // *** REGISTER WRITE INTERFACES *** //
    output logic vreg_we_o,
    output logic[6:0] vreg_addr_o,
    output t_v128 vreg_data_o,
    
    output logic sreg_we_o,
    output logic[6:0] sreg_addr_o,
    output logic[15:0] sreg_data_o,
    
    output logic TEST_ERROR_O
);

logic vfpu_valid,
      valu_valid,
      sfpu_valid,
      salu_valid;

assign vfpu_valid = |dst_addr_vfpu_i;
assign valu_valid = 0;
assign sfpu_valid = |dst_addr_sfpu_i;
assign salu_valid = |dst_addr_salu_i;

always_comb begin
    vreg_we_o <= 1'b0;
    sreg_we_o <= 1'b0;
    vreg_addr_o <= '0;
    sreg_addr_o <= '0;
    TEST_ERROR_O <= 1'b0;
    
    unique casex ({vfpu_valid, valu_valid, sfpu_valid, salu_valid, logic'(valid_result_vfpu_i)})
        5'b10000 : begin // VFPU vector result
            vreg_we_o <= 1'b1;
            vreg_addr_o <= dst_addr_vfpu_i;
            vreg_data_o <= {result_vector_vfpu_i};
        end
        5'b10001 : begin // VFPU scalar result
            sreg_we_o <= 1'b1;
            sreg_addr_o <= dst_addr_vfpu_i;
            sreg_data_o <= result_scalar_vfpu_i;
        end
        5'b0100X : begin // VALU result
            vreg_we_o <= 1'b1;
            vreg_addr_o <= dst_addr_valu_i;
            vreg_data_o <= {result_vector_valu_i};
        end
        5'b0010X : begin // SFPU result
            sreg_we_o <= 1'b1;
            sreg_addr_o <= dst_addr_sfpu_i;
            sreg_data_o <= result_sfpu_i;
        end
        5'b0001X : begin // SALU_result
            sreg_we_o <= 1'b1;
            sreg_addr_o <= dst_addr_salu_i;
            sreg_data_o <= result_salu_i;
        end
        default : TEST_ERROR_O <= 1'b1;
    endcase
end
endmodule
