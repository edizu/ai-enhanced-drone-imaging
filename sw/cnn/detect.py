# Import utility functions and model definition
from cnn.utils import prep_image, load_classes, write_results, draw_detections
from cnn.model import Darknet
from Vision.VisionClass import *
from Servo.ServoClass import *
from gps.XYlatlong import *

# PyTorch Libraries
import torch
from torch.autograd import Variable

# For timing processing stages
import time

# For creating output directory
import os
import os.path as osp

# For image processing
import cv2
import pickle as pkl

# For data processing
import pandas as pd

def init_net(cfgfile, weights, reso, hw):
    # Instantiate the model and load in the weights
    model = Darknet(cfgfile, hw)
    model.load_weights(weights)

    # Alternatively
    #model = Darknet(cfgfile)
    #model.load_state_dict(torch.load('weights/yolo_pytorch_weights.pth'))

    # Alternatively
    #model = torch.load('weights/yolo_pytorch_model.pth')

    # Save the input dimensions to the model
    model.net_info["height"] = reso

    return model


def ServoSetup(ObjectTrack):
    #Stop Timer counter
    ObjectTrack.Stop_counter()
    #Reset Timer Register Values
    ObjectTrack.Reset_Register_values()
    #Initialise Timer 
    ObjectTrack.TTC_init()
    ObjectTrack.TTC_init2()
    #Set Interrupts
    ObjectTrack.GIC_set()
    ObjectTrack.Interrupt_enable1()
    #Set Output Pin 
    ObjectTrack.Set_MIO_PIN()
    #Start Timer
    ObjectTrack.Start_timer()

def VisionSetup(Webcam):
    #Allocate Memory
    Webcam.Allocate_memory()
    #Config Resizer
    Webcam.resizer_Config()
    #Write Frame To input buffer
    Webcam.Write_frame_memory()

def Shutdown_system(Webcam,ObjectTrack):
    #Set Event to end Thread
    Webcam.event.set()
    #Release Webcam and Display monitor
    Webcam.Vision_devices_Release()
    ObjectTrack.Stop_counter()
    
    

def det_loop(model, images, det, confidence, nms_thresh,Webcam,Vis_sys,ObjectTrack):
    # Load in model parameters
    batch_size = 1

    # Put the model into evalutation mode for inference
    model.eval()

    # Ensure the input dimensions are a multiple of 32 greater than 32
    inp_dim = int(model.net_info["height"])
    assert inp_dim % 32 == 0 
    assert inp_dim > 32
    
 
    # Save current time of accessing images for timing analysis
    read_dir = time.time()
    try:
        # Load in the images in the images directory
        imlist = [osp.join(osp.realpath('.'), images, img) for img in os.listdir(images)]
    except NotADirectoryError:
        # Load in images in current folder if current directory specified
        imlist = []
        imlist.append(osp.join(osp.realpath('.'), images))
    except FileNotFoundError:
        # Print error if file or directory is not found
        print ("Error:\nCould not find specified location: {}".format(images))

    # Check if directory to hold detections exists
    if not os.path.exists(det):
        # If not then make one
        os.makedirs(det)
        
    # Save current time of loading images for timing analysis
    load_batch = time.time()
    
    if Vis_sys == True:
        # Webcam System is being called
        # Call the Webcam frame function 
        Webcam.Write_frame_memory()
        #prepare the caputure image from the Webcam
        im_batches = prep_image(Webcam.in_buffer, inp_dim)
        im_dim_list = [(640,480)]
        im_dim_list = torch.FloatTensor(im_dim_list).repeat(1,2)
        im_batches = im_batches.unsqueeze(1)
        #Call Resizer to resize image for the Output
        Webcam.run_resizer()
    else:
        #indent here
        # Use CV library to load in images
        loaded_ims = [cv2.imread(x) for x in imlist]

        # Prepare the loaded images and save them to a list
        im_batches = [prep_image(img, inp_dim) for img in loaded_ims]
        # Rewritten above using list comprehension
        #im_batches = list(map(prep_image, loaded_ims, [inp_dim for x in range(len(imlist))]))

        # Save the dimensions of the original images
        im_dim_list = [(x.shape[1], x.shape[0]) for x in loaded_ims]
        im_dim_list = torch.FloatTensor(im_dim_list).repeat(1,2)

    # Load in the classes
    num_classes = 80
    classes = load_classes("data/coco.names")

    # Flag for whether output has been intialised yet
    write = 0

    # Save current time of starting detections for timing analysis
    start_det_loop = time.time()
    for i, batch in enumerate(im_batches):
        # Save initial time for each batch
        start = time.time()
        # Perform prediction on current batch
        with torch.no_grad():
            prediction = model(Variable(batch))

        # Use the predictions to find the true detections in the image
        prediction = write_results(prediction, confidence, num_classes, nms_conf = nms_thresh)

        # Save end time for current batch
        end = time.time()

        # If no detections are made
        if Vis_sys == False:
            if type(prediction) == int:
                # Iterate through images and display prediction times
                for im_num, image in enumerate(imlist[i*batch_size: min((i+1)*batch_size, len(imlist))]):
                    im_id = i*batch_size + im_num
                    print("{0:20s} predicted in {1:6.3f} seconds".format(image.split("/")[-1], (end - start)/batch_size))
                    print("{0:20s} {1:s}".format("Objects Detected:", ""))
                    print("----------------------------------------------------------")
                # Skip the rest of the loop
                continue
        
                
        # Change from batch index to imlist index
        prediction[:,0] += i*batch_size

        # Check flag to see if output has been initialised
        if not write:
            output = prediction  
            write = 1
        else:
            # If it has then concatenate results to output
            output = torch.cat((output,prediction))
        if Vis_sys == False:
            # Iterate through reamining images and display predictions and related times
            for im_num, image in enumerate(imlist[i*batch_size: min((i+1)*batch_size, len(imlist))]):
                im_id = i*batch_size + im_num
                objs = [classes[int(x[-1])] for x in output if int(x[0]) == im_id]
                print("{0:20s} predicted in {1:6.3f} seconds".format(image.split("/")[-1], (end - start)/batch_size))
                print("{0:20s} {1:s}".format("Objects Detected:", " ".join(objs)))
                print("----------------------------------------------------------") 
        else:
            im_num = 0
            im_id = i*batch_size + im_num
            objs = [classes[int(x[-1])] for x in output if int(x[0]) == im_id]
            #print("{0:20s} predicted in {1:6.3f} seconds".format(image.split("/")[-1], (end - start)/batch_size))
            print("{0:20s} {1:s}".format("Objects Detected:", " ".join(objs)))
            print("----------------------------------------------------------") 
            

    # If output still is not initialised then no detections were made
    try:
        output
    except NameError:
        print ("No detections were made")

    ObjectTrack.Start_timer()

    # Get the original image dimensions
    im_dim_list = torch.index_select(im_dim_list, 0, output[:,0].long())
    # Get the scaling factor introduced by letterbox function
    scaling_factor = torch.min(float(model.net_info["height"])/im_dim_list,1)[0].view(-1,1)
    #if Vis_sys == True:
    # output[:,[1,3]] -= (inp_dim - torch.min(float(640)/im_dim_list,1)[0].view(-1,1)*im_dim_list[:,0].view(-1,1))/2
    # output[:,[2,4]] -= (inp_dim - torch.min(float(480)/im_dim_list,1)[0].view(-1,1)*im_dim_list[:,1].view(-1,1))/2
    
    #else:
    # Scale bounding boxes to letterbox image size
    output[:,[1,3]] -= (inp_dim - scaling_factor*im_dim_list[:,0].view(-1,1))/2
    output[:,[2,4]] -= (inp_dim - scaling_factor*im_dim_list[:,1].view(-1,1))/2
    # Scale bounding boxes to original image size
    output[:,1:5] /= scaling_factor

    # Clip any bounding boxes which lie outwith the original image
    for i in range(output.shape[0]):
        output[i, [1,3]] = torch.clamp(output[i, [1,3]], 0.0, im_dim_list[i,0])
        output[i, [2,4]] = torch.clamp(output[i, [2,4]], 0.0, im_dim_list[i,1])

    # Save current time of resizing bounding boxes for timing analysis
    output_recast = time.time()

    # Save current time of loading classes for timing analysis
    class_load = time.time()
    # Load in pickled file so bounding boxes can be drawn in different colours
    pallete = 'data/pallete'
    colors = pkl.load(open(pallete, "rb"))

    # Save current time of drawing results for timing analysis
    draw = time.time()
    
    if Vis_sys == True:
        loaded_ims = [Webcam.out_buffer]
        
    # Draw the bounding boxes on each image
    list(map(lambda x: draw_detections(x, loaded_ims, classes, colors), output))
    
    Webcam.Display_buffer = loaded_ims[0]
    Webcam.Display_Video()

    # Make a pandas series for the detected images
    det_names = pd.Series(imlist).apply(lambda x: "{}/det_{}".format(det,x.split("/")[-1]))

    # Save the images to the detection folder
    list(map(cv2.imwrite, det_names, loaded_ims))

    # Save the ending time for timing analysis
    end = time.time()
    
    # Object Tracking Code Implemented
    
    ObjectTrack.Xcord1 = output[0,1]
    ObjectTrack.Xcord2 = output[0,3]
    ObjectTrack.Camera_POV()
    
    GPS = positionToLatLon(output)
    
    print("The Latitude is", GPS[0])
    print("The Longitude is", GPS[1])
    
    # Show timing summary
    #print("SUMMARY")
    #print("----------------------------------------------------------")
    #print("{:25s}: {}".format("Task", "Time Taken (in seconds)"))
    #print()
    #print("{:25s}: {:2.3f}".format("Reading addresses", load_batch - read_dir))
    #print("{:25s}: {:2.3f}".format("Loading batch", start_det_loop - load_batch))
    #print("{:25s}: {:2.3f}".format("Detection (" + str(len(imlist)) +  " images)", output_recast - start_det_loop))
    #print("{:25s}: {:2.3f}".format("Output Processing", class_load - output_recast))
    #print("{:25s}: {:2.3f}".format("Drawing Boxes", end - draw))
    #print("{:25s}: {:2.3f}".format("Average time_per_img", (end - load_batch)/len(imlist)))
    #print("----------------------------------------------------------")