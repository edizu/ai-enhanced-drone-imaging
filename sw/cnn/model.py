# Import utility functions and model definition
from cnn.utils import predict_transform

# PyTorch Libraries
import torch 
import torch.nn as nn
import torch.nn.functional as F

# For data processing
import math
import numpy as np

# For timing processing stages
import time

# PYNQ libraries
from pynq import Overlay
import pynq.lib.dma
from pynq import allocate
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
try:
    from skimage.util.shape import view_as_windows
except ImportError:
    from skimage.util.shape import view_as_windows

# download bitstream
ol = "hw/mm_systolic_new_2SA_1.bit"
overlay = Overlay(ol)

# Instances of the DMA library for each of the 3 DMAs in the hardware design
dma_in = overlay.dma_input_fm
dma_weights = overlay.dma_weights
dma_out = overlay.dma_output_fm

# Defined by the HLS design macros - matrix tiles being transferred to H/W must not exceed these sizes
Tr_max = 262144
Tc_max = 32
Telem_max = 4608



def parse_cfg(cfgfile):
    """
    Returns a list of dictionaries describing the network defined in the config file.
    
    
    """
    # Open the config file
    file = open(cfgfile, 'r')
    # Split into lines and store in list
    lines = file.read().split('\n') 
    # Remove empty lines from the list
    lines = [x for x in lines if len(x) > 0]
    # Remove comment lines 
    lines = [x for x in lines if x[0] != '#']
    # Strip initial and trailing whitespace
    lines = [x.rstrip().lstrip() for x in lines]
    
    # Generate list of dictionaries to hold layer parameters
    block = {}
    blocks = []
    
    for line in lines:
        # If new block
        if line[0] == "[":
            # If block is not empty then it must contain values from  previous block.
            if len(block) != 0:     
                # Add the block to the list
                blocks.append(block)
                # Initialise fresh block
                block = {}
            # Read in the type of the block
            block["type"] = line[1:-1].rstrip()     
        else:
            # Read in the block parameters
            key,value = line.split("=") 
            block[key.rstrip()] = value.lstrip()
    # Add final block to list
    blocks.append(block)
    
    # Return the list of dictionaries describing the blocks
    return blocks



# Empty layer used for shortcut/route layers
class EmptyLayer(nn.Module):
    def __init__(self):
        super(EmptyLayer, self).__init__()



# Layer used for YOLO layers to perform detection
class DetectionLayer(nn.Module):
    def __init__(self, anchors):
        super(DetectionLayer, self).__init__()
        self.anchors = anchors



# create a new convolution implementation which inherits from nn.Conv2d
class Conv2d_hw(nn.Conv2d):
    
    # "forward" tiles the input feature map and weights matrices,
    # creates the PYNQ buffers and sends data to AXI4-Stream using DMA library
    def forward(self,input_fm_tensor):      
        Hk_l = self.kernel_size[0]
        Wk_l = self.kernel_size[1]
        D_l = self.in_channels
        D_l_next = self.out_channels
        
        # size before padding
        N = input_fm_tensor.size(0) # number of feature maps in input batch
        H_l = input_fm_tensor.size(2) # spatial height of input fm
        W_l = input_fm_tensor.size(3) # spatial width of input fm
        
        # only supports uniform padding around the feature map
        padding_width = self.padding[0]
        eroded_pixels = math.floor(Hk_l/2) - padding_width
        
        # get tuple corresponding to required stride in convolution - the '1' is so we don't stride the input channels
        stride = self.stride[0]
        stride_tuple = (1,stride,stride)
        
        start_time = time.time()
        
        # zero pad the input FM tensor
        input_fm_tensor = F.pad(input_fm_tensor,pad=(padding_width,padding_width,padding_width,padding_width))
        
        # convert bfloat16 input feature map tensor into single-precision floating point matrix
        #### start_time = time.time()
        input_fm_tensor_float = input_fm_tensor.float()
#        print("input FM initial operations: ", time.time()-start_time, " seconds")
        
        # BFLOAT16===
        output_fm_tensor = torch.zeros((N,D_l_next,math.ceil((H_l-2*eroded_pixels)/stride),math.ceil((W_l-2*eroded_pixels)/stride)),dtype=torch.bfloat16)
        
        # get weights matrix from tensor with reshape
        start_time = time.time()
        # new bfloat16 fast
        weights_matrix = np.transpose(np.reshape(self.weight.detach().float().numpy().view(dtype=np.uint16)[:,:,:,1::2],(D_l_next,-1)))
#        print("weights bfloat16 to uint16 and transpose: ", time.time()-start_time, " seconds")
        
        # loop over different 3D feature maps in the input batch
        for fm_in_batch in range(N):
        
            # gets the input fm matrix from tensor (fast memory strided im2row) - zero padding applied
            # get bfloat16 input fm tensor (as numpy uint16 array)
            
            start_time = time.time()
            
            # the array is copied so that 'view_as_windows' can access contiguous memory - not sure if this is better...
            input_fm_tensor_bfloat16 = np.zeros(shape=input_fm_tensor_float[fm_in_batch,:,:,:].size(),dtype=np.uint16)
            np.copyto(input_fm_tensor_bfloat16,input_fm_tensor_float[fm_in_batch,:,:,:].numpy().view(dtype=np.uint16)[:,:,1::2])
            
            # view_as_windows cannot access contiguous memory:
            #input_fm_tensor_bfloat16 = input_fm_tensor_float[fm_in_batch,:,:,:].numpy().view(dtype=np.uint16)[:,:,1::2]
            
            # new bfloat16 fast
            input_fm_matrix = np.reshape(np.transpose(view_as_windows(input_fm_tensor_bfloat16,(D_l,Hk_l,Wk_l),stride_tuple),(0,2,1,3,4,5)),(math.ceil((H_l-2*eroded_pixels)/stride)*math.ceil((W_l-2*eroded_pixels)/stride),-1))
            
#            print("im2row latency: ", time.time()-start_time, " seconds")

            # RECORD TIME
            start_time_driver = time.time()
            ############

            # get matrix dimensions
            n_rows = input_fm_matrix.shape[0]

            # check if columns of first matrix are equal to rows of second (i.e. the matrix multiplication is valid)
            if(input_fm_matrix.shape[1] != weights_matrix.shape[0]):
                print("ERROR: matrix inner dimensions do not match")
            else:
                n_elems = input_fm_matrix.shape[1]

            n_cols = weights_matrix.shape[1]
            
            # new bfloat16 fast
            output_fm_matrix = torch.zeros((n_rows,n_cols), dtype=torch.bfloat16)

            # compute required number of tile iterations
            c_itr = math.ceil(n_cols / Tc_max)

            # rows and columns remaining if tiles of size Tr_max, Tc_max, Telem_max are used
            remain_col = n_cols%Tc_max

            ## loop tiling for matrix multiply:

            # c_itr tiles of size c_tile_size or (c_itr - 1) tiles of size c_tile_size
            # and one tile of size remain_col
            for c_tile in range(c_itr):
                c_start_idx = c_tile*Tc_max
                c_end_idx = c_tile*Tc_max+Tc_max
                if((c_tile == (c_itr-1)) and (remain_col > 0)):
                    # last loop - fewer columns than c_tile_size
                    c_end_idx = c_tile*Tc_max+remain_col

                r_tile_size = n_rows
                c_tile_size = c_end_idx - c_start_idx
                elem_tile_size = n_elems

                # define PYNQ buffers using "Allocate" library  
                matrix_dim_buffer = allocate(shape=(8,1), dtype=np.uint16)
    
                # these values are the number of columns of the input FM/weights or output FM matrices rounded up to the nearest integer multiple of 8
                if(elem_tile_size%8 != 0):
                    elem_columns_send = elem_tile_size + 8 - elem_tile_size%8
                else:
                    elem_columns_send = elem_tile_size
                            
                if(c_tile_size%8 != 0):
                    col_columns_send = c_tile_size + 8 - c_tile_size%8
                else:
                    col_columns_send = c_tile_size
                            
                cache_in_buffer = allocate(shape=(r_tile_size,elem_columns_send), dtype=np.uint16)
                cache_weights_buffer = allocate(shape=(elem_tile_size,col_columns_send), dtype=np.uint16)
                cache_out_buffer = allocate(shape=(r_tile_size,col_columns_send), dtype=np.uint16)

                ## copy data to PYNQ buffers
                # copy matrix dimensions
                tr_buffer = np.zeros(shape=(1,),dtype=np.uint32)
                tr_buffer[0] = int(r_tile_size)
                tr_buffer = tr_buffer.view(dtype=np.uint16)
                matrix_dim_buffer[0:2,0] = tr_buffer
                
                # temporary work around
                if(c_tile_size % 8 != 0):
                    matrix_dim_buffer[2] = int(c_tile_size + 8 - c_tile_size % 8)
                else:
                    matrix_dim_buffer[2] = int(c_tile_size)

                ######################
                #matrix_dim_buffer[2] = int(c_tile_size)
                
                matrix_dim_buffer[3] = int(elem_tile_size)
                        
                # append zeros to input fm matrix and weights matrix before copying to PYNQ buffers
                input_fm_matrix_tile_appended = np.zeros(shape=(r_tile_size,elem_columns_send),dtype=np.uint16)
                weight_matrix_tile_appended = np.zeros(shape=(elem_tile_size,col_columns_send),dtype=np.uint16)
                
                input_fm_matrix_tile_appended[:,0:elem_tile_size] = input_fm_matrix
                weight_matrix_tile_appended[:,0:c_tile_size] = weights_matrix[:,c_start_idx:c_end_idx]

                # in MPSoC, we cannot copy data to PYNQ buffers unless the data is an integer multiple of 64 bits
                # this is accounted for in the fact that we are transferring integer multiples of 8 columns (16 bit values)
                # copy input feature map matrix
                np.copyto(cache_in_buffer,input_fm_matrix_tile_appended)

                # copy weights matrix
                np.copyto(cache_weights_buffer,weight_matrix_tile_appended)
                        
                start_time = time.time()

                # initiate AXI4-Stream using DMA library
                dma_in.sendchannel.transfer(matrix_dim_buffer)
                dma_in.sendchannel.wait()
                dma_weights.sendchannel.transfer(cache_weights_buffer)
                dma_in.sendchannel.transfer(cache_in_buffer)
                dma_out.recvchannel.transfer(cache_out_buffer)

                dma_weights.sendchannel.wait()
                dma_in.sendchannel.wait()
                dma_out.recvchannel.wait()
                        
#                        print("hardware only time (DMA transfers): ",time.time() - start_time, " seconds")

                # copy AXI4-Stream output to output matrix and add to previous values (to account for n_elements tiling)   
                output_fm_matrix_temp = np.zeros(shape=(r_tile_size,col_columns_send), dtype=np.uint16)
                        
                np.copyto(output_fm_matrix_temp, cache_out_buffer)
                        
                start_time = time.time()
                                                
                # new bfloat16 fast
                output_fm_matrix_temp_interp = np.zeros(shape=(r_tile_size*c_tile_size*2,),dtype=np.uint16)
                # output_fm_matrix_temp_interp[1::2] = output_fm_matrix_temp
                output_fm_matrix_temp_interp[1::2] = np.reshape(output_fm_matrix_temp[:,0:c_tile_size],(r_tile_size*c_tile_size))
                        
#                        print("output FM matrix convert from uint16 to bfloat16: ",time.time() - start_time, " seconds")
                        
                start_time = time.time()

                # set output FM matrix tile
                output_fm_matrix[:,c_start_idx:c_end_idx] = torch.tensor(output_fm_matrix_temp_interp.view(dtype=np.single).reshape(r_tile_size,c_tile_size)).type(torch.bfloat16)
                        
#                        print("partial matrix multiply additions latency: ",time.time() - start_time, " seconds")
                        
                # delete PYNQ buffers
                matrix_dim_buffer.close()
                cache_in_buffer.close()
                cache_weights_buffer.close()
                cache_out_buffer.close()

            # RECORD TIME
#            print("latency w/o im2row and bfloat16 conversions: ",time.time() - start_time_driver, " seconds")
            ###########

            start_time = time.time()    
            # new bfloat16 fast
            output_fm_tensor[fm_in_batch,:,:,:] = torch.transpose(output_fm_matrix.view(math.ceil((W_l-2*eroded_pixels)/stride),math.ceil((H_l-2*eroded_pixels)/stride),D_l_next),0,2)

            # add the bias term
            if(self.bias != None):
                output_fm_tensor[fm_in_batch,:,:,:] = torch.add(output_fm_tensor[fm_in_batch,:,:,:], self.bias.view(D_l_next,1,1))
                     
#            print("output FM conversion to tensor and bias adding: ",time.time() - start_time, " seconds")
                    
        return output_fm_tensor
 


def create_modules(blocks, hw):
    """
    Generates a PyTorch model from input block list using hardware acceleration if true, returning network information and a list of modules.
    
    
    """
    # First block contains network information
    net_info = blocks[0]
    # Create modulelist to hold layers
    module_list = nn.ModuleList()
    # Initially set to 3 for RGB channels
    prev_filters = 3
    # List to store size of output filters
    output_filters = []
    
    for index, x in enumerate(blocks[1:]):
        # For executing multiple layers in a single block
        module = nn.Sequential()
        
        # Check what type of block it is
        if (x["type"] == "convolutional"):
            # Read in information about the block
            activation = x["activation"]
            try:
                batch_normalize = int(x["batch_normalize"])
                bias = False
            except:
                batch_normalize = 0
                bias = True
        
            filters= int(x["filters"])
            padding = int(x["pad"])
            kernel_size = int(x["size"])
            stride = int(x["stride"])
        
            if padding:
                pad = (kernel_size - 1) // 2
            else:
                pad = 0
        
            # Add convolutional layer to the sequential
            if(hw == False):
                conv = nn.Conv2d(prev_filters, filters, kernel_size, stride, pad, bias = bias)
            elif(hw == True):
                conv = Conv2d_hw(prev_filters, filters, [kernel_size, kernel_size], (stride, stride), padding=(pad, pad), bias=bias, padding_mode = 'zeros')

            module.add_module("conv_{0}".format(index), conv)
        
            # Add batch normalise layer to the sequential
            if batch_normalize:
                bn = nn.BatchNorm2d(filters)
                module.add_module("batch_norm_{0}".format(index), bn)
        
            # Check activation function (YOLO uses only leaky ReLU or linear)
            if activation == "leaky":
                activn = nn.LeakyReLU(0.1, inplace = True)
                module.add_module("leaky_{0}".format(index), activn)
        
        
        elif (x["type"] == "upsample"):
            stride = int(x["stride"])
            # Add upsample layer to the sequential
            upsample = nn.Upsample(scale_factor = 2, mode = "nearest")
            module.add_module("upsample_{}".format(index), upsample)
                
                
        elif (x["type"] == "route"):
            x["layers"] = x["layers"].split(',')
            # Load in the start of the route
            start = int(x["layers"][0])
            # Check if a second value exists and save as end value
            try:
                end = int(x["layers"][1])
            except:
                end = 0
            
            # Gets route index in absolute terms (e.g. 5 instead of -1 from 6)
            if start > 0: 
                start = start - index
            if end > 0:
                end = end - index
            # Initialise route using EmptyLayer class
            route = EmptyLayer()
            # Add route layer to the sequential
            module.add_module("route_{0}".format(index), route)
            # Calculate total number of filters based on layer combinations
            if end < 0:
                filters = output_filters[index + start] + output_filters[index + end]
            else:
                filters= output_filters[index + start]
    
    
        elif x["type"] == "shortcut":
            # Shortcut layer indicates a skip connection
            # Initialise shortcut using EmptyLayer class
            shortcut = EmptyLayer()
            # Add shortcut layer to the sequential
            module.add_module("shortcut_{}".format(index), shortcut)
            

        elif x["type"] == "yolo":
            # YOLO layers perform final detections
            # Load in mask indexes as a list
            mask = x["mask"].split(",")
            mask = [int(x) for x in mask]
    
            # Take the relevant anchors using the mask indexes
            anchors = x["anchors"].split(",")
            anchors = [int(a) for a in anchors]
            anchors = [(anchors[i], anchors[i+1]) for i in range(0, len(anchors),2)]
            anchors = [anchors[i] for i in mask]
    
            # Initialise the YOLO layer as a DetectionLayers with the selected anchors
            detection = DetectionLayer(anchors)
            # Add the YOLO layer to the sequential
            module.add_module("Detection_{}".format(index), detection)
                              
        # Add sequential to the module list
        module_list.append(module)
        # Update the filters
        prev_filters = filters
        # Save output filter size to list for route layers
        output_filters.append(filters)
        
        #print(filters)
        
    return (net_info, module_list)



class Darknet(nn.Module):
    
    def __init__(self, cfgfile, hw):
        super(Darknet, self).__init__()
        # Parse the cfg file into blocks and extract net info and mod list
        self.blocks = parse_cfg(cfgfile)
        self.hw = hw
        self.net_info, self.module_list = create_modules(self.blocks, self.hw)
        

    def forward(self, x):
        # Skip first block since it only contains net info
        modules = self.blocks[1:]
        # List to store the outputs of each layer, used by route layer
        outputs = {}
        
        # Flag used to indicate whether to generate output or append to existing
        write = 0
        for i, module in enumerate(modules):
            # Extract the module type (conv, upsample, route, shortcut or YOLO)
            module_type = (module["type"])
            
            #print(i, module_type)
            #print('Input size:',x.size())
            layer_start = time.time()
            
            
            # If convolutional layer then need to past instance of convolutional hardware driver
            if module_type == "convolutional":
                if(self.hw == False):
                    # Perform software convolution, batch normalisation and Leaky ReLU
                    x = self.module_list[i](x)

                elif(self.hw == True):
                    #print('Kernel size:', model.module_list[0][0].kernel_size)
                    #print(self.module_list[i])
                    bfloat_start = time.time()
                    # Convert to bfloat
                    x = x.bfloat16()
                    bfloat_end = time.time()
                    #print("Bfloat time: ", bfloat_end - bfloat_start)
                    
                    conv_start = time.time()
                    # Perform hardware convolution
                    x = self.module_list[i][0](x)
                    conv_end = time.time()
                    #print("Conv time: ", conv_end - conv_start)
                    
                    float_start = time.time()
                    # Convert back to float
                    x = x.float()
                    float_end = time.time()
                    #print("Float time: ", float_end - float_start)
                    try:
                        # If bias exists then add bias
                        self.module_list[i][0].bias.size()
                        x = x + self.module_list[i][0].bias.value
                    except AttributeError:
                        pass
                    
                    try:
                        # Perform batch normalisation and Leaky ReLU
                        x = self.module_list[i][1](x)
                        x = self.module_list[i][2](x)
                    except IndexError:
                        pass
                
            # Since upsample layer defined previously can just use it to calculate output
            elif module_type == "upsample":
                x = self.module_list[i](x)
    
            # For route layers then must check what layers to concatenate
            elif module_type == "route":
                layers = module["layers"]
                layers = [int(a) for a in layers]

                if (layers[0]) > 0:
                    # Convert to negative indexing (-6 from 11 instead of 5)
                    layers[0] = layers[0] - i
    
                if len(layers) == 1:
                    # If only one layer given simply output the specified layer output
                    x = outputs[i + (layers[0])]
    
                else:
                    # Convert to negative indexing (-6 from 11 instead of 5)
                    if (layers[1]) > 0:
                        layers[1] = layers[1] - i
    
                    # Collect the two outputs and concatenate to get output
                    map1 = outputs[i + layers[0]]
                    map2 = outputs[i + layers[1]]

                    x = torch.cat((map1, map2), 1)
                
    
            elif  module_type == "shortcut":
                # For shortcut layers simply add the indexed layer output to the previous output
                from_ = int(module["from"])
                x = outputs[i-1] + outputs[i+from_]
    
            elif module_type == 'yolo':
                # Get the anchor boxes for the current detection layer
                anchors = self.module_list[i][0].anchors
                # Get the input dimensions
                inp_dim = int (self.net_info["height"])
                # Get the number of classes
                num_classes = int (module["classes"])
        
                # Get the tensor data from the Variable
                x = x.data
                # Converts the detection feature map into a 2D tensor where each row is a attribute of a bounding box
                x = predict_transform(x, inp_dim, anchors, num_classes)
                
                # Check flag to see if output has been initialised
                if not write:
                    # If not then initialise and update flag
                    detections = x
                    write = 1
                else:
                    # Otherwise simply append detections to the output
                    detections = torch.cat((detections, x), 1)
                    
            #print(x)
        
            # Save the output from each layer for use in route layers
            outputs[i] = x
            
            layer_end = time.time()
            #print("Layer time: ", layer_end - layer_start, "\n")
        
        return detections

    def load_weights(self, weightfile):
        # Open weight file for reading in binary
        fp = open(weightfile, "rb")
        
        # First 5 values are header information:
        # 1 - Major version number
        # 2 - Minor version number
        # 3 - Subversion number
        # 4 - Images seen during training
        # 5 - Images seen during training
        
        # All numbers are stored as 32 bit integers
        
        # Read the first five values from the file
        header = np.fromfile(fp, dtype = np.int32, count = 5)
        # Save header information to model
        self.header = torch.from_numpy(header)
        # Save the images seen by the network during training
        self.seen = self.header[3]   
        
        # The rest of the file contains the weights for each layer
        weights = np.fromfile(fp, dtype = np.float32)
        
        # Needed to keep track of position in file
        ptr = 0
        for i in range(len(self.module_list)):
            # Ignore first layer since it contains only net info
            module_type = self.blocks[i + 1]["type"]
            
            # Only convolutional layers contain weights
            if module_type == "convolutional":
                model = self.module_list[i]
                # Check if the layer has batch normalisation biases
                try:
                    batch_normalize = int(self.blocks[i+1]["batch_normalize"])
                except:
                    batch_normalize = 0
            
                # The convolutional layer is first in the sequential block
                conv = model[0]
                
                
                if (batch_normalize):
                    # The batch normalisation layer is second in the sequential block
                    bn = model[1]
        
                    # Find the number of biases in the batch normalisation layer
                    # This is equivalent to the number of filters
                    # The total biases for the layer will be 4 times the number of filters
                    num_bn_biases = bn.bias.numel()
        
                    # Load the biases and advance the file pointer
                    bn_biases = torch.from_numpy(weights[ptr:ptr + num_bn_biases])
                    ptr += num_bn_biases
        
                    bn_weights = torch.from_numpy(weights[ptr: ptr + num_bn_biases])
                    ptr  += num_bn_biases
        
                    bn_running_mean = torch.from_numpy(weights[ptr: ptr + num_bn_biases])
                    ptr  += num_bn_biases
        
                    bn_running_var = torch.from_numpy(weights[ptr: ptr + num_bn_biases])
                    ptr  += num_bn_biases
        
                    # Cast the biases into the dimensions of the layer
                    bn_biases = bn_biases.view_as(bn.bias.data)
                    bn_weights = bn_weights.view_as(bn.weight.data)
                    bn_running_mean = bn_running_mean.view_as(bn.running_mean)
                    bn_running_var = bn_running_var.view_as(bn.running_var)
        
                    # Save the biases into the model
                    bn.bias.data.copy_(bn_biases)
                    bn.weight.data.copy_(bn_weights)
                    bn.running_mean.copy_(bn_running_mean)
                    bn.running_var.copy_(bn_running_var)
                
                else:
                    # If no batch normalisation then there are only convolutional biases
                    num_biases = conv.bias.numel()
                
                    # Load the biases and advance the pointer
                    conv_biases = torch.from_numpy(weights[ptr: ptr + num_biases])
                    ptr = ptr + num_biases
                
                    # Cast the biases into the dimension of the layer
                    conv_biases = conv_biases.view_as(conv.bias.data)
                
                    # Save the biases to the model
                    conv.bias.data.copy_(conv_biases)
                    
                    
                # Check the number of weights for the convolutional layer
                num_weights = conv.weight.numel()
                
                # Load the weights 
                conv_weights = torch.from_numpy(weights[ptr:ptr+num_weights])
                ptr = ptr + num_weights
                
                # Cast the biases into the dimensions of the layer
                conv_weights = conv_weights.view_as(conv.weight.data)
                # Save the weights into the model
                conv.weight.data.copy_(conv_weights)