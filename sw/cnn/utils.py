# PyTorch Libraries
import torch 

# For image processing
import cv2

# For data processing
import numpy as np

# For randomising detection colours
import random

def unique(tensor):
    """
    Returns a tensor containing only the unique values.
    
    
    """
    # Converts tensor to numpy array, gets unique elements, then converts back
    tensor_np = tensor.cpu().numpy()
    unique_np = np.unique(tensor_np)
    unique_tensor = torch.from_numpy(unique_np)
    
    # Creates a copy of tensor to return
    tensor_res = tensor.new(unique_tensor.shape)
    tensor_res.copy_(unique_tensor)
    
    return tensor_res



def bbox_iou(box1, box2):
    """
    Returns the Intersection over Union of the two input bounding boxes.
    
    
    """
    # Get bounding box corner coordinates
    b1_x1, b1_y1, b1_x2, b1_y2 = box1[:,0], box1[:,1], box1[:,2], box1[:,3]
    b2_x1, b2_y1, b2_x2, b2_y2 = box2[:,0], box2[:,1], box2[:,2], box2[:,3]
    
    # Get the intersecting rectangle coordinates
    inter_rect_x1 =  torch.max(b1_x1, b2_x1)
    inter_rect_y1 =  torch.max(b1_y1, b2_y1)
    inter_rect_x2 =  torch.min(b1_x2, b2_x2)
    inter_rect_y2 =  torch.min(b1_y2, b2_y2)
    
    # Calculate area of intersection
    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * torch.clamp(inter_rect_y2 - inter_rect_y1 + 1, min=0)

    # Calculate area of union
    b1_area = (b1_x2 - b1_x1 + 1)*(b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1)*(b2_y2 - b2_y1 + 1)
    
    # Find Intersection over Union
    iou = inter_area / (b1_area + b2_area - inter_area)
    
    return iou



def predict_transform(prediction, inp_dim, anchors, num_classes):
    """
    Converts the detection feature map into a 2D tensor where each row is a attribute of a bounding box.
    
    
    """
    # Gather information according to config file
    batch_size = prediction.size(0)
    stride =  inp_dim // prediction.size(2)
    grid_size = inp_dim // stride
    bbox_attrs = 5 + num_classes
    num_anchors = len(anchors)
    
    # Perform the transformation from feature map to 2D tensor
    prediction = prediction.view(batch_size, bbox_attrs*num_anchors, grid_size*grid_size)
    prediction = prediction.transpose(1,2).contiguous()
    prediction = prediction.view(batch_size, grid_size*grid_size*num_anchors, bbox_attrs)
    anchors = [(a[0]/stride, a[1]/stride) for a in anchors]

    # Perform sigmoid activation on the centre X, centre Y and object confidence
    prediction[:,:,0] = torch.sigmoid(prediction[:,:,0])
    prediction[:,:,1] = torch.sigmoid(prediction[:,:,1])
    prediction[:,:,4] = torch.sigmoid(prediction[:,:,4])
    
    # Add offsets to the centre
    grid = np.arange(grid_size)
    a,b = np.meshgrid(grid, grid)

    x_offset = torch.FloatTensor(a).view(-1,1)
    y_offset = torch.FloatTensor(b).view(-1,1)
    #x_offset = torch.bfloat16Tensor(a).view(-1,1)
    #y_offset = torch.bfloat16Tensor(b).view(-1,1)

    x_y_offset = torch.cat((x_offset, y_offset), 1).repeat(1,num_anchors).view(-1,2).unsqueeze(0)

    prediction[:,:,:2] += x_y_offset

    # Apply log space transform for the height and width
    anchors = torch.FloatTensor(anchors)
    #anchors = torch.BFloat16Tensor(anchors)

    anchors = anchors.repeat(grid_size*grid_size, 1).unsqueeze(0)
    prediction[:,:,2:4] = torch.exp(prediction[:,:,2:4])*anchors
    
    prediction[:,:,5: 5 + num_classes] = torch.sigmoid((prediction[:,:, 5 : 5 + num_classes]))

    prediction[:,:,:4] *= stride
    
    return prediction



def write_results(prediction, confidence, num_classes, nms_conf = 0.4):
    """
    Takes the predictions and returns the true detections for each image.
    
    
    """
    # Set bounding boxes rows with objectness score below the confidence threshold to zero
    conf_mask = (prediction[:,:,4] > confidence).float().unsqueeze(2)
    #conf_mask = (prediction[:,:,4] > confidence).bfloat16().unsqueeze(2)
    prediction = prediction*conf_mask
    
    # Convert box centres, height and width to corner coordinates
    box_corner = prediction.new(prediction.shape)
    box_corner[:,:,0] = (prediction[:,:,0] - prediction[:,:,2]/2)
    box_corner[:,:,1] = (prediction[:,:,1] - prediction[:,:,3]/2)
    box_corner[:,:,2] = (prediction[:,:,0] + prediction[:,:,2]/2) 
    box_corner[:,:,3] = (prediction[:,:,1] + prediction[:,:,3]/2)
    prediction[:,:,:4] = box_corner[:,:,:4]
    
    batch_size = prediction.size(0)
    # Flag to show that the output has not been initialised yet
    write = False
    

    for ind in range(batch_size):
        image_pred = prediction[ind] # Image Tensor
    
        # Keep only the index of the maximum valued class
        max_conf, max_conf_score = torch.max(image_pred[:,5:5+ num_classes], 1)
        max_conf = max_conf.float().unsqueeze(1)
        max_conf_score = max_conf_score.float().unsqueeze(1)
        #max_conf = max_conf.bfloat16().unsqueeze(1)
        #max_conf_score = max_conf_score.bfloat16().unsqueeze(1)
        seq = (image_pred[:,:5], max_conf, max_conf_score)
        image_pred = torch.cat(seq, 1)
        
        # Remove bounding box rows that have been set to 0
        non_zero_ind =  (torch.nonzero(image_pred[:,4]))
        # In case there are 0 detections
        try:
            image_pred_ = image_pred[non_zero_ind.squeeze(),:].view(-1,7)
        except:
            continue
        
        # Added for PyTorch 0.4 compatibility
        if image_pred_.shape[0] == 0:
            continue      

        # Get the detected classes for the image
        img_classes = unique(image_pred_[:,-1])  # -1 index holds the class index
        
        
        for cls in img_classes:
            # Perform classwise Non-Maximum Suppression
        
            # Get the detections for the current class
            cls_mask = image_pred_*(image_pred_[:,-1] == cls).float().unsqueeze(1)
            #cls_mask = image_pred_*(image_pred_[:,-1] == cls).bfloat16().unsqueeze(1)
            class_mask_ind = torch.nonzero(cls_mask[:,-2]).squeeze()
            image_pred_class = image_pred_[class_mask_ind].view(-1,7)
            
            # Rearrange the detections from max objectiveness confidence down
            conf_sort_index = torch.sort(image_pred_class[:,4], descending = True )[1]
            image_pred_class = image_pred_class[conf_sort_index]
            idx = image_pred_class.size(0)   # Number of detections
            
            for i in range(idx):
                # Perform IoU for all subsequent boxes in the loop after the current one
                try:
                    # Bounding box row and all subsequent bounding boxes entered
                    # Returns a tensor containing IoU of the each combination of boxes
                    ious = bbox_iou(image_pred_class[i].unsqueeze(0), image_pred_class[i+1:])
                # Since bounding boxes are removed during loop then box[i] may not exist
                except ValueError:
                    break
                except IndexError:
                    break
            
                # Set all detections with IoU > threshold to zero
                iou_mask = (ious < nms_conf).float().unsqueeze(1)
                #iou_mask = (ious < nms_conf).bfloat16().unsqueeze(1)
                image_pred_class[i+1:] *= iou_mask       
            
                # Get rid of all non-zero entries
                non_zero_ind = torch.nonzero(image_pred_class[:,4]).squeeze()
                image_pred_class = image_pred_class[non_zero_ind].view(-1,7)
                
            # For each detection of the class cls in the image then repeat the batch_ind
            batch_ind = image_pred_class.new(image_pred_class.size(0), 1).fill_(ind)
            seq = batch_ind, image_pred_class
            
            # Check flag to create output or concatenate to existing output
            if not write:
                output = torch.cat(seq,1)
                write = True
            else:
                out = torch.cat(seq,1)
                output = torch.cat((output,out))

    # Check if there are any detections at all
    try:
        # Returns index of batch image, 4 corner coordinates, objectness score,
        # score of maxiumum confidence class, and the index of that class.
        return output
    except:
        return 0



def draw_detections(x, results, classes, colors):
    """
    Draws bounding boxes onto image.
    
    
    """
    # Get the two corner coordinates
    c1 = tuple(x[1:3].int())
    c2 = tuple(x[3:5].int())
    # Get the image
    img = results[int(x[0])]
    # Get the detected class
    cls = int(x[-1])
    # Randomly select a colour for each class
    color = random.choice(colors)
    # Create a label for the selected class
    label = "{0}".format(classes[cls])
    # Create a bounding box around the detected object
    cv2.rectangle(img, c1, c2, color, 1)
    # Get the text size for the label
    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1 , 1)[0]
    # Move the bottom right corner to the top left
    c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
    # Add a box for the label
    cv2.rectangle(img, c1, c2,color, -1)
    # Add text for the label
    cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1, [225,255,255], 1)
    
    # Return image with bounding box drawn on
    return img



def letterbox_image(img, inp_dim):
    '''
    Resize the image while retaining the aspect ration using padding.
    
    
    '''
    # Take the dimensions of the input image
    img_w, img_h = img.shape[1], img.shape[0]
    # Get input dimensions from config file
    w, h = inp_dim
    # Resize image while maintaining aspect radio
    new_w = int(img_w * min(w/img_w, h/img_h))
    new_h = int(img_h * min(w/img_w, h/img_h))
    resized_image = cv2.resize(img, (new_w,new_h), interpolation = cv2.INTER_CUBIC)
    
    # Pad blank space with (128, 128, 128)
    canvas = np.full((inp_dim[1], inp_dim[0], 3), 128)

    canvas[(h-new_h)//2:(h-new_h)//2 + new_h,(w-new_w)//2:(w-new_w)//2 + new_w,  :] = resized_image
    
    return canvas



def prep_image(img, inp_dim):
    """
    Prepare image for inputting to the neural network. 
    
 
    """
    # Resize image to size in cfg file
    img = (letterbox_image(img, (inp_dim, inp_dim)))
    # Change from BGR to RGB and from H x W x C to C x H x W
    img = img[:,:,::-1].transpose((2,0,1)).copy()
    # Convert numpy array to tensor
    img = torch.from_numpy(img).float().div(255.0).unsqueeze(0)
    #img = torch.from_numpy(img).bfloat16().div(255.0).unsqueeze(0)

    return img



def load_classes(namesfile):
    """
    Read in class labels from file.
    
    
    """
    fp = open(namesfile, "r")
    names = fp.read().split("\n")[:-1]

    return names