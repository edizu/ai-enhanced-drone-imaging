{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from geopy.geocoders import Nominatim\n",
    "import time\n",
    "from pprint import pprint"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# instantiate a new Nominatim client\n",
    "app = Nominatim(user_agent=\"tutorial\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'boundingbox': ['-1.444471', '-1.163332', '36.6509378', '37.1038871'],\n",
      " 'class': 'place',\n",
      " 'display_name': 'Nairobi, Kenya',\n",
      " 'icon': 'https://nominatim.openstreetmap.org/ui/mapicons//poi_place_city.p.20.png',\n",
      " 'importance': 0.845026759433763,\n",
      " 'lat': '-1.3031689499999999',\n",
      " 'licence': 'Data © OpenStreetMap contributors, ODbL 1.0. '\n",
      "            'https://osm.org/copyright',\n",
      " 'lon': '36.826061224105075',\n",
      " 'osm_id': 9185096,\n",
      " 'osm_type': 'relation',\n",
      " 'place_id': 297354849,\n",
      " 'type': 'city'}\n"
     ]
    }
   ],
   "source": [
    "# get location raw data\n",
    "location = app.geocode(\"Nairobi, Kenya\").raw\n",
    "# print raw data\n",
    "pprint(location)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'boundingbox': ['-1.444471', '-1.163332', '36.6509378', '37.1038871'],\n",
       " 'class': 'place',\n",
       " 'display_name': 'Nairobi, Kenya',\n",
       " 'icon': 'https://nominatim.openstreetmap.org/images/mapicons/poi_place_city.p.20.png',\n",
       " 'importance': 0.845026759433763,\n",
       " 'lat': '-1.2832533',\n",
       " 'licence': 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',\n",
       " 'lon': '36.8172449',\n",
       " 'osm_id': 9185096,\n",
       " 'osm_type': 'relation',\n",
       " 'place_id': 273942566,\n",
       " 'type': 'city'}"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "{'boundingbox': ['-1.444471', '-1.163332', '36.6509378', '37.1038871'],\n",
    " 'class': 'place',\n",
    " 'display_name': 'Nairobi, Kenya',\n",
    " 'icon': 'https://nominatim.openstreetmap.org/images/mapicons/poi_place_city.p.20.png',\n",
    " 'importance': 0.845026759433763,\n",
    " 'lat': '-1.2832533',\n",
    " 'licence': 'Data © OpenStreetMap contributors, ODbL 1.0. '\n",
    "            'https://osm.org/copyright',\n",
    " 'lon': '36.8172449',\n",
    " 'osm_id': 9185096,\n",
    " 'osm_type': 'relation',\n",
    " 'place_id': 273942566,\n",
    " 'type': 'city'}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_location_by_address(address):\n",
    "    \"\"\"This function returns a location as raw from an address\n",
    "    will repeat until success\"\"\"\n",
    "    time.sleep(1)\n",
    "    try:\n",
    "        return app.geocode(address).raw\n",
    "    except:\n",
    "        return get_location_by_address(address)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-6.7460426, 39.2750435\n",
      "{'boundingbox': ['-6.7467114', '-6.7454549', '39.2741685', '39.2760165'],\n",
      " 'class': 'highway',\n",
      " 'display_name': 'Makai Road, Msasani Peninsula, Masaki, Msasani, Dar es '\n",
      "                 'Salaam, Coastal Zone, 2585, Tanzania',\n",
      " 'importance': 0.82,\n",
      " 'lat': '-6.7460426',\n",
      " 'licence': 'Data © OpenStreetMap contributors, ODbL 1.0. '\n",
      "            'https://osm.org/copyright',\n",
      " 'lon': '39.2750435',\n",
      " 'osm_id': 23347726,\n",
      " 'osm_type': 'way',\n",
      " 'place_id': 93456914,\n",
      " 'type': 'residential'}\n"
     ]
    }
   ],
   "source": [
    "address = \"Makai Road, Masaki, Dar es Salaam, Tanzania\"\n",
    "location = get_location_by_address(address)\n",
    "latitude = location[\"lat\"]\n",
    "longitude = location[\"lon\"]\n",
    "print(f\"{latitude}, {longitude}\")\n",
    "# print all returned data\n",
    "pprint(location)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'boundingbox': ['-6.7467061', '-6.7454602', '39.2741806', '39.2760514'],\n",
       " 'class': 'highway',\n",
       " 'display_name': 'Makai Road, Masaki, Msasani, Dar es-Salaam, Dar es Salaam, Coastal Zone, 2585, Tanzania',\n",
       " 'importance': 0.82,\n",
       " 'lat': '-6.7460493',\n",
       " 'licence': 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',\n",
       " 'lon': '39.2750804',\n",
       " 'osm_id': 23347726,\n",
       " 'osm_type': 'way',\n",
       " 'place_id': 89652779,\n",
       " 'type': 'residential'}"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "-6.7460493, 39.2750804\n",
    "{'boundingbox': ['-6.7467061', '-6.7454602', '39.2741806', '39.2760514'],\n",
    " 'class': 'highway',\n",
    " 'display_name': 'Makai Road, Masaki, Msasani, Dar es-Salaam, Dar es Salaam, '\n",
    "                 'Coastal Zone, 2585, Tanzania',\n",
    " 'importance': 0.82,\n",
    " 'lat': '-6.7460493',\n",
    " 'licence': 'Data © OpenStreetMap contributors, ODbL 1.0. '\n",
    "            'https://osm.org/copyright',\n",
    " 'lon': '39.2750804',\n",
    " 'osm_id': 23347726,\n",
    " 'osm_type': 'way',\n",
    " 'place_id': 89652779,\n",
    " 'type': 'residential'}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
