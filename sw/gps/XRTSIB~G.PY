{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pprint\n",
    "import time\n",
    "import math\n",
    "\n",
    "output_nr = 1\n",
    "\n",
    "def positionToLatLon( positionx, positiony):\n",
    "    posx = positionx[0]\n",
    "    posy = positiony[0]\n",
    "    R = 6371 #Radius of the Earth\n",
    "    brng = (math.pi/2) #Bearing is 90 degrees converted to radians.\n",
    "    d = math.sqrt((posx*posx) + (posy*posy)) #Distance in km from the lat/long  #Pythagoras formula\n",
    "    lat1 = math.radians(40.477719)#reference lat point converted to radians\n",
    "    lon1 = math.radians(16.941589)#reference long point converted to radians\n",
    "    lat2 = math.asin(math.sin(lat1)*math.cos(d/R) + math.cos(lat1)*math.sin(d/R)*math.cos(brng))\n",
    "    lon2 = lon1 + math.atan2(math.sin(brng)*math.sin(d/R)*math.cos(lat1),\n",
    "    math.cos(d/R)-math.sin(lat1)*math.sin(lat2))\n",
    "    lat2 = math.degrees(lat2)\n",
    "    lon2 = math.degrees(lon2)\n",
    "    result = []\n",
    "    result.append(lat2)\n",
    "    result.append(lon2)\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
