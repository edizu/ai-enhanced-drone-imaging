import pprint
import time
import math

output_nr = 1

def positionToLatLon(output):
    posx = output[0,1]
    posy = output[0,2]
    R = 6371 #Radius of the Earth
    brng = (math.pi/2) #Bearing is 90 degrees converted to radians.
    d = math.sqrt((posx*posx) + (posy*posy)) #Distance in km from the lat/long  #Pythagoras formula
    lat1 = math.radians(55.826111)#reference lat point converted to radians
    lon1 = math.radians(-4.2423927)#reference long point converted to radians
    lat2 = math.asin(math.sin(lat1)*math.cos(d/R) + math.cos(lat1)*math.sin(d/R)*math.cos(brng))
    lon2 = lon1 + math.atan2(math.sin(brng)*math.sin(d/R)*math.cos(lat1),
    math.cos(d/R)-math.sin(lat1)*math.sin(lat2))
    lat2 = math.degrees(lat2)
    lon2 = math.degrees(lon2)
    result = []
    result.append(lat2)
    result.append(lon2)
    return result