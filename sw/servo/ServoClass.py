from pynq import Register
from pynq import GPIO
from pynq import Clocks

class ObjectTracking(object):
    #define all of the register addresss to use TTC0
    Clock_control_address = 0xFF110000
    Counter_control_address = 0xFF11000C
    Counter_value_address = 0xFF110018
    Match_counter_address = 0xFF110030
    Interval_counter_address = 0xFF110024
    Interrupt_register_address = 0xFF110054
    Interrupt_enable_address = 0xFF110060
    MIO_PIN_39_address = 0xFF18009C
    #Generic Interrupt trigger handler
    GICP1_IRQ_Trigger_address = 0xFF418024
    #Generic Interrupt Enable
    GICP1_IRQ_Enable_address = 0xFF41801C 
    #define the width of each TTC0 register
    Clock_control_width = 32
    Counter_control_width = 32
    Counter_value_width = 32
    Match_counter_width = 32
    Interval_counter_width = 32
    Interrupt_register_width = 32
    Interrupt_enable_width = 32
    MIO_PIN_39_width = 32
    GICP1_IRQ_Trigger_width = 32
    GICP1_IRQ_Enable_width = 32
    #Declare Reset Values for each Register object
    Counter_control_Reset_value = 0x00000021
    Clock_control_Reset_value = 0x00000000
    Interval_counter_Reset_value = 0x00000000
    Match_counter_Reset_value = 0x00000000
    Interrupt_enable_Reset_value = 0x00000000
    Interrupt_register_Reset_value = 0x00000000
   
    #initialise
    def __init__(self):
        # Set Resize Object
        self.Resize = 640
        self.Xcord1 = 310
        self.Xcord2 = 350
        #Create Variable to Stored the Current Posistion
        self.Current_pos = "Centre"
        #Create Variable to Store the current value of the camera
        self.Current_CP = 50
        #create clock control register object
        self.Clock_control = Register(self.Clock_control_address,self.Clock_control_width)
        #create Counter control Register object
        self.Counter_control = Register(self.Counter_control_address,self.Counter_control_width)
        #create Counter value register object
        self.Counter_value = Register(self.Counter_value_address,self.Counter_value_width)
        #create match counter register object
        self.Match_counter = Register(self.Match_counter_address,self.Match_counter_width)
        #create interval counter register object
        self.Interval_counter = Register(self.Interval_counter_address,self.Interval_counter_width)
        #create interrupt register Object
        self.Interrupt_register = Register(self.Interrupt_register_address,self.Interrupt_register_width)
        #create interrupt enable register object
        self.Interrupt_enable = Register(self.Interrupt_enable_address,self.Interrupt_enable_width)
        #create MIO Pin register object
        self.MIO_PIN_39 = Register(self.MIO_PIN_39_address,self.MIO_PIN_39_width)
        #create general interrupt controller trigger register object
        self.GICP1_IRQ_Trigger = Register(self.GICP1_IRQ_Trigger_address,self.GICP1_IRQ_Trigger_width)
        #Create general interrupt controller enable register object
        self.GICP1_IRQ_Enable = Register(self.GICP1_IRQ_Enable_address,self.GICP1_IRQ_Enable_width)
        
    #define methods
    def Test_check(self):
        print("The clock control width is",self.Clock_control_width)
        print("The clock control width is",self.Resize)

    # Function to stop counter 
    def Stop_counter(self):
        self.Counter_control.__setitem__(0,1)
        #print("The Timer Enable is:",self.Counter_control.__getitem__(0))
    # Function carry out Reset of all Register objects
    def Reset_Register_values(self):
        #Reset the count variable by writing reset value to register
        self.Counter_control.__setitem__(slice(6,0),self.Counter_control_Reset_value)
        #Reset the clock control by writing reset value to the register
        self.Clock_control.__setitem__(slice(6,0),self.Clock_control_Reset_value)
        #Reset the interval counter by writing the reset value to register
        self.Interval_counter.__setitem__(slice(31,0),self.Interval_counter_Reset_value)
        #Reset the Match counter by wrting the reset value to register
        self.Match_counter.__setitem__(slice(31,0),self.Match_counter_Reset_value)
        #Reset the interrupt enable by writing the reset value to the register
        self.Interrupt_enable.__setitem__(slice(5,0),self.Interrupt_enable_Reset_value)
        #Reset the interrupt register by writing the reset value to register
        self.Interrupt_register.__setitem__(slice(5,0),self.Interrupt_register_Reset_value)
        # Reset Counter RST = 4, set high
        self.Counter_control.__setitem__(4,1)
        #print to ensure that timer is equal to zero and the timer is still stopped
        #print("The current counter value is:",self.Counter_value.__getitem__(slice(31,0)))
        #print("The Timer Enable is:",self.Counter_control.__getitem__(0))
    # Function to TTC Initialisation
    def TTC_init(self):
        #Enable timer to operate in INT mode, When set high timer is operating in Interval mode
        self.Counter_control.__setitem__(1,1)
        #Enable Match mode
        self.Counter_control.__setitem__(3,1)
        #Enable output Waveform wave_en - active low
        self.Counter_control.__setitem__(5,0)
        #Determine waveform polarity Wave_Pol
        self.Counter_control.__setitem__(6,0)
        #print out to ensure correct initialisation
        #print(" The Int value is: ",self.Counter_control.__getitem__(1))
        #print(" The Match value is: ",self.Counter_control.__getitem__(3))
        #print(" The wave_en value is: ",self.Counter_control.__getitem__(5))
        #print(" The waveform polarity value is: ",self.Counter_control.__getitem__(6))
    # Function for set max count, prescale and value for match intterupt
    def TTC_init2(self):
        #Enable Prescaler
        self.Clock_control.__setitem__(0,1)
        #Ensure Prescaler has been reset
        self.Clock_control.__setitem__(slice(4,1),0)
        #Set Prescaler value to a value of 9 as Prescaler = (N+1)
        self.Clock_control.__setitem__(1,1)
        self.Clock_control.__setitem__(2,0)
        self.Clock_control.__setitem__(3,0)
        self.Clock_control.__setitem__(4,1)
        #Ensure Max interval value has been reset 
        self.Interval_counter.__setitem__(slice(31,0),0)
        #Set interval Value to a max count of 651
        self.Interval_counter.__setitem__(0,1)
        self.Interval_counter.__setitem__(1,1)
        self.Interval_counter.__setitem__(2,0)
        self.Interval_counter.__setitem__(3,1)
        self.Interval_counter.__setitem__(4,0)
        self.Interval_counter.__setitem__(5,0)
        self.Interval_counter.__setitem__(6,0)
        self.Interval_counter.__setitem__(7,1)
        self.Interval_counter.__setitem__(8,0)
        self.Interval_counter.__setitem__(9,1)
        #Ensure that match counter value has been reset
        self.Match_counter.__setitem__(slice(31,0),0)
        # Set value of the match counter to control the duty cycle of the PWM signal
        # Set the value to 580 to ensure centre starting posisiton
        self.Match_counter.__setitem__(0,0)
        self.Match_counter.__setitem__(1,0)
        self.Match_counter.__setitem__(2,1)
        self.Match_counter.__setitem__(3,0)
        self.Match_counter.__setitem__(4,0)
        self.Match_counter.__setitem__(5,0)
        self.Match_counter.__setitem__(6,1)
        self.Match_counter.__setitem__(7,0)
        self.Match_counter.__setitem__(8,0)
        self.Match_counter.__setitem__(9,1)
        # Print values to ensure correct initliasation
        #print(" The Prescale Enable is: ",self.Clock_control.__getitem__(0))
        #print(" The Prescale value N =  ",self.Clock_control.__getitem__(slice(4,1)))
        #print(" The interval value has been set to =  ",self.Interval_counter.__getitem__(slice(31,0)))
        #print(" The Match couter value has been set to =  ",self.Match_counter.__getitem__(slice(31,0)))
    # Function for the general interrupt controller
    def GIC_set(self):
        self.GICP1_IRQ_Trigger.__setitem__(4,1)
        self.GICP1_IRQ_Trigger.__setitem__(5,1)
        self.GICP1_IRQ_Trigger.__setitem__(6,1)
        self.GICP1_IRQ_Enable.__setitem__(4,1)
        self.GICP1_IRQ_Enable.__setitem__(5,1)
        self.GICP1_IRQ_Enable.__setitem__(6,1)
    #Function to set the intterupts
    def Interrupt_enable1(self):
        #Enable Interval interrupt
        self.Interrupt_register.__setitem__(0,1)
        #Enable Match interrupt
        self.Interrupt_register.__setitem__(1,1)
        #Enable the interupts, first by resetting
        self.Interrupt_enable.__setitem__(slice(5,0),0)
        #enable the interupt
        self.Interrupt_enable.__setitem__(0,1)
        self.Interrupt_enable.__setitem__(1,1)
        #print(" Match interrupt enable: =  ",self.Interrupt_enable.__getitem__(1))
        #print(" Interval interrupt enable: =  ",self.Interrupt_enable.__getitem__(0))
    #Function to set the MIO pin
    def Set_MIO_PIN(self):
        self.MIO_PIN_39.__setitem__(5,1)
        self.MIO_PIN_39.__setitem__(6,0)
        self.MIO_PIN_39.__setitem__(7,1)
        #print("The MIO should = 5 for TTC0 Waveform output : ",self.MIO_PIN_39.__getitem__(slice(7,5)))
    #Function to start timer
    def Start_timer(self):
        self.Counter_control.__setitem__(0,0)
    def Camera_POV1(self):
        #calculate the centre point of detected object 
        mid_p = ((self.Xcord2 - self.Xcord1)/2)
        cp = self.Xcord2 - mid_p
        #Calculate which region within the image the object is detected as a percentage
        Obj_region = ((cp/self.Resize)*100)
        if (Obj_region >=40 and Obj_region <=60):
            #set centre position 570
            self.Match_counter.__setitem__(slice(31,0),0)  
            self.Match_counter.__setitem__(0,0)
            self.Match_counter.__setitem__(1,1)
            self.Match_counter.__setitem__(2,0)
            self.Match_counter.__setitem__(3,1)
            self.Match_counter.__setitem__(4,1)
            self.Match_counter.__setitem__(5,1)
            self.Match_counter.__setitem__(6,0)
            self.Match_counter.__setitem__(7,0)
            self.Match_counter.__setitem__(8,0)
            self.Match_counter.__setitem__(9,1)
            print("Camera in Centre posistion")
        #If Object is less than 40 and greater than 20
        if (Obj_region <40 and Obj_region >=20):
            # set mid left posisition 540
            self.Match_counter.__setitem__(slice(31,0),0) 
            self.Match_counter.__setitem__(0,0)
            self.Match_counter.__setitem__(1,0)
            self.Match_counter.__setitem__(2,1)
            self.Match_counter.__setitem__(3,1)
            self.Match_counter.__setitem__(4,1)
            self.Match_counter.__setitem__(5,0)
            self.Match_counter.__setitem__(6,0)
            self.Match_counter.__setitem__(7,0)
            self.Match_counter.__setitem__(8,0)
            self.Match_counter.__setitem__(9,1)
            print("Camera in Left posistion")
        if (Obj_region >60 and Obj_region <=80):
            #move camera to mid right posisition
            self.Match_counter.__setitem__(slice(31,0),0) 
            self.Match_counter.__setitem__(0,0)
            self.Match_counter.__setitem__(1,1)
            self.Match_counter.__setitem__(2,0)
            self.Match_counter.__setitem__(3,1)
            self.Match_counter.__setitem__(4,1)
            self.Match_counter.__setitem__(5,0)
            self.Match_counter.__setitem__(6,1)
            self.Match_counter.__setitem__(7,0)
            self.Match_counter.__setitem__(8,0)
            self.Match_counter.__setitem__(9,1)
            print("Camera in right posistion")
        if (Obj_region <20):
            #move camera to far left
            self.Match_counter.__setitem__(slice(31,0),0) #540
            self.Match_counter.__setitem__(0,0)
            self.Match_counter.__setitem__(1,1)
            self.Match_counter.__setitem__(2,1)
            self.Match_counter.__setitem__(3,1)
            self.Match_counter.__setitem__(4,1)
            self.Match_counter.__setitem__(5,1)
            self.Match_counter.__setitem__(6,1)
            self.Match_counter.__setitem__(7,1)
            self.Match_counter.__setitem__(8,1)
            self.Match_counter.__setitem__(9,0)
        if (Obj_region >80):
            self.Match_counter.__setitem__(slice(31,0),0)
            self.Match_counter.__setitem__(0,1)
            self.Match_counter.__setitem__(1,1)
            self.Match_counter.__setitem__(2,1)
            self.Match_counter.__setitem__(3,1)
            self.Match_counter.__setitem__(4,1)
            self.Match_counter.__setitem__(5,1)
            self.Match_counter.__setitem__(6,1)
            self.Match_counter.__setitem__(7,0)
            self.Match_counter.__setitem__(8,0)
            self.Match_counter.__setitem__(9,1)
    
    def Camera_POV(self):
        #calculate the centre point of detected object 
        mid_p = ((self.Xcord2 - self.Xcord1)/2)
        cp = self.Xcord2 - mid_p
        #Calculate which region within the image the object is detected as a percentage
        Obj_region = ((cp/self.Resize)*100)
        
        # Center Point Conditions
        if (self.Current_pos == "Centre"):
            #Condition for when new posisiton is greater than Current Posistion
            if ((self.Current_CP + 20) < (Obj_region)):
                #Move camera to the Right 595
                self.Match_counter.__setitem__(slice(31,0),0)  
                self.Match_counter.__setitem__(0,1)
                self.Match_counter.__setitem__(1,1)
                self.Match_counter.__setitem__(2,0)
                self.Match_counter.__setitem__(3,0)
                self.Match_counter.__setitem__(4,1)
                self.Match_counter.__setitem__(5,0)
                self.Match_counter.__setitem__(6,1)
                self.Match_counter.__setitem__(7,0)
                self.Match_counter.__setitem__(8,0)
                self.Match_counter.__setitem__(9,1)
                print("Camera in Centre posistion")
                self.Current_pos = "Centre"
            if ((self.Current_CP - 20) > (Obj_region)):
                # Move camera To the left
                # set mid left posisition 565
                self.Match_counter.__setitem__(slice(31,0),0)  
                self.Match_counter.__setitem__(0,0)
                self.Match_counter.__setitem__(1,0)
                self.Match_counter.__setitem__(2,0)
                self.Match_counter.__setitem__(3,0)
                self.Match_counter.__setitem__(4,1)
                self.Match_counter.__setitem__(5,1)
                self.Match_counter.__setitem__(6,0)
                self.Match_counter.__setitem__(7,0)
                self.Match_counter.__setitem__(8,0)
                self.Match_counter.__setitem__(9,1)
                print("Camera in Left posistion")
                self.Current_pos = "Left"
            if (((Obj_region) < (self.Current_CP + 20)) and ((Obj_region) > (self.Current_CP - 20))):
                # Keep Dont move camera
                print("Camera in Center posistion")
                self.Current_pos = "Centre"
        # Left Camera Posistion
        if (self.Current_pos == "Left"):
            if ((self.Current_CP + 20) < (Obj_region)):
                # set center 580
                self.Match_counter.__setitem__(slice(31,0),0)  
                self.Match_counter.__setitem__(0,0)
                self.Match_counter.__setitem__(1,0)
                self.Match_counter.__setitem__(2,1)
                self.Match_counter.__setitem__(3,0)
                self.Match_counter.__setitem__(4,0)
                self.Match_counter.__setitem__(5,0)
                self.Match_counter.__setitem__(6,1)
                self.Match_counter.__setitem__(7,0)
                self.Match_counter.__setitem__(8,0)
                self.Match_counter.__setitem__(9,1)
                print("Camera in Centre posistion")
                self.Current_pos = "Centre"
            if ((self.Current_CP - 20) > (Obj_region)):
                # Camera Out of range
                #
                print(" Object Out of View, Turn Drone")
                self.Current_pos = "Left"
            if (((Obj_region) < (self.Current_CP + 20)) and ((Obj_region) > (self.Current_CP - 20))):
                # Keep Dont move camera
                print("Camera in Left Posistion")
                self.Current_pos = "Left"
            
        # Camera in right posistion
        if (self.Current_pos == "Right"):
            if ((self.Current_CP - 20) > (Obj_region)):
                #Move back to Center
                self.Match_counter.__setitem__(slice(31,0),0)  
                self.Match_counter.__setitem__(0,0)
                self.Match_counter.__setitem__(1,0)
                self.Match_counter.__setitem__(2,1)
                self.Match_counter.__setitem__(3,0)
                self.Match_counter.__setitem__(4,0)
                self.Match_counter.__setitem__(5,0)
                self.Match_counter.__setitem__(6,1)
                self.Match_counter.__setitem__(7,0)
                self.Match_counter.__setitem__(8,0)
                self.Match_counter.__setitem__(9,1)
                print("Camera in Centre posistion")
                self.Current_pos = "Centre"
            if ((self.Current_CP + 20) < (Obj_region)):
                print(" Object Out of View, Turn Drone")
                self.Current_pos = "Right"
                
            if (((Obj_region) < (self.Current_CP + 20)) and ((Obj_region) > (self.Current_CP - 20))):
                # Keep Dont move camera
                print("Camera in Right Posistion")
                self.Current_pos = "Right"
    