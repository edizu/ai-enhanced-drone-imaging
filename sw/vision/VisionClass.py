#import libraries needed
from threading import Thread, Event
import cv2
import numpy as np
import IPython
import time
from pynq.lib.video import *
from pynq import allocate
from pynq import Overlay


resize_design = Overlay("hw/resizer_IP.bit")  


class Webcamframes(object):
    #define webcam parameters
    width = 320
    height = 240
    #define Resize parameters
    scaled_height = 480
    scaled_width = 640
    #create event object for threading
    event = Event()
    # initialise Hardware,Webcam and threading
    def __init__(self,Webcam=0):
        #initilise hardware objects
        self.dma = resize_design.resize_dma
        self.resizer = resize_design.resize_accel_0
        #set up Display port
        self.displayport = DisplayPort()
        self.displayport.configure(VideoMode(640,480,24), PIXEL_RGB)
        #configure Display port for correct settings
        #open USB webcam 
        #create a stop function
        self.Drone_cam = cv2.VideoCapture(Webcam)
        self.Drone_cam.set(cv2.CAP_PROP_FRAME_WIDTH,self.width)
        self.Drone_cam.set(cv2.CAP_PROP_FRAME_HEIGHT,self.height)
        self.thread = Thread(target=self.Captureframe,args=())
        self.thread.daemon = True
        self.thread.start() 
        self._running = True
        
    #define Class methods
    # Function to Capture frames using Webcam
    def Captureframe(self):
        #create loop so that frames will be continously looped until
        while True:
            if self.Drone_cam.isOpened():
                (self.retval, self.frame_in) = self.Drone_cam.read()
            time.sleep(.01)
    # Function to determine the size of the image
    def framesize(self):
        self.orig_height,self.orig_width,self.orig_channel = self.frame_in.shape
    #Function to allocate the input and output buffer, to be used by the Hardware Resizer
    def Allocate_memory(self):
        self.in_buffer = allocate(shape=(self.height,self.width,3), dtype=np.uint8, cacheable=True)
        self.out_buffer = allocate(shape=(self.scaled_height,self.scaled_width,3),dtype=np.uint8,cacheable=True)
        self.Display_buffer = allocate(shape=(self.scaled_height,self.scaled_width,3),dtype=np.uint8,cacheable=True)
    # Function to when called grab a frame from the camera and store within memory
        
    def Write_frame_memory(self):
        self.in_buffer[:] = self.frame_in
        if self.event.is_set():
            self.Drone_cam.release()
            print("program has ended")
            self._running = False
            print("thread Stopped")
    # Function to execute the Hardware Resizer process    
    def run_resizer(self):
        self.dma.sendchannel.transfer(self.in_buffer)
        self.dma.recvchannel.transfer(self.out_buffer)
        self.resizer.write(0x00,0x81)
        self.dma.sendchannel.wait()
        self.dma.recvchannel.wait() 
    # Function to configure the hardware resizer for what image size is needed
    def resizer_Config(self):
        self.resizer.write(0x10, self.height)
        self.resizer.write(0x18, self.width)
        self.resizer.write(0x20, self.scaled_height)
        self.resizer.write(0x28, self.scaled_width)
    # Function to stream frames to the Display port    
    def Display_Video(self):
        self.start = time.time()
        for _ in range(5):
            # Write Frame to input buffer
            #self.Write_frame_memory()
            # Call resizer Function to resize the image data
            #self.run_resizer()
            #Create new frame to be displayed to on the display port
            self.frame = self.displayport.newframe()
            #Store resized frame in the output buffer
            self.frame[:] = self.Display_buffer
            self.displayport.writeframe(self.frame)
        self.end = time.time()
        self.duration = self.end-self.start
        print(f"Took {self.duration} seconds at {5 / self.duration} FPS")
        
    # Function to display image within the input buffer to the jupyter notebook
    def Imshow_input(self):
        self.retval1, self.Imagepng = cv2.imencode('.png',self.in_buffer)
        IPython.display.display(IPython.display.Image(data=self.Imagepng.tobytes()))
        #IPython.display.clear_output(wait=True) 
        #Create condition that if a Key or a value is met then close the camera and exit the program
        #if self.event.is_set():
            #self.Drone_cam.release()
            #print("program has ended")
            #exit(1)
    #Function to Display the image within the output buffer to the jupyter notebook        
    def Imshow_output(self):
        self.retval1, self.Imagepng = cv2.imencode('.png',self.out_buffer)
        IPython.display.display(IPython.display.Image(data=self.Imagepng.tobytes()))
        #Create condition that if a Key or a value is met then close the camera and exit the program
        if self.event.is_set():
            self._running = False
            print("thread Stopped")
            self.Drone_cam.release()
            print("Camera Released")
            self.displayport.stop()
            print("Display port Stopped")
            self.displayport.close()
            print("Display port closed")
            
    def Imshow_Detections(self):
        self.retval1, self.Imagepng = cv2.imencode('.png',self.Display_buffer)
        IPython.display.display(IPython.display.Image(data=self.Imagepng.tobytes()))
        #Create condition that if a Key or a value is met then close the camera and exit the program
          
     
    #Function To end thread, Release all Devices
    def Vision_devices_Release(self):
        if self.event.is_set():
            self._running = False
            print("thread Stopped")
            self.Drone_cam.release()
            print("Camera Released")
            self.displayport.stop()
            print("Display port Stopped")
            self.displayport.close()
            print("Display port closed")
        
